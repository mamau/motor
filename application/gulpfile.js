var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var merge = require('merge-stream');
var pathExists = require('path-exists');
var sort = require('gulp-sort');

//Где разрабатываешь, то значение и поставь (admin или front)
var place = 'front';

//Копируем в public
gulp.task('copyfiles', function() {

    //Копирование ace темы
    if(!pathExists.sync('./public/ace/')){
        gulp.src([
            './resources/assets/libs/ace/**/*'
        ]).pipe(gulp.dest('./public/ace'));
    }
    //Копируем картинки
    gulp.src([
        './resources/assets/custom/img/**/*/'
    ]).pipe(gulp.dest('./public/img'));

    //Копируем шрифты
    gulp.src([
        './node_modules/font-awesome/fonts/*',
        './node_modules/bootstrap/fonts/*',
        './resources/assets/libs/ace/assets/fonts/*',
        './resources/assets/custom/fonts/**/*'
    ]).pipe(gulp.dest('./public/css/fonts/'));
});

gulp.task('custom_css', function(){

    var cssStream, sassStream;

    cssStream = gulp.src([
        './resources/assets/custom/css/*.css'
    ]);
    sassStream = gulp.src('./resources/assets/custom/sass/'+place+'/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }));


    merge(cssStream,sassStream)
        .pipe(sort({
            asc: true,
            comparator: function(file) {

                if (file.path.indexOf('sass') > -1) {
                    return 1;
                }
            }
        }))
        .pipe(uglifycss({
            "maxLineLen": 80
        }))
        .pipe(concat('app_custom.min.css'))
        .pipe(gulp.dest('./public/css/'+place+'/'));
});

//Компиляция scss и склейка его с css, на выходе имеем 1 файл
gulp.task('styles_libs', function(){

    if(!pathExists.sync('./public/css/images/')){
        gulp.src([
            './resources/assets/libs/ace/assets/css/images/*'
        ]).pipe(gulp.dest('./public/css/'+place+'/images/'));
    }

    gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.css',
        './node_modules/font-awesome/css/font-awesome.css',
        './resources/assets/custom/css/theme/*.css',
        './resources/assets/libs/ace/assets/css/ace-fonts.css',
        './resources/assets/libs/ace/assets/css/ace.css',
        './resources/assets/libs/ace/assets/css/ace-part2.css',
        './resources/assets/libs/ace/assets/css/ace-rtl.css',
        './resources/assets/libs/ace/assets/css/ace-ie.css',
        './resources/assets/custom/css/*.css'
    ]).pipe(sort({
        asc: true,
        comparator: function(file) {

            if (file.path.indexOf('sass') > -1) {
                return 1;
            }
        }
    })).pipe(uglifycss({
        "maxLineLen": 80
    }))
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./public/css/'+place+'/'));

});

//Копируем кастомные js для компонентов, и конкретных страниц
gulp.task('copyJs', function(){

    gulp.src([
        './resources/assets/custom/js/'+place+'/**/*',
        '!./resources/assets/custom/js/'+place+'/custom.js'
    ]).pipe(uglify()).pipe(gulp.dest('./public/js/'+place+'/'));
});

//Собираем все js в 1 файл
gulp.task('concatjs', function(){

    return gulp.src([
        './resources/assets/libs/ace/components/jquery/dist/jquery.js',
        './node_modules/bootstrap/dist/js/bootstrap.js',
        './resources/assets/custom/js/'+place+'/custom.js'
    ]).pipe(concat('app.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('./public/js/'+place+'/'));
});

//Watcher
gulp.task('watch', function() {
    gulp.watch('./resources/assets/custom/js/'+place+'/*.js', ['copyJs']);
    gulp.watch('./resources/assets/custom/js/'+place+'/**/*.js', ['copyJs']);
    gulp.watch('./resources/assets/custom/js/'+place+'/**/**/*.js', ['copyJs']);

    gulp.watch('./resources/assets/custom/sass/'+place+'/*.scss', ['custom_css']);
    gulp.watch('./resources/assets/custom/sass/'+place+'/**/*.scss', ['custom_css']);
    gulp.watch('./resources/assets/custom/css/'+place+'/*.css', ['custom_css']);
});
gulp.task('default', ['copyfiles','copyJs', 'styles_libs', 'custom_css', 'concatjs','watch']);
gulp.task('prod', ['copyfiles','copyJs', 'styles_libs', 'custom_css', 'concatjs']);
