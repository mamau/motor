<?php

namespace App\Widgets;

use App\Models\Settings;
use Arrilot\Widgets\AbstractWidget;

class WelcomeBlock extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function __construct(array $config = [])
    {
	    $this->addConfigDefaults([
		    'field' => 'welcome',
		    'explode' => false
	    ]);
    	parent::__construct($config);
    }

	/**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
	    $field = Settings::select($this->config['field'])->first();

	    if($this->config['explode']){

			$field_name = $this->config['field'];
		    $field = $this->breakWords($field->$field_name);
	    }

        return view("widgets.{$this->config['field']}", [
            'field' => $field
        ]);
    }

    protected function breakWords($data)
    {
    	$str = explode(',',$data);

    	return view('widgets._layouts._explode',['strs' => $str])->render();
    }
}
