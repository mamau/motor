<?php

namespace App\Widgets;

use App\Models\Banner;
use Arrilot\Widgets\AbstractWidget;

class Slider extends AbstractWidget
{
    public function __construct(array $config)
    {
	    $this->addConfigDefaults([
		    'type' => 'slider'
	    ]);
	    parent::__construct($config);
    }

	/**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

	    $banners = Banner::where('type',$this->config['type'])->with('thumb')->get();

        return view("widgets.{$this->config['type']}", [
            'banners' => $banners,
        ]);
    }
}
