<?php

namespace App\Widgets;

use App\Models\News;
use Arrilot\Widgets\AbstractWidget;

/**
 * Class NewsBlock
 * @package App\Widgets
 */
class NewsBlock extends AbstractWidget
{
    /**
     * NewsBlock constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $query = News::query()->with('thumb')->orderBy('id', 'desc');
        if ($this->config['type'] === 'col') {
            $query->skip(3)->limit(5);
        } else {
            $query->limit(9);
        }
        $query = $query->get();

        return view('widgets.news_block', [
            'news' => $query,
            'type' => $this->config['type'],
        ]);
    }
}
