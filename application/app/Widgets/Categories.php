<?php
namespace App\Widgets;

use App\Models\Category;
use Arrilot\Widgets\AbstractWidget;

/**
 * Class Categories
 * @package App\Widgets
 */
class Categories extends AbstractWidget
{
    protected $categories;

    /**
     * Categories constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->categories = Category::whereStatus(1)
            ->whereParentCat(0)
            ->orderBy('name')
            ->with('childrenCategory')
            ->get();

        $this->addConfigDefaults([
            'type' => 'top'
        ]);
        parent::__construct($config);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run()
    {
        return view("widgets.menu.{$this->config['type']}_categories_menu", [
            'categories' => $this->categories,
            'config' => $this->config,
        ]);
    }
}
