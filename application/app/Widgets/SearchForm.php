<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class SearchForm extends AbstractWidget
{
	public function __construct(array $config)
	{
		$this->addConfigDefaults([
			'active' => 'search3',
			'needle' => null,
			'analog' => null
		]);
		parent::__construct($config);
	}

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.search_form", [
            'active' => $this->config['active'],
            'needle' => $this->config['needle'],
            'analog' => $this->config['analog'],
        ]);
    }
}
