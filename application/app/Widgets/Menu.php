<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Http\Controllers\Front\PageController;

class Menu extends AbstractWidget
{

	protected $page;

	public function __construct(array $config = [])
	{
		$this->page = new PageController();
		$this->addConfigDefaults([
			'type' => 'top'
		]);
		parent::__construct($config);
	}



    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $pages = $this->page->getAllPages($this->config['type']);

        return view("widgets.menu.{$this->config['type']}_menu", [
        	'pages' => $pages,
            'config' => $this->config,
        ]);
    }

}
