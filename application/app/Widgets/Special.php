<?php
namespace App\Widgets;

use App\Models\Product;
use App\Models\Tire;
use Arrilot\Widgets\AbstractWidget;

/**
 * Class Special
 * @package App\Widgets
 */
class Special extends AbstractWidget
{
    /** @var array  */
    protected $config = [];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run()
    {
        $tires = Product::where('special', 1)->limit(4)->get();

        return view('widgets.special', [
            'tires' => $tires,
        ]);
    }
}
