<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ultraware\Roles\Models\Role;

class CreateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating roles for users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

	    Role::create([
		    'name' => 'Admin',
		    'slug' => 'admin',
		    'description' => 'Admin of shop',
		    'level' => 10,
	    ]);

	    Role::create([
		    'name' => 'Manager',
		    'slug' => 'manager',
		    'description' => 'Manager of this shop',
		    'level' => 5,
	    ]);

	    Role::create([
		    'name' => 'Client',
		    'slug' => 'client',
		    'description' => 'Client of shop',
		    'level' => 2,
	    ]);
    }
}
