<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Images;
use App\Models\Product;
use Illuminate\Console\Command;

class ParseXml extends Command {
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'parse:xml';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Parsing xml 1c';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle() {
        $import           = simplexml_load_string(file_get_contents(public_path('csv/webdata/import0_1.xml')));
        $offers           = simplexml_load_string(file_get_contents(public_path('csv/webdata/offers0_1.xml')));
        $offers->registerXPathNamespace('x', 'urn:1C.ru:commerceml_2');
        $categories       = $import->{'Классификатор'}->{'Группы'}->{'Группа'};
        $products         = $import->{'Каталог'}->{'Товары'}->{'Товар'};
        $total_products   = count($products);

        $bar_products     = $this->output->createProgressBar($total_products);

        $this->parseCategories($categories);

        foreach ($products as $product) {
            $this->setProduct($product, $offers);
            $bar_products->advance();
        }
        $bar_products->finish();
    }

    private function parseCategories($categories, $parent = 0) {
        foreach ($categories as $node) {
            $this->setCategory($node, $parent);
        }
    }

    /**
     * @param                   $product
     * @param \SimpleXMLElement $offers
     * @return void
     */
    private function setProduct($product, $offers) {

        $productData = $this->collectProduct($product, $offers);

        /** @var Product $product */
        $product = Product::updateOrCreate([
            'id_1c' => $productData['productId'],
        ], [
            'id_1c'    => $productData['productId'],
            'name'     => $productData['name'],
            'sku'      => $productData['sku'],
            'price'    => $productData['price'],
            'quantity' => $productData['quantity'],
        ]);

        $category = Category::where('id_1c', $productData['categoryId'])->first();
        if ($category) {
            $product->categories()->attach($category->id);
        }

        if ($productData['image']) {
            $this->setImage($productData, $product);
        }
        $dataSeo['title_seo']        = $productData['name'];
        $dataSeo['keywords_seo']     = $productData['name'];
        $dataSeo['description_seo']  = $productData['name'];
        $dataSeo['full_description'] = $productData['name'];

        $product->saveSeo($dataSeo, 'Product', $product->id);
    }

    /**
     * @param array   $productData
     * @param Product $product
     * @return void
     */
    private function setImage($productData, $product) {
        list($folder, $namePath, $fileName) = explode('/', $productData['image']);
        list($name, $extension) = explode('.', $fileName);
        $path = 'csv/webdata/' . $folder . '/' . $namePath . '/';
        $type = 'product';

        $img            = new Images;
        $img->path      = $path;
        $img->name      = $name;
        $img->extension = $extension;
        $img->type      = $type;
        $product->thumb()->save($img);
    }

    /**
     * @param Product           $product
     * @param \SimpleXMLElement $offers
     * @return array
     */
    private function collectProduct($product, $offers) {
        $price     = 0;
        $quantity  = 0;
        $productId = trim($product->{'Ид'});
        $offer     = $offers->xpath("x:ПакетПредложений/x:Предложения/x:Предложение[x:Ид/text() = '$productId']");

        if ($offer) {
            $price    = (int)$offer[0]->Цены->Цена->ЦенаЗаЕдиницу;
            $quantity = (int)$offer[0]->Количество;
        }

        $sku        = trim($product->{'Артикул'});
        $name       = trim($product->{'Наименование'});
        $categoryId = trim($product->{'Группы'}->{'Ид'});
        $image      = trim($product->{'Картинка'});

        return compact('sku', 'name', 'categoryId', 'image', 'price', 'quantity', 'productId');
    }

    /**
     * @param     $group
     * @param int $parent_id
     * @return void
     */
    private function setCategory($group, $parent_id = 0) {
        $groupId   = trim($group->{'Ид'});
        $groupName = trim($group->{'Наименование'});
        $childs    = $group->{'Группы'};

        /** @var Category $category */
        $category = Category::updateOrCreate([
            'id_1c' => $groupId,
        ], [
            'name'       => $groupName,
            'id_1c'      => $groupId,
            'parent_cat' => $parent_id,
        ]);

        $dataSeo['title_seo']        = $groupName;
        $dataSeo['keywords_seo']     = $groupName;
        $dataSeo['description_seo']  = $groupName;
        $dataSeo['full_description'] = $groupName;

        $category->saveSeo($dataSeo, 'Category', $category->id);
        if (!empty($childs)) {
            $this->parseCategories($childs->{'Группа'}, $category->id);
        }
    }
}
