<?php

namespace App\Console\Commands;

use App\Models\News;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ParseNews
 * @package App\Console\Commands
 */
class ParseNews extends Command
{
    const URL = 'https://www.zr.ru/news/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse news from https://www.zr.ru/news/';

    /** @var int */
    private $newsCount = 10;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parseNews();
        $this->deleteOldNews();
    }

    /**
     * @return void
     */
    private function deleteOldNews()
    {
        $this->infoMessage("\nStart delete old news");
        $this->deleteLastNews();
        $this->infoMessage("\nFinish delete old news");
    }

    private function parseNews()
    {
        $this->infoMessage("\nStart parse " . self::URL);
        $this->startParse();
        $this->infoMessage("\nFinish parse" . self::URL);
    }

    /**
     * @param string $message
     * @return void
     */
    private function infoMessage($message)
    {
        $this->info($message);
        Log::info($message);
    }

    /**
     * @return void
     */
    private function deleteLastNews()
    {
        $oldNews = News::orderBy('id')->limit($this->newsCount)->get();
        $bar = $this->output->createProgressBar($oldNews->count());
        foreach ($oldNews as $old) {
            $old->delete();
            $bar->advance();
        }
        $bar->finish();
    }

    /**
     * @return void
     */
    private function startParse()
    {
        $newsData = $this->getAllNewsLink(self::URL);
        $this->newsCount = count($newsData);
        $bar = $this->output->createProgressBar($this->newsCount);
        foreach ($newsData as $news) {
            $dataSeo = [];
            $newsRecord = News::create([
                'title' => $news['title'],
                'description' => $news['anounce'],
            ]);

            if ($news['img']) {
                $newsRecord->saveImgFromZip($this->saveImage($news['img']), 'news', $newsRecord->id);
            }

            $dataSeo['title_seo'] = $news['title'];
            $dataSeo['keywords_seo'] = $news['title'];
            $dataSeo['description_seo'] = $news['anounce'];
            $dataSeo['full_description'] = $this->changeSeoText($news['body']) . ' <cite>источник: ' . '<a href="' . $news['link'] . '">За рулем</a></cite>';
            $newsRecord->saveSeo($dataSeo, 'news', $newsRecord->id);
            $bar->advance();
        }
        $bar->finish();
    }

    /**
     * @param $matches
     * @return string
     */
    private static function cbReplace($matches)
    {
        $results = array_unique($matches);
        if ($results) {
            foreach ($results as $res) {
                if (strripos($res, 'href') + 1) {
                    $res = substr($res, 0, -1);
                    $res = substr($res, stripos($res, '/'));
                    return 'href=\'https://www.zr.ru' . $res . '\'';
                }
            }
        }
    }

    /**
     * @param $text
     * @return null|string|string[]
     */
    private function changeSeoText($text)
    {
        $pattern = '/((href=(\'|\")+\/[0-9a-z\.\-_\/=]+(\'|\")))/i';
        return preg_replace_callback($pattern, 'self::cbReplace', $text);
    }

    /**
     * @param $image
     * @return bool|int
     */
    private function saveImage($image)
    {
        $filename = basename($image) . '.jpg';
        $path = public_path('tmpImg/' . $filename);
        if (!File::exists(public_path('tmpImg'))) {
            File::makeDirectory(public_path('tmpImg'), 0777, true);
        } else {
            File::cleanDirectory(public_path('tmpImg'));
        }

        Image::make($image)->save($path);

        $files = File::allFiles(public_path('tmpImg'));

        return $files[0];
    }


    /**
     * @param $link
     * @return array
     */
    private function getAllNewsLink($link)
    {
        $html = file_get_contents($link);

        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $data = $crawler->filter('.story-short')->each(function (Crawler $node, $i) {
            $img = $node->filter('.story-short .articles__item-img img')->image()->getUri();
            $anounce = $node->filter('.story-short .articles__item-desc')->html();
            $dataNews = $node->filter('.story-short .articles__item-ttl a')->each(function (Crawler $node, $i) {
                $parser = [
                    'title' => '.story-detail-panel .head',
                    'body' => '.story-detail-panel .content',
                ];
                return $this->getContent($parser, $node->link()->getUri());
            });
            $dataNews[0]['img'] = $img;
            $dataNews[0]['anounce'] = $anounce;
            return $dataNews[0];
        });

        return $data;
    }

    /**
     * @param $parser
     * @param $link
     * @return array
     */
    private function getContent($parser, $link)
    {
        // Get html remote text.
        $html = file_get_contents($link);

        // Create new instance for parser.
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        // Get images from page.
        $title = $crawler->filter($parser['title'])->text();

        // Get body text.
        $body = $crawler->filter($parser['body'])->html();

        $content = [
            'link' => $link,
            'title' => $title,
            'body' => $body,
        ];
        return $content;
    }
}
