<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Artem328\LaravelYandexKassa\Events\BeforeCheckOrderResponse' => [
            'App\Listeners\CheckOrderRequisites',
        ],
        'App\Events\OrderNotification' => [
            'App\Listeners\OrderNotificationListener',
        ],
        'Artem328\LaravelYandexKassa\Events\BeforePaymentAvisoResponse' => [
            'App\Listeners\ChangeOrderStatusWhenPaymentSuccessful',
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
        ],
        'App\Events\ParseXml' => [
            'App\Listeners\ParseXmlListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
