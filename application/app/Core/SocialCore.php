<?php
namespace App\Core;

use App\Models\UserSocialAccount;
use App\Models\User;
use Ultraware\Roles\Models\Role;

/**
 * Class SocialCore
 * @package App\Core
 */
class SocialCore
{
    /**
     * @param $providerObj
     * @param $providerName
     * @return static
     */
    public function createOrGetUser($providerObj, $providerName)
    {
        $providerUser = $providerObj->user();

        $account = UserSocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new UserSocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName]);

            $email = $providerUser->getEmail();
            if (!$email) {
                $email = $providerUser->accessTokenResponseBody['email'];
            }

            $user = User::whereEmail($email)->first();

            if (!$user) {
                $user = User::createBySocialProvider($providerUser);
            }

            $account->user()->associate($user);
            $account->save();

            $clientRole = Role::where('slug', 'client')->first();
            if (!is_null($clientRole)) {
                $user->attachRole($clientRole);
            }

            return $user;
        }
    }
}
