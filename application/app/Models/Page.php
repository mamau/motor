<?php

namespace App\Models;

use App\Traits\SeoTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

	use SeoTrait;

    protected $fillable = [
    	'name',
	    'parent_page',
	    'menu',
	    'sort',
	    'url',
        'alias'
    ];

	public function seo(){

		return $this->morphOne(Seo::class, 'seoable');
	}

	public function children()
	{
		return $this->hasMany(Page::class, 'parent_page');
	}


	public function childrenPage()
	{
		return $this->children()->with('childrenPage');
	}
}
