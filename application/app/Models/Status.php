<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name', 'description'];

    public function order()
    {
        return $this->belongsTo(Foreign_order::class,'status','id');
    }
}
