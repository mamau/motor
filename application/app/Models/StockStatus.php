<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StockStatus
 * @package App\Models
 */
class StockStatus extends Model
{
    const SLUG_IN_STOCK = 'have';
    const SLUG_OUT_OF_STOCK = 'nothave';
    const SLUG_WAITING = 'waiting';
    const SLUG_PREORDER = 'preorder';

    /** @var string */
    protected $table = 'stock_status';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
    ];
}
