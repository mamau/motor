<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
	protected $table = 'seo';

	protected $fillable = ['title_seo','keywords_seo','description_seo','full_description'];

	public function seoable(){

		return $this->morphTo();
	}
}
