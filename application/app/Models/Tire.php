<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tire extends Model
{
	protected $fillable = ['id','name','code_1c','code','width','height','diameter','index_speed','index_load','type','season','brand','model','multiplicity','price','vendor_id'];
}
