<?php

namespace App\Models;

use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	use UploadThumb;

    protected $fillable = [
        'name',
	    'type',
	    'url',
	    'description'
    ];

	public function thumb()
	{
		return $this->morphOne(Images::class, 'imageable');
	}

	public function thumbType($type = 'slider')
	{
		return $this->morphOne(Images::class, 'imageable')->where('type',$type);
	}
}
