<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
	protected $table = 'dialog';

    protected $fillable = [
        'feedback_id',
	    'author_id',
	    'viewed',
	    'text'
    ];

    public function feedback()
    {
    	return $this->belongsTo(Feedback::class);
    }

	/**
	 * Спрашивающий пользователь
	 */
    public function asking()
    {
    	return $this->hasOne(User::class, 'id', 'asking_id');
    }

    /**
     * Отвечающий пользователь
     */

    public function respondent()
    {
    	return $this->hasOne(User::class, 'id', 'respondent_id');
    }
}
