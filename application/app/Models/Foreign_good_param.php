<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foreign_good_param extends Model
{
    protected $fillable = [
        'foreign_good_id', 'param', 'value'
    ];

    public function foreignGood()
    {
    	return $this->belongsTo(Foreign_good::class);
    }
}
