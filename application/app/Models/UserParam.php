<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserParam extends Model
{
	protected $fillable = [
		'user_id',
		'surname',
		'city_id',
		'gender',
		'birthday',
		'address'
	];

	public function getDates()
	{
		return ['created_at','updated_at', 'birthday'];
	}
}
