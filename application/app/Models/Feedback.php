<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'author_id',
	    'title',
	    'closed'
    ];

    public function dialog()
    {
		return $this->hasMany(Dialog::class);
    }

    public function author()
    {
    	return $this->hasOne(User::class, 'id', 'author_id');
    }
}
