<?php

namespace App\Models;

use App\Traits\SeoTrait;
use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
	use UploadThumb, SeoTrait;

    protected $fillable = ['name', 'sort'];

	public static function boot(){

		parent::boot();

		static::deleting(function($manufacturer){

			$thumb = $manufacturer->thumb()->first();
			$seo = $manufacturer->seo()->first();

			if($thumb){
				Images::find($thumb->id)->forceDelete();
			}
			if($seo){
				Seo::find($seo->id)->delete();
			}

		});
	}

	public function thumb(){

		return $this->morphOne(Images::class, 'imageable');
	}

	public function seo(){

		return $this->morphOne(Seo::class, 'seoable');
	}

	public function products(){

		return $this->belongsToMany(Product::class);
	}
}
