<?php

namespace App\Models;

use App\Traits\SeoTrait;
use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

/**
 * Class Product
 * @package App\Models
 */
class Product extends Model
{
    use UploadThumb, SeoTrait, SoftDeletes, Eloquence;

    const CLARIFY_PRICE = 'Уточните цену по телефону';
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'model_product',
        'sku',
        'price',
        'quantity',
        'minimum',
        'stock_status_id',
        'date_available',
        'status',
        'sort_order',
        'manufacturer_id',
        'id_1c',
        'special',
    ];

    /**
     * @var array
     */
    protected $searchableColumns = ['name', 'model_product', 'sku', 'price', 'attributes.value', 'attributes.name'];

    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'date_available', 'updated_at', 'deleted_at'];
    }

    /**
     * @return void
     */
    public static function boot()
    {

        parent::boot();

        static::restoring(static function ($product) {

            $thumb = $product->thumb()->withTrashed()->first();
            if (count($thumb) > 0) {
                $thumb->restore();
            }
        });

        static::deleting(static function ($product) {

            $thumb = $product->thumb()->withTrashed()->first();
            $seo = $product->seo()->first();

            if (count($thumb) > 0) {
                if ($product->forceDeleting) {
                    $thumb->forceDelete();
                } else {
                    $thumb->delete();
                }
            }
            if (count($seo) > 0) {
                if ($product->forceDeleting) {
                    $seo->forceDelete();
                }
            }

            if ($product->forceDeleting) {

                $q = 'DELETE FROM category_product where product_id = ?';
                \DB::delete($q, [$product->id]);
                $q2 = 'DELETE FROM manufacturer_product where product_id = ?';
                \DB::delete($q2, [$product->id]);
            }

        });
    }

    /**
     * @param $value
     * @return int
     */
    public function getPriceAttribute($value)
    {
        return (int)$value;
    }

    /**
     * @param $value
     * @return int
     */
    public function getQuantityAttribute($value)
    {
        return (int)$value;
    }

    /**
     * @return string
     */
    public function getStockStatusAttribute()
    {
        $stock_status = $this->stockStatus()->first();
        if ($stock_status !== null) {
            $status = $stock_status->name;
        } elseif ($this->price === 0) {
            $status = StockStatus::whereSlug(StockStatus::SLUG_WAITING)->value('name');
        } else {
            $status = StockStatus::whereSlug(StockStatus::SLUG_IN_STOCK)->value('name');
        }
        return $status;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function thumb()
    {
        return $this->morphOne(Images::class, 'imageable');
    }

    public function seo()
    {

        return $this->morphOne(Seo::class, 'seoable');
    }

    public function attributes()
    {
        return $this->morphMany(Attribute::class, 'attributable');
    }

    public function attribute()
    {
        return $this->morphOne(Attribute::class, 'attributable');
    }

    public function categories()
    {

        return $this->belongsToMany(Category::class);
    }

    public function categoryById($id)
    {

        return Category::find($id);
    }

    public function manufacturer()
    {

        return $this->hasOne(Manufacturer::class, 'id', 'manufacturer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stockStatus()
    {
        return $this->hasOne(StockStatus::class, 'id', 'stock_status_id');
    }

    public function setSpecialAttribute($value)
    {
        return $this->attributes['special'] = isset($value) ? 1 : 0;
    }
}
