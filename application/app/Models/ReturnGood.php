<?php

namespace App\Models;

use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;

class ReturnGood extends Model
{
	use UploadThumb;

	protected $fillable = ['order_id', 'user_id','cause', 'description'];

	public function thumb(){

		return $this->morphOne(Images::class, 'imageable');
	}

	public function thumbs(){

		return $this->morphMany(Images::class, 'imageable');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function order()
	{
		return $this->belongsTo(Foreign_order::class,'order_id','id');
	}
}
