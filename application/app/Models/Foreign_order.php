<?php

namespace App\Models;

use App\Events\OrderNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Foreign_order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'sum', 'status',
    ];

    public function foreignGoods()
    {
        return $this->hasMany(Foreign_good::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rbacks()
    {
        return $this->hasOne(ReturnGood::class, 'id', 'order_id');
    }

    public function statuz()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }
}
