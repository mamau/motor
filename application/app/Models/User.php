<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Ultraware\Roles\Models\Role;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use Notifiable, SoftDeletes, HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'phone', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function boot()
	{
		parent::boot();

		static::created(function($user){

			UserParam::create([
				'user_id' => $user->id,
				'surname' => '',
				'city_id' => 1192,
				'gender' => 'male',
				'birthday' => Carbon::now(),
				'address' => '',
			]);
		});
	}

    public function routeNotificationForMail()
    {
    	return $this->email;
    }

    public function userParams()
    {
    	return $this->hasOne(UserParam::class);
    }

	public function balance()
	{
		// TODO: Implement balance() method.
	}

	public function transactions()
	{
		// TODO: Implement transactions() method.
	}

	public function rbacks()
	{
		return $this->hasMany(ReturnGood::class);
	}

    public static function createBySocialProvider($providerUser)
    {
        $email = $providerUser->getEmail();
        if (!$email) {
            $email = $providerUser->accessTokenResponseBody['email'];
        }
        return self::create([
            'email' => $email,
            'name' => $providerUser->getName(),
            'password' => bcrypt($providerUser->getName()),
        ]);
    }
}
