<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'shop_name',
	    'address',
	    'email',
	    'phone',
	    'work_time',
	    'welcome'
    ];
}
