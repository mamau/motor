<?php

namespace App\Models;

use App\Traits\SeoTrait;
use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App\Models
 */
class News extends Model
{
	use UploadThumb, SeoTrait;

	/** @var array */
    protected $fillable = [
        'title',
	    'description'
    ];

    /**
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(static function ($news) {
            $thumb = $news->thumb()->first();
            $seo = $news->seo()->first();

            if ($thumb !== null) {
                $thumb->forceDelete();
            }

            if ($seo !== null) {
                $seo->delete();
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
	public function thumb()
	{
		return $this->morphOne(Images::class, 'imageable');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
	public function seo()
	{
		return $this->morphOne(Seo::class, 'seoable');
	}
}
