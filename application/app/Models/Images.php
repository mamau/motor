<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use File;

class Images extends Model
{
	use SoftDeletes;

	protected $fillable = ['path','name','extension','type'];

	public static function boot(){

		parent::boot();

		static::deleted(static function($image){

			if ($image->forceDeleting) {

				$path = $image->path;
				if(File::exists($path)){

					File::deleteDirectory($path);
				}
			}
		});
	}

	public function imageable(){

		return $this->morphTo();
	}
}
