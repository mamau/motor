<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Markup extends Model
{
    protected $fillable = ['param', 'value'];
}
