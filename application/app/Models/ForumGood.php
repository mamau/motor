<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class ForumGood extends Model
{
	use Eloquence;

	protected $searchableColumns = ['marka', 'catalog_num', 'name', 'code'];

	protected $fillable = [
		'marka', 'catalog_num', 'name', 'price', 'count', 'minimum', 'code'
	];
}
