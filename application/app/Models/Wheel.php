<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wheel extends Model
{
    protected $fillable = ['id','name','code_1c','width','diameter','diameter_hole','outreach','diameter_central','color1','color2','type','brand','model','price','vendor_id'];
}
