<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSocialAccount
 * @package App\Models
 */
class UserSocialAccount extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
        'access_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
