<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class FavoritGoods extends Model
{
	use Eloquence;

	protected $searchableColumns = ['proizvoditel', 'nomer_po_katalogu', 'naimenovanie'];

    protected $fillable = [
        'proizvoditel', 'nomer_po_katalogu', 'naimenovanie', 'tsena_po_dogovoru', 'kolichestvo', 'kratnost', 'no_name'
    ];
}
