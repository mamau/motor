<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foreign_good extends Model
{
    protected $fillable = [
        'foreign_order_id', 'name', 'price', 'count', 'client_type', 'product_id'
    ];


    public function foreignOrders()
    {
    	return $this->belongsTo(Foreign_good::class);
    }

    public function foreignGoodParams()
    {
    	return $this->hasMany(Foreign_good_param::class);
    }
}
