<?php

namespace App\Models;

use App\Traits\SeoTrait;
use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{
    use UploadThumb, SeoTrait, SoftDeletes;

    const STATUS_ACTIVE = 1;

    protected $table = 'categories';

    protected $fillable = [
        'id',
        'name',
        'parent_cat',
        'sort',
        'status',
        'id_1c',
    ];

    public static function boot()
    {

        parent::boot();

        static::deleting(function ($category) {

            if ($category->forceDeleting) {

                $thumb = $category->thumb()->first();
                $seo = $category->seo()->first();

                if ($thumb) {
                    Images::find($thumb->id)->forceDelete();
                }
                if ($seo) {
                    Seo::find($seo->id)->delete();
                }
            }
        });
    }

    public function thumb()
    {

        return $this->morphOne(Images::class, 'imageable');
    }

    public function seo()
    {

        return $this->morphOne(Seo::class, 'seoable');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_cat');
    }


    public function childrenCategory()
    {
        return $this->children()->with('childrenCategory');
    }

    public function parent()
    {

        return $this->hasOne(Category::class, 'id', 'parent_cat');
    }

    public function parentCategory()
    {

        return $this->parent()->with('parentCategory');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->groupBy('id');
    }
}
