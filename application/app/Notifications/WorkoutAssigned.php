<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WorkoutAssigned extends Notification
{
    use Queueable;

	private $workout;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($workout)
    {
	    $this->workout = $workout;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Вам назначена новая тренировка!')
                    ->action('Просмотреть тренировку', 'https://laravel.com')
                    ->line('Спасибо, что пользуетесь нашим приложением!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
	        'workout' => $this->workout->id
        ];
    }
}
