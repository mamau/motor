<?php

namespace App\Events;

use App\Models\Foreign_order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderNotification
{
    use InteractsWithSockets, SerializesModels;

    protected $order;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Foreign_order $order)
    {
		$this->order = $order;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');
    }
}
