<?php
/**
 * Created by PhpStorm.
 * User: mamau
 * Date: 15.01.17
 * Time: 22:43
 */

namespace App\Traits;

use File;
use Image;
use App\Models\Images;

/**
 * Trait UploadThumb
 * @package App\Traits
 */
trait UploadThumb
{
    /**
     * @param $request
     * @param $type
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function setThumb($request, $type, $id)
    {
        $image = $this->saveImage($request, $type, $id);

        return response()->json(['status' => true, 'value' => $image]);
    }

    /**
     * @param $file
     * @param $type
     * @param $id
     */
    public function setManyThumbs($file, $type, $id)
    {
        $className = 'App\\Models\\' . $type;
        $model = $className::find($id);

        $extension = $file->getClientOriginalExtension();
        $path = "images/$type/$id/";
        $name = str_random(8);

        $img = new Images;
        $img->path = $path;
        $img->name = $name;
        $img->extension = $extension;
        $img->type = $type;
        $model->thumb()->save($img);

        $this->cropLogo(public_path($path), $name, $extension, $file, false);
    }

    /**
     * @param $file
     * @param $img_type
     * @param $id
     * @return string
     */
    protected function saveImage($file, $img_type, $id)
    {
        $file = $this->saveLogo($file, $img_type, $id);

        if (File::exists($file->path . $file->name . '.sm.' . $file->extension)) {
            $image = asset($file->path . $file->name . '.sm.' . $file->extension);
        } else {
            $image = asset($file->path . $file->name . '.' . $file->extension);
        }
        return $image;

    }

    /**
     * @param $file
     * @param $img_type
     * @param $id
     * @return Images
     */
    protected function saveLogo($file, $img_type, $id)
    {
        $className = 'App\\Models\\' . $img_type;
        $model = $className::find($id);

        $extension = $file->getClientOriginalExtension();
        $path = "images/$img_type/$id/";
        $name = str_random(8);

        $thumb = $model->thumb()->first();
        if ($thumb === null) {

            $img = new Images;
            $img->path = $path;
            $img->name = $name;
            $img->extension = $extension;
            $img->type = $img_type;
            $model->thumb()->save($img);
        } else {
            $img = Images::find($thumb->id);
            $img->update([
                'path' => $path,
                'name' => $name,
                'extension' => $extension,
                'type' => $img_type,
            ]);
        }

        $this->cropLogo($path, $name, $extension, $file);

        return $img;
    }

    /**
     * @param $path
     * @param $fileName
     * @param $fileExtension
     * @param $file
     * @param bool $clean
     * @param bool $spl
     * @return bool
     */
    protected function cropLogo($path, $fileName, $fileExtension, $file, $clean = true, $spl = false)
    {
        $isFileExists = File::exists($path);
        if ($isFileExists && $clean) {
            File::cleanDirectory($path);
        } else if (!$isFileExists) {
            File::makeDirectory($path, 0777, true);
        }

        if (!$spl) {
            $file->move($path, $fileName . '.' . $fileExtension);
        } else {
            rename($file, $path . $fileName . '.' . $fileExtension);
        }


        $image = Image::make($path . $fileName . '.' . $fileExtension)->orientate();
        if ($image->width() > 1000 || $image->height() > 1600) {
            $image->resize($image->width(), $image->height(), static function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path . $fileName . '.lg.' . $fileExtension);
        }
        if ($image->width() > 320 || $image->height() > 240) {
            $image->resize(320, 240, static function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path . $fileName . '.sm.' . $fileExtension);
        }
        if ($image->width() > 150 || $image->height() > 100) {
            $image->resize(150, 150, static function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path . $fileName . '.xs.' . $fileExtension);
        }

        return true;
    }


    /**
     * @param $file
     * @param $img_type
     * @param $id
     * @return Images
     */
    public function saveImgFromZip($file, $img_type, $id)
    {
        $className = 'App\\Models\\' . $img_type;
        $model = $className::find($id);

        $extension = $file->getExtension();
        $path = "images/$img_type/$id/";
        $name = str_random(8);
        $thumb = $model->thumb()->first();
        if ($thumb === null) {
            $img = new Images;
            $img->path = $path;
            $img->name = $name;
            $img->extension = $extension;
            $img->type = $img_type;
            $model->thumb()->save($img);
        } else {
            $img = Images::find($thumb->id);
            $img->update([
                'path' => $path,
                'name' => $name,
                'extension' => $extension,
                'type' => $img_type,
            ]);
        }

        $this->cropLogo(public_path($path), $name, $extension, $file, false, true);

        return $img;
    }
}
