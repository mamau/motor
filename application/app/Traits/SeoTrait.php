<?php
/**
 * Created by PhpStorm.
 * User: mamau
 * Date: 15.01.17
 * Time: 22:57
 */

namespace App\Traits;

use App\Models\Seo;

trait SeoTrait
{

	public function saveSeo($data, $type, $id){

		$className = 'App\\Models\\'.$type;
		$model = $className::find($id);

		$seo = $model->seo()->first();
		if(is_null($seo)){

			$seo = new Seo();
			$seo->title_seo = $data['title_seo'];
			$seo->keywords_seo = $data['keywords_seo'];
			$seo->description_seo = $data['description_seo'];
			$seo->full_description = $data['full_description'];

			$model->seo()->save($seo);
		}else{

			Seo::find($seo->id)->update([
				'title_seo' => $data['title_seo'],
				'keywords_seo' => $data['keywords_seo'],
				'description_seo' => $data['description_seo'],
				'full_description' => $data['full_description']
			]);
		}
	}
}
