<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArmtekBasket implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $productsArmtek = [];

	    $goodsArmtek = $this->order->foreignGoods()
		    ->where('client_type','armtek')
		    ->get();

	    $productsArmtekToBasket = [];
	    if($goodsArmtek){
		    foreach ($goodsArmtek as $good){
			    $currentProduct = array_pluck($good->foreignGoodParams()->get(),'value','param');
			    $productsArmtek['PIN'] = $currentProduct['PIN'];
			    $productsArmtek['BRAND'] = $currentProduct['BRAND'];
			    $productsArmtek['KWMENG'] = (int)$currentProduct['COUNT'];
			    $productsArmtek['KEYZAK'] = $currentProduct['KEYZAK'];
			    $productsArmtek['PRICEMAX'] = $currentProduct['PRICE'];
			    $productsArmtek['DATEMAX'] = $currentProduct['DLVDT'];
			    $productsArmtek['COMMENT'] = '';
			    $productsArmtek['COMPL_DLV'] = '';
			    $productsArmtekToBasket[] = $productsArmtek;
		    }
		    app('App\Http\Controllers\Front\ArmtekController')->createOrder($productsArmtekToBasket);
	    }
    }
}
