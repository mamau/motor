<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvtotoBasket implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $productsAvtoto = [];
	    $goodsAvtoto = $this->order->foreignGoods()
		    ->where('client_type','avtoto')
		    ->get();

	    if($goodsAvtoto){
		    foreach ($goodsAvtoto as $good){
			    $productsAvtoto[] = array_pluck($good->foreignGoodParams()->get(),'value','param');
		    }

		    app('App\Http\Controllers\Front\AvtotoController')->avtotoBasket($productsAvtoto);
	    }
    }
}
