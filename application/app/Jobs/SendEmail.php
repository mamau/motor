<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

	protected $email;
	protected $theme;
	protected $type;
	protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $theme, $type, $data)
    {
	    $this->email = $email;
	    $this->theme = $theme;
	    $this->type = $type;
	    $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $email = $this->email;
	    $theme = $this->theme;
	    Mail::send('emails.'.$this->type, $this->data, function($message) use ($email, $theme){

		    $message->to($email)->subject($theme);
	    });
    }
}
