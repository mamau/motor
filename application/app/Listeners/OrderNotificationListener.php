<?php

namespace App\Listeners;

use App\Events\OrderNotification;
use App\Jobs\SendEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderNotification  $event
     * @return void
     */
    public function handle(OrderNotification $event)
    {
	    dispatch(new SendEmail(config('settings.admin_email'), 'Поступил заказ', 'orderNotification', []));
    }
}
