<?php
namespace App\Listeners;

//use App\Order;
use App\Jobs\ArmtekBasket;
use App\Jobs\AvtotoBasket;
use App\Models\Foreign_order;
use Artem328\LaravelYandexKassa\Events\BeforePaymentAvisoResponse;
use Gloudemans\Shoppingcart\Facades\Cart;


class ChangeOrderStatusWhenPaymentSuccessful
{
	/**
	 * @param \Artem328\LaravelYandexKassa\Events\BeforePaymentAvisoResponse
	 * @return void
	 */
	public function handle(BeforePaymentAvisoResponse $event)
	{
		// if hash is valid we know that payment is successful
		// and we can change order status
		if ($event->request->isValidHash()) {

			$order =  Foreign_order::find($event->request->get('orderNumber'));
			$order->update([
				'status' => 1
			]);

//			dispatch(new AvtotoBasket($order));
//			dispatch(new ArmtekBasket($order));


		} else {
			// Logic on non valid hash
			// You don't need to set response code to 1
			// YandexKassaRequest do it automatically
		}
	}
}
