<?php
namespace App\Listeners;

//use App\Order;
use Artem328\LaravelYandexKassa\Events\BeforeCheckOrderResponse;

class CheckOrderRequisites
{
	/**
	 * @param \Artem328\LaravelYandexKassa\Events\BeforeCheckOrderResponse
	 * @return array|null
	 */
	public function handle(BeforeCheckOrderResponse $event)
	{

		if (!isset($event->request->orderNumber)) {
			$event->responseParameters['code'] = 100;
			$event->responseParameters['message'] = 'Нет поля orderNumber';

			return $event->responseParameters;
		}

		return null;
	}
}
