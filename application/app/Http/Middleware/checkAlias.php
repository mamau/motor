<?php

namespace App\Http\Middleware;

use App\Models\Page;
use Closure;

class checkAlias
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Page::query()->where('alias', $request->path())->first()) {
            route('front.page.alias', $request->path());
        }

        return $next($request);
    }
}
