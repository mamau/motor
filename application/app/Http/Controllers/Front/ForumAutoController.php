<?php

namespace App\Http\Controllers\Front;

use App\Models\ForumGood;
use App\Models\Markup;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForumAutoController extends Controller
{
    public function addBasket(Request $request)
    {
	    $product = ForumGood::find($request->id);
	    $markup = Markup::select('value')->where('param','forumAuto')->first();
	    $price = ($product->price/100)*$markup->value+$product->price;

	    Cart::add($product->id, $product->name, $request->count, $price, [
		    'maxCount' => $product->count,
		    'minCount' => $product->minimum,
		    'marka' => $product->marka,
		    'catalog_num' => $product->catalog_num,
		    'name' => $product->name,
		    'price' => $product->price,
		    'count' => $product->count,
		    'minimum' => $product->minimum,
		    'code' => $product->code,
		    'client_type' => 'forumAuto'
	    ]);

	    $list = $this->productsList();

	    return response()->json(['status' => true, 'data' => ['name' => $product->name,'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
    }

	protected function productsList(){

		return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
	}
}
