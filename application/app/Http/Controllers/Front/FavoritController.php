<?php

namespace App\Http\Controllers\Front;

use App\Models\FavoritGoods;
use App\Models\Markup;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoritController extends Controller
{
    public function addBasket(Request $request)
    {
    	$product = FavoritGoods::find($request->id);
	    $markup = Markup::select('value')->where('param','favoritAuto')->first();

	    $price = ($product->tsena_po_dogovoru/100)*$markup->value+$product->tsena_po_dogovoru;

	    Cart::add($product->id, $product->naimenovanie, $request->count, $price, [
		    'maxcount' => $product->kolichestvo,
		    'mincount' => $product->kratnost,
		    'naimenovanie' => $product->naimenovanie,
		    'proizvoditel' => $product->proizvoditel,
		    'nomer_po_katalogu' => $product->nomer_po_katalogu,
		    'tsena_po_dogovoru' => $product->tsena_po_dogovoru,
		    'kolichestvo' => $product->kolichestvo,
		    'kratnost' => $product->kratnost,
		    'no_name' => $product->no_name,
		    'client_type' => 'favorit'
	    ]);

	    $list = $this->productsList();

	    return response()->json(['status' => true, 'data' => ['name' => $product->naimenovanie,'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
    }

	protected function productsList(){

		return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
	}
}
