<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

/**
 * Class TrinityController
 * @package App\Http\Controllers\Front
 */
class TrinityController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addBasket(Request $request)
    {
        Cart::add($request->code, $request->caption, $request->count, $request->price, $request->all());

        $list = $this->productsList();

        return response()->json([
            'status' => true,
            'data' => [
                'name' => $request['Name'],
                'count' => Cart::count(),
                'total' => Cart::subtotal(),
                'html' => $list,
            ],
        ]);
    }

    /**
     * @return mixed|null|string
     * @throws \Throwable
     */
    protected function productsList()
    {
        return view('front.pages.cart._cart.productsList', [
                'cartContent' => Cart::content(),
                'cartSubtotal' => Cart::subtotal()]
        )->render();
    }
}
