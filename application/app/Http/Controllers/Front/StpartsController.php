<?php

namespace App\Http\Controllers\Front;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StpartsController extends Controller
{
    public function basket(Request $request)
    {
        Cart::add($request['id'], $request['description'], $request['Count'], $request['price'], [
            'maxcount' => $request['MaxCount'],
            'mincount' => $request['BaseCount'],
            'description' => $request['description'],
            'count' => $request['Count'],
            'price' => $request['price'],
            'id' => $request['id'],
            'distributorId' => $request['distributorId'],
            'grp' => $request['grp'],
            'code' => $request['code'],
            'brand' => $request['brand'],
            'number' => $request['number'],
            'numberFix' => $request['numberFix'],
            'availability' => $request['availability'],
            'packing' => $request['packing'],
            'deliveryPeriod' => $request['deliveryPeriod'],
            'deliveryPeriodMax' => $request['deliveryPeriodMax'],
            'deadlineReplace' => $request['deadlineReplace'],
            'distributorCode' => $request['distributorCode'],
            'markup' => $request['markup'],
            'supplierCode' => $request['supplierCode'],
            'supplierColor' => $request['supplierColor'],
            'supplierDescription' => $request['supplierDescription'],
            'weight' => $request['weight'],
            'volume' => $request['volume'],
            'deliveryProbability' => $request['deliveryProbability'],
            'lastUpdateTime' => $request['lastUpdateTime'],
            'additionalPrice' => $request['additionalPrice'],
            'noReturn' => $request['noReturn'],
            'client_type' => 'stparts'

        ]);

        $list = $this->productsList();

        return response()->json(['status' => true, 'data' => ['name' => $request['description'],'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
    }

    protected function productsList(){

        return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
    }
}
