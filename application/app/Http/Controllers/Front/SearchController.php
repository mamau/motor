<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\Front\SearchAvtotekRequest;
use App\Http\Requests\Front\SearchRequest;
use App\Http\StpartsService\Stparts;
use App\Models\FavoritGoods;
use App\Models\ForumGood;
use App\Models\Markup;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Triniti\TrinityPartsWS;

class SearchController extends Controller
{

    public function searchBySku(SearchRequest $request)
    {
        $needle = $request->needle;
        $result = Product::search("*" . $needle . "*", ['sku' => 10], false)->get();
        return view('front.pages.search', ['products' => $result, 'needle' => $needle, 'active' => 'search1']);
    }

    public function searchByAny(SearchRequest $request)
    {
        $needle = $request->needle;
        $result = Product::search($needle)->get();
        return view('front.pages.search', ['products' => $result, 'needle' => $needle, 'active' => 'search2']);
    }

    public function searchByFavorit($request)
    {
        $client_id = 0;
        if (Auth::check()) {
            $client_id = Auth::user()->id;
        }
        $needle = $request->pin;
        $markup = Markup::select('value')->where('param', 'favoritAuto')->first();

        $products = FavoritGoods::search("*" . $needle . "*")->limit(5)->get();

        return view('front.pages.favorit._search', ['products' => $products, 'client_id' => $client_id, 'markup' => $markup->value])->render();
    }

    public function searchByForumAuto($request)
    {
        $client_id = 0;
        if (Auth::check()) {
            $client_id = Auth::user()->id;
        }
        $needle = $request->pin;
        $markup = Markup::select('value')->where('param', 'forumAuto')->first();
        $products = ForumGood::search("*" . $needle . "*")->limit(5)->get();

        return view('front.pages.forumAuto._search', ['products' => $products, 'client_id' => $client_id, 'markup' => $markup->value])->render();
    }

    /**
     * @param SearchAvtotekRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function searchAvtotek(SearchAvtotekRequest $request)
    {
        $trinityHtml = $this->searchByTrinityPartsWS($request);
        $favoritHtml = $this->searchByFavorit($request);
        $forumHtml = $this->searchByForumAuto($request);

        return view('front.pages.avtotekSearch.search', [
            'result' => $trinityHtml . $favoritHtml . $forumHtml,
            'active' => 'search3',
            'needle' => $request->pin,
        ]);
    }

    /**
     * @param $article
     * @param $brand
     * @return mixed|null|string
     * @throws \Throwable
     */
    public function articleBrand($article, $brand)
    {
        $client_id = 0;
        if (Auth::check()) {
            $client_id = Auth::user()->id;
        }
        $ws = new TrinityPartsWS('6213A8959A9A96589CA484DFD1E25053');
        $products = $ws->searchItems($article, $brand);
        $markup = Markup::select('value')->where('param', 'trinity')->first();
        if (!$markup) {
            $markup = 10;
        } else {
            $markup = $markup->value;
        }
        $collection = collect($products['data']);
        $products = $collection->unique('code');
        $searchable = $products->where('code', $article);
        $products->prepend($searchable->first());

        $result = view('front.pages.trinity._searchArticleBrand', [
            'products' => $products,
            'brand' => $brand,
            'article' => $article,
            'client_id' => $client_id,
            'markup' => $markup
        ])->render();

        return view('front.pages.avtotekSearch.search', [
            'result' => $result,
            'active' => 'search3',
            'needle' => $article,
        ]);
    }

    public function searchStparts(Request $request)
    {
        $stparts = new Stparts();
        $data = $stparts->searchBrands($request->code);

        $html = view('front.pages.stparts._parts._brands', ['brands' => $data])->render();

        return response()->json(['status' => true, 'html' => $html]);
    }

    public function codeBrand($brand, $code)
    {
        $client_id = 0;
        if (Auth::check()) {
            $client_id = Auth::user()->id;
        }
        $stparts = new Stparts();
        $data = $stparts->searchBrandsAndCodes($code, $brand);
        $markup = Markup::select('value')->where('param', 'stparts')->first();
        $data = is_null($data) ? [] : $data;

        $html = view('front.pages.stparts._parts._codeBrand', ['products' => $data, 'client_id' => $client_id, 'markup' => $markup->value])->render();

        return view('front.pages.stparts.search', [
            'result' => $html,
            'active' => 'search4',
            'needle' => $code,
        ]);
    }

    /**
     * @param $request
     * @return mixed|null|string
     * @throws \Throwable
     */
    public function searchByTrinityPartsWS($request)
    {
        $needle = $request->pin;
        $ws = new TrinityPartsWS('6213A8959A9A96589CA484DFD1E25053');
        $products = $ws->searchBrands($needle);

        return view('front.pages.trinity._search', [
            'products' => $products['data']
        ])->render();
    }
}
