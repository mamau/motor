<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;

/**
 * Class CategoriesController
 * @package App\Http\Controllers\Front
 */
class CategoriesController extends Controller
{
    /**
     * Показывает все активные категории
     */
    public function index()
    {
        $categories = Category::where('status', Category::STATUS_ACTIVE)
            ->where('parent_cat', 0)
            ->orderBy('sort')
            ->with('childrenCategory')
            ->get();

        return view('front.pages.categories.index', compact('categories'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($id)
    {
        $category = Category::where('status', Category::STATUS_ACTIVE)
            ->where('id', $id)
            ->with(['childrenCategory' => static function ($query) {
                $query->orderBy('sort');
            }, 'seo', 'parentCategory'])
            ->firstOrFail();

        $products = $category->products()->paginate(16);

        return view('front.pages.categories.show', [
            'current_category' => $category,
            'products' => $products,
        ]);
    }
}
