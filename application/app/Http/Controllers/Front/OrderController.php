<?php

namespace App\Http\Controllers\Front;

use App\Events\OrderNotification;
use App\Http\Requests\Front\OrderRequest;
use App\Models\Foreign_good;
use App\Models\Foreign_good_param;
use App\Models\Foreign_order;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
	public function makeOrder(OrderRequest $request)
	{
		$cartContent = Cart::content();
		$sum = number_format((float)str_replace(",","",$request->sum), 2, '.', '');

		$order = Foreign_order::create([
					'user_id' => $request->user_id,
					'sum' => $sum,
					'status' => 0
				]);

		foreach ($cartContent as $product){
			$f_good = Foreign_good::create([
						'foreign_order_id' => $order->id,
						'product_id' => $product->id,
						'name' => $product->name,
						'price' => $product->price,
						'count' => $product->qty,
						'client_type' => $product->options['client_type']
					]);

			if(isset($product->options['DeliveryPercent']) || isset($product->options['KEYZAK']) || isset($product->options['nomer_po_katalogu']) || isset($product->options['marka']) || isset($product->options['vendor_id']) || isset($product->options['distributorId']) || isset($product->options['bra_id'])){
				foreach ($product->options as $key => $option){
					Foreign_good_param::create([
						'foreign_good_id' => $f_good->id,
						'param' => $key,
						'value' => $option
					]);
				}
			}

		}

		event(new OrderNotification($order));

		return response()->json(['status' => true, 'order_id' => $order->id]);
	}
}
