<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\FeedbackRequest;
use App\Http\Requests\Front\OrdercallRequest;
use App\Models\Category;
use App\Models\sape\SAPE_client;
use App\Models\Settings;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\Exception;

class IndexController extends Controller
{
    public function index()
    {

        $sape = new SAPE_client();

        $categories = Category::where('status', 1)->where('parent_cat', 0)->orderBy('sort')->with('thumb')->get();

        return view('front.pages.index', [
            'categories' => $categories,
            'sape' => $sape,
        ]);
    }

    public function orderCall(OrdercallRequest $request)
    {
        $adminEmail = Settings::select('email')->first();

        try {
            Mail::send('emails.ordercall', ['name' => $request->name, 'phone' => $request->phone], function ($message) use ($adminEmail) {

                $message->to($adminEmail->email)->subject('Заказан звонок');
            });
            return response()->json(['status' => true]);
        } catch (Exception $e) {

            Log::error($e->getMessage());
            return response()->json(['status' => false]);
        }
    }

    public function feedback(FeedbackRequest $request)
    {
        $adminEmail = Settings::select('email')->first();

        try {
            Mail::send('emails.feedback', ['name' => $request->name, 'email' => $request->email, 'question' => $request->question], function ($message) use ($adminEmail) {

                $message->to($adminEmail->email)->subject('Задан вопрос');
            });
            return response()->json(['status' => true]);
        } catch (Exception $e) {

            Log::error($e->getMessage());
            return response()->json(['status' => false]);
        }
    }
}
