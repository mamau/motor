<?php

namespace App\Http\Controllers\Front;

use App\Models\Markup;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\ArmtekRestClient\Http\Config\Config as ArmtekRestClientConfig;
use App\Http\ArmtekRestClient\Http\ArmtekRestClient as ArmtekRestClient;
use Illuminate\Support\Facades\Auth;

class ArmtekController extends Controller
{
	protected $login_params;
	protected $armtek;
	protected $vkorg = '4000';
	protected $kunnr_rg = '43051818';
	protected $pin = '';
	protected $brand = '';
	protected $query_type = 2;
	protected $kunnr_za = '48003106';
	protected $incoterms = '';
	protected $vbeln = '40050142';
	protected $client_id = 0;
	protected $limit = 5;
	protected $markup = 0;


	public function __construct(Request $request)
	{
		$this->login_params = [
			'user_login' => config('armtek.user_login'),
			'user_password' => config('armtek.user_password')
		];
		$markup = Markup::select('value')->where('param','armtek')->first();
		$this->markup = $markup->value;

		$armtek_client_config = new ArmtekRestClientConfig($this->login_params);
		$this->armtek = new ArmtekRestClient($armtek_client_config);

		if(empty($this->vkorg)){
			$this->getVkorg();
		}
		if(empty($this->kunnr_rg)){
			$this->getUserInfo();
		}

		$this->middleware(function($request, $next){
			if(Auth::check()){

				$this->client_id = Auth::user()->id;
			}
			return $next($request);
		});

	}

	public function search(Request $request)
    {
	    if(Auth::check()){

		    $this->client_id = Auth::user()->id;
	    }

    	$this->setParams($request);

	    $request_params = [
		    'url' => 'search/search',
		    'params' => [
			    'VKORG'         => $this->vkorg
			    ,'KUNNR_RG'     => $this->kunnr_rg
			    ,'PIN'          => $this->pin
			    ,'BRAND'        => $this->brand
			    ,'QUERY_TYPE'   => $this->query_type
			    ,'KUNNR_ZA'     => $this->kunnr_za
			    ,'INCOTERMS'    => $this->incoterms
			    ,'VBELN'        => $this->vbeln
			    ,'format'       => 'json'
		    ]
	    ];

	    // send data
	    $response = $this->armtek->post($request_params);
		$json_responce_data = $response->json();
		$products = $json_responce_data->RESP;
	    if(isset($products->MSG)){
		    $products = [];
	    }
	    $products = array_slice($products, 0, $this->limit);


	    return view('front.pages.armtek._search', ['products' => $products,'client_id' => $this->client_id, 'markup' => $this->markup])->render();

    }

    public function createOrder($products)
    {
		//dd($this->vkorg, $this->kunnr_rg);
	    $request_params = [

		    'url' => 'order/createOrder',
		    'params' => [
			    'VKORG'             => $this->vkorg
			    ,'KUNRG'            => $this->kunnr_rg
			    ,'KUNWE'            => ''
			    ,'KUNZA'            => ''
			    ,'INCOTERMS'        => ''
			    ,'PARNR'            => ''
			    ,'VBELN'            => ''
			    ,'TEXT_ORD'         => ''
			    ,'TEXT_EXP'         => ''
			    ,'DBTYP'            => 3
			    ,'ITEMS'        => $products
			    ,'format'       => 'json'
		    ]

	    ];
	    $response = $this->armtek->post($request_params);

	    $json_responce_data = $response->json();
    }

	public function addBasket(Request $request)
	{
		Cart::add($request->ARTID, $request->NAME, $request->COUNT, $request->PRICE, [
			'PIN' => $request->PIN,
			'maxcount' => 100,
			'mincount' => (int)$request->MINBM,
			'BRAND' => $request->BRAND,
			'NAME' => $request->NAME,
			'ARTID' => $request->ARTID,
			'PARNR' => $request->PARNR,
			'KEYZAK' => $request->KEYZAK,
			'RVALUE' => $request->RVALUE,
			'RDPRF' => $request->RDPRF,
			'MINBM' => $request->MINBM,
			'VENSL' => $request->VENSL,
			'PRICE' => $request->PRICE,
			'WAERS' => $request->WAERS,
			'DLVDT' => $request->DLVDT,
			'ANALOG' => $request->ANALOG,
			'COUNT' => $request->COUNT,
			'markup' => $request->markup,
			'startprice' => $request->startprice,
			'client_type' => 'armtek'
		]);

		$list = $this->productsList();

		return response()->json(['status' => true, 'data' => ['name' => $request->NAME,'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
	}

	protected function productsList(){

		return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
	}

    protected function getVkorg()
    {

	    // requeest params for send
	    $request_params = [
		    'url' => 'user/getUserVkorgList',
	    ];

	    // send data
	    $response = $this->armtek->get($request_params);
	    $json_responce_data = $response->json();
		$this->vkorg = $json_responce_data->RESP[0]->VKORG;
    }

    protected function getUserInfo()
    {
	    $request_params = [

		    'url' => 'user/getUserInfo',
		    'params' => [
			    'VKORG'         => $this->vkorg
			    ,'STRUCTURE'    => 1
			    ,'FTPDATA'      => 0
			    ,'format'       => 'json'
		    ]

	    ];

	    // send data
	    $response = $this->armtek->post($request_params);

	    $json_responce_data = $response->json();
	    $this->kunnr_rg = $json_responce_data->RESP->STRUCTURE->RG_TAB[0]->KUNNR;
    }

	protected function setParams($request)
	{
		foreach($request->all() as $key => $val){
			$this->{$key} = $val;
		}
	}
}
