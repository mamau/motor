<?php

namespace App\Http\Controllers\Front;

use App\Http\ArmtekRestClient\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;

/**
 * Class PageController
 * @package App\Http\Controllers\Front
 */
class PageController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $page = Page::where('id', $id)->with('seo')->first();
        if ($page) {
            return view('front.pages.page.show', ['page' => $page]);
        }
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getAllPages($type)
    {
        $pages = Page::where('menu', $type)->where('parent_page', 0)->with('childrenPage')->orderBy('sort', 'asc')->get();
        return $pages;
    }

    /**
     * @param $param
     * @internal param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alias($param)
    {
        $page = Page::query()->where('alias', $param)->first();
        if ($page) {
            return view('front.pages.page.show', ['page' => $page]);
        } else {
            abort(404);
        }
    }
}
