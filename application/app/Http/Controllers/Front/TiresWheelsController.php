<?php

namespace App\Http\Controllers\Front;

use App\Models\Tire;
use App\Models\User;
use App\Models\Wheel;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TiresWheelsController extends Controller
{
    public function indexWheels()
    {
		$wheels = Wheel::orderBy('price')->where('price','>',1500)->paginate(15);

		if(Auth::check()){
		    $client_id = Auth::user()->id;
	    }else{
		    $client_id = 0;
	    }
    	return view('front.pages.wheels.index',['wheels' => $wheels, 'client_id' => $client_id,'path' => []]);
    }

    public function indexTires()
    {
	    $tires = Tire::orderBy('price')->where('price','>',1500)->paginate(15);

	    if(Auth::check()){
		    $client_id = Auth::user()->id;
	    }else{
		    $client_id = 0;
	    }

	    return view('front.pages.tires.index',['tires' => $tires, 'client_id' => $client_id, 'path' => []]);
    }

    public function showWheel(Request $request)
    {
    	$wheel = Wheel::find($request->id);

    	return response()->json(['status' => true, 'data' => $wheel]);
    }

	public function showTire(Request $request)
	{
		$tire = Tire::find($request->id);

		return response()->json(['status' => true, 'data' => $tire]);
	}

	public function basketTires(Request $request)
	{
		$product = Tire::find($request->id);

		Cart::add($product->id, $product->name, $request->count, $product->price, [
			'maxcount' => 100,
			'mincount' => 4,
			'vendor_id' => $product->vendor_id,
			'name' => $product->name,
			'code_1c' => $product->code_1c,
			'code' => $product->code,
			'width' => $product->width,
			'height' => $product->height,
			'diameter' => $product->diameter,
			'index_speed' => $product->index_speed,
			'index_load' => $product->index_load,
			'type' => $product->type,
			'season' => $product->season,
			'brand' => $product->brand,
			'model' => $product->model,
			'multiplicity' => $product->multiplicity,
			'price' => $product->price,
			'client_type' => 'tire'
		]);

		$list = $this->productsList();

		return response()->json(['status' => true, 'data' => ['name' => $product->name,'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
	}

    public function basketWheel(Request $request)
    {
	    $product = Wheel::find($request->id);

	    Cart::add($product->id, $product->name, $request->count, $product->price, [
		    'maxcount' => 100,
		    'mincount' => 4,
		    'vendor_id' => $product->vendor_id,
		    'name' => $product->name,
		    'code_1c' => $product->code_1c,
		    'width' => $product->width,
		    'diameter' => $product->diameter,
		    'diameter_hole' => $product->diameter_hole,
		    'outreach' => $product->outreach,
		    'diameter_central' => $product->diameter_central,
		    'color1' => $product->color1,
		    'color2' => $product->color2,
		    'type' => $product->type,
		    'brand' => $product->brand,
		    'model' => $product->model,
		    'price' => $product->price,
		    'client_type' => 'wheel'
	    ]);

	    $list = $this->productsList();

	    return response()->json(['status' => true, 'data' => ['name' => $product->name,'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
    }

	protected function productsList(){

		return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
	}

	public function filterWheels(Request $request)
	{
		$wheels = Wheel::select('*')->orderBy('price')->where('price','>',1500);

		if(Auth::check()){
			$client_id = Auth::user()->id;
		}else{
			$client_id = 0;
		}

		if($request->diameter){
			$wheels->where('diameter', 'LIKE',$request->diameter.'%');
		}

		if($request->width){
			$wheels->where('width', $request->width);
		}

		if($request->diameter_hole){
			$wheels->where('diameter_hole', $request->diameter_hole);
		}

		if($request->outreach != 'null'){
			$wheels->where('outreach', $request->outreach);
		}

		if($request->type){
			$wheels->where('type', $request->type);
		}

		if($request->brand){
			$wheels->where('brand', $request->brand);
		}

		if($request->brand){
			$wheels->where('brand', $request->brand);
		}

		if($request->diameter_central){
			$wheels->where('diameter_central', $request->diameter_central);
		}

		$wheels = $wheels->paginate(15);

		return view('front.pages.wheels.index',['wheels' => $wheels, 'client_id' => $client_id, 'path' => $request->all()]);
	}

	public function filterTires(Request $request)
	{
		$tires = Tire::select('*')->orderBy('price')->where('price','>',1500);

		if(Auth::check()){
			$client_id = Auth::user()->id;
		}else{
			$client_id = 0;
		}

		if($request->width){
			$tires->where('width', $request->width);
		}

		if($request->height){
			$tires->where('height', $request->height);
		}

		if($request->diameter){
			$tires->where('diameter', $request->diameter);
		}

		if($request->season){
			$tires->where('season', $request->season);
		}

		if($request->brand){

			$tires->where('brand', $request->brand);
		}

		$tires = $tires->paginate(15);

		return view('front.pages.tires.index',['tires' => $tires, 'client_id' => $client_id, 'path' => $request->all()]);
	}
}
