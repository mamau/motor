<?php

namespace App\Http\Controllers\Front;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
	protected $client_id = 0;

	public function __construct()
	{
		$this->middleware(function($request, $next){
			if(Auth::check()){

				$this->client_id = Auth::user()->id;
			}
			return $next($request);
		});
	}

	public function show($id)
	{
	    /** @var Product $product */
    	$product = Product::where('id', $id)->with('seo','attributes')->firstOrFail();

    	return view('front.pages.products.show',[
    		'product' => $product,
		    'client_id' => $this->client_id,
	    ]);
    }
}
