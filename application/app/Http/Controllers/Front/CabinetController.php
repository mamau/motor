<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers;
use App\Http\Requests\Front\DialogRequest;
use App\Http\Requests\Front\PasswordRequest;
use App\Http\Requests\Front\ProfileRequest;
use App\Http\Requests\Front\QuestionRequest;
use App\Http\Requests\Front\ReturnRequest;
use App\Models\City;
use App\Models\Dialog;
use App\Models\Feedback;
use App\Models\Foreign_order;
use App\Models\ReturnGood;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CabinetController extends Controller
{
    public function index()
    {
    	$user = Auth::user();

        return view('front.pages.cabinet.index',['user' => $user]);
    }

	public function profile()
	{
		$user = Auth::user();
		$userParams = $user->userParams()->first();
		$params = [];
		if(!is_null($userParams)){
			$params['surname'] = $userParams->surname;
			$params['city_id'] = $userParams->city_id;
			$params['gender'] = $userParams->gender;
			$params['birthday'] = $userParams->birthday;
			$params['address'] = $userParams->address;
			$city = City::where('id', $params['city_id'])->first();

		}else{
			$params['surname'] = '';
			$params['city_id'] = '';
			$params['gender'] = '';
			$params['birthday'] = '';
			$params['address'] = '';
			$city = null;
		}

		return view('front.pages.cabinet.profile',['user' => $user, 'params' => collect($params), 'city' => $city]);
	}

	public function update(ProfileRequest $request, $id)
	{
		$user = User::find($id);
		$user->update([
			'name' => $request->name,
			'email' => $request->email,
			'phone' => Helpers::normalizePhone($request->phone)
		]);

		$params = $user->userParams()->first();
		if(is_null($params)){
			$user->userParams()->create([
				'surname' => $request->params['surname'],
				'city_id' => $request->params['city'],
				'gender' => $request->params['gender'],
				'birthday' => Carbon::parse($request->params['birthday']),
				'address' => $request->params['address'],
			]);
		}else{
			$user->userParams()->update([
				'surname' => $request->params['surname'],
				'city_id' => $request->params['city'],
				'gender' => $request->params['gender'],
				'birthday' => Carbon::parse($request->params['birthday']),
				'address' => $request->params['address'],
			]);
		}

		Session::flash('success','Профиль успешно обновлён');
		return redirect()->route('cabinet.profile');
	}

	public function password()
	{
		$user = Auth::user();

		return view('front.pages.cabinet.password',['user' => $user]);
	}

	public function updatePassword(PasswordRequest $request, $id)
	{
		$user = User::find($id);
		$user->update([
			'password' => Hash::make($request->password)
		]);

		Session::flash('success','Пароль успешно обновлён');
		return redirect()->route('cabinet.password');
	}

	public function feedback()
	{
		$user = Auth::user();
		$feedbacks = Feedback::where('author_id', $user->id)->where('closed', 0)->get();

		return view('front.pages.cabinet.feedback',['user' => $user, 'feedbacks' => $feedbacks]);
	}

	public function question(QuestionRequest $request)
	{
		$feedback = Feedback::create([
						'author_id' => $request->author_id,
						'title' => $request->title
					]);
		Dialog::create([
			'feedback_id' => $feedback->id,
			'text' => $request->text,
			'author_id' => $request->author_id
		]);

		Session::flash('success','Вы успешно задали свой вопрос. В ближайшее время вам ответят');
		return redirect()->route('cabinet.feedback');
	}

	public function dialogIndex($id)
	{
		$feedback = Feedback::where('id', $id)->with('dialog')->first();

		return view('front.pages.cabinet.dialog',['feedback' => $feedback]);
	}

	public function dialogStore(DialogRequest $request, $id)
	{
		Dialog::create([
			'feedback_id' => $id,
			'author_id' => $request->author_id,
			'text' => $request->text,
		]);

		Session::flash('success','Ответ успешно добавлен');
		return redirect()->route('dialog.index',['id' => $id]);
	}

	public function orders()
	{
		$user = Auth::user();
		$orders = Foreign_order::with('statuz')->where('user_id', $user->id)->orderBy('id','desc')->get();
		return view('front.pages.cabinet.order',['orders' => $orders]);
	}

	public function orderShow($id)
	{
		$order = Foreign_order::where('id', $id)->with('foreignGoods.foreignGoodParams')->first();
		return view('front.pages.cabinet.orderShow',['order' => $order]);
	}

	public function returnIndex()
	{
		$user = Auth::user();

		$returnGoods = ReturnGood::where('user_id', $user->id)->get();
		foreach ($returnGoods as $good){
			$title = Foreign_order::select('created_at')->where('id',$good->order_id)->first();
			$good->orderTitle = $title->created_at;
		}
		$returnGoods = $returnGoods->toArray();

		return view('front.pages.cabinet.return.index', ['returns' => $returnGoods]);
	}

	public function returnCreate()
	{
		$user = Auth::user();
		$orders = Foreign_order::where('user_id', $user->id)->where('status', 1)->get();

		return view('front.pages.cabinet.return.create',['orders' => $orders]);
	}

	public function returnShow($id)
	{
		$return = ReturnGood::where('id', $id)->with('thumbs')->first();

		return view('front.pages.cabinet.return.show',['return' => $return]);
	}

	public function returnAction(ReturnRequest $request)
	{
		$user = Auth::user();
		$returnGood = ReturnGood::create([
			'user_id' => $user->id,
			'order_id' => $request->orders,
			'cause' => $request->cause,
			'description' => $request->description
		]);

		if(isset($request->thumb)){
			foreach ($request->thumb as $file){
				$returnGood->setManyThumbs($file, 'ReturnGood', $returnGood->id);
			}
		}

		Session::flash('success','Заявка создана. В ближайшее время с вами свяжется администратор');
		return redirect()->route('cabinet.return');
	}
}
