<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\Front\CartRequest;
use App\Models\Foreign_order;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addCart(CartRequest $request)
    {

        Cart::add($request->id, $request->name, $request->quantity, $request->price, ['maxcount' => $request->max, 'mincount' => $request->min, 'client_type' => 'motor']);

        $list = $this->productsList();

        return response()->json(['status' => true, 'data' => ['name' => $request->name, 'count' => Cart::count(), 'total' => Cart::subtotal(), 'html' => $list]]);
    }

    public function getCart()
    {

        Cart::content()->groupBy('id');
    }

    public function destroyProduct(Request $request)
    {

        Cart::remove($request->id);
        return response()->json(['status' => true, 'data' => ['count' => Cart::count(), 'total' => Cart::subtotal()]]);
    }

    protected function productsList()
    {

        return view('front.pages.cart._cart.productsList', ['cartContent' => Cart::content(), 'cartSubtotal' => Cart::subtotal()])->render();
    }

    public function index()
    {

        $cartContent = Cart::content();
        $total = Cart::subtotal();

//	    $order = Foreign_order::find(11);
//	    $goodsArmtek = $order->foreignGoods()
//		    ->where('client_type','armtek')
//		    ->get();
//
//		$productsArmtekToBasket = [];
//	    if($goodsArmtek){
//		    foreach ($goodsArmtek as $good){
//			    $currentProduct = array_pluck($good->foreignGoodParams()->get(),'value','param');
//			    $productsArmtek['PIN'] = $currentProduct['PIN'];
//			    $productsArmtek['BRAND'] = $currentProduct['BRAND'];
//			    $productsArmtek['KWMENG'] = (int)$currentProduct['COUNT'];
//			    $productsArmtek['KEYZAK'] = $currentProduct['KEYZAK'];
//			    $productsArmtek['PRICEMAX'] = $currentProduct['PRICE'];
//			    $productsArmtek['DATEMAX'] = $currentProduct['DLVDT'];
//			    $productsArmtek['COMMENT'] = '';
//			    $productsArmtek['COMPL_DLV'] = '';
//			    $productsArmtekToBasket[] = $productsArmtek;
//		    }
//		    app('App\Http\Controllers\Front\ArmtekController')->createOrder($productsArmtekToBasket);
//	    }
//        dd($cartContent);
        return view('front.pages.cart.index', ['cartContent' => $cartContent, 'total' => $total]);
    }

    public function updateCart(Request $request)
    {
        $cart = Cart::update($request->rowId, $request->count);
        if ($cart->options->has('Count')) {

            $cart->options['Count'] = $request->count;
        }

        $current_product = Cart::get($request->rowId);
        $total = Cart::subtotal();

        return response()->json(['status' => true, 'rowid' => $request->rowId, 'product' => $current_product, 'total' => $total, 'productcount' => Cart::count()]);
    }
}
