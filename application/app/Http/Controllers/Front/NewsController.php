<?php

namespace App\Http\Controllers\Front;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class NewsController
 * @package App\Http\Controllers\Front
 */
class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = News::with('thumb')->orderBy('id', 'desc')->paginate(10);

        return view('front.pages.news.index', ['news' => $news]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $news = News::where('id', $id)->with('seo')->first();

        return view('front.pages.news.show', ['news' => $news]);
    }
}
