<?php

namespace App\Http\Controllers\Front;

use App\Core\SocialCore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class SocialController
 * @package App\Http\Controllers\Front
 */
class SocialController extends Controller
{
    protected $socialCore;

    /**
     * SocialController constructor.
     */
    public function __construct()
    {
        $this->socialCore = new SocialCore();
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function login($provider)
    {
        return Socialite::with($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback($provider)
    {
        $driver   = Socialite::driver($provider);
        $user = $this->socialCore->createOrGetUser($driver, $provider);
        Auth::login($user, true);
        return redirect()->intended('/');
    }
}
