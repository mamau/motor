<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\Front\ForgotRequest;
use App\Http\Requests\Front\RegisterRequest;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Ultraware\Roles\Models\Role;

class AuthController extends Controller
{
	use AuthenticatesUsers, SendsPasswordResetEmails;

	protected $redirectTo = '/cabinet';

	public function __construct(){

		$this->redirectTo = url()->previous();
		$this->middleware('guest', ['except' => 'logout']);
	}

	public function showLoginForm()
	{
		return view('front.pages.index');
	}

	public function registerIndex(){

		if(Auth::check()){
			return redirect()->route('cabinet.index');
		}else{
			return view('front.pages.auth.register');
		}
	}

	public function register(RegisterRequest $request){

		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => bcrypt($request->password),
		]);

		$clientRole = Role::where('slug','client')->first();
		if(!is_null($clientRole)){
			$user->attachRole($clientRole);
		}

		if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
			return redirect()->route('cabinet.index');
		}

	}

	/**
	 * Вход пользователя, проверка на валидность и если брутят форму, то сработает throttle
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 */
	public function login(Request $request){

		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);

			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) {

			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	/**
	 * Выход пользователя
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->flush();

		$request->session()->regenerate();

		return redirect()->route('index');
	}

	public function forgotIndex()
	{
		return view('front.pages.auth.forgot');
	}

	public function forgot(ForgotRequest $request)
	{

		// We will send the password reset link to this user. Once we have attempted
		// to send the link, we will examine the response then see the message we
		// need to show to the user. Finally, we'll send out a proper response.
		$response = $this->broker()->sendResetLink(
			$request->only('email')
		);

		if ($response === Password::RESET_LINK_SENT) {
			return back()->with('status', trans($response));
		}

		// If an error was returned by the password broker, we will get this message
		// translated so we can notify a user of the problem. We'll redirect back
		// to where the users came from so they can attempt this process again.
		return back()->withErrors(
			['email' => trans($response)]
		);
	}

	public function showLinkRequestForm()
    {

    }

    public function showResetForm(Request $request, $token = null)
    {

        return view('front.pages.auth.passwords.reset', ['token' => $token, 'email' => $request->email]);
    }

}
