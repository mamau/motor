<?php

namespace App\Http\Controllers\Front;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function search(Request $request)
    {
    	$needle = $request['query']['term'];
	    $tek_items = City::select('id as value', 'id','city as text', 'region')->where('city', 'LIKE', $needle.'%')->get();
		$output = $tek_items->toArray();

	    foreach ($output as &$city){
	    	if(!empty($city['region'])){
	    	    $city['text'] = $city['text'].' ('.$city['region'].')';
		    }
	    }


	    return response()->json([$output]);
    }

    public function getCity(Request $request)
    {
    	$city = City::select('id as value', 'city as text')->where('id',$request->value)->first();
    	return response()->json([$city]);
    }
}
