<?php

namespace App\Http\Controllers\Front;

use App\Models\Markup;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Avtoto\avtoto_parts;
use Illuminate\Support\Facades\Auth;

class AvtotoController extends Controller
{
	protected $login_params;
	protected $avtoto;
	protected $max_time = 15;
	protected $search_analogs = 'on';
	protected $limit = 5;
	protected $client_id = 0;
	protected $markup = 0;

	public function __construct(Request $request)
	{
		$this->login_params = [
			'user_id' => config('avtoto.user_id'),
			'user_login' => config('avtoto.user_login'),
			'user_password' => config('avtoto.user_password'),
		];

		$markup = Markup::select('value')->where('param','avtoto')->first();
		$this->markup = $markup->value;
		$this->middleware(function($request, $next){
			if(Auth::check()){

				$this->client_id = Auth::user()->id;
			}
			return $next($request);
		});

		//$this->search_analogs = (!is_null($request->analog)) ? 'on' : 'off';
		$this->avtoto = new avtoto_parts($this->login_params);
		$this->avtoto->set_search_extension_time_limit($this->max_time);
	}

	/**
	 * Поиск предложений
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function search(Request $request)
    {
	    if(Auth::check()){

		    $this->client_id = Auth::user()->id;
	    }
	    $needle = $request->pin;
	    $result = $this->avtoto->get_parts($needle, $this->search_analogs, $this->limit);
	    foreach ($result['Parts'] as $key => &$part){
		    $part['RemoteID'] = random_int(00000000,99999999);
		    $part['SearchID'] = $result['Info']['SearchId'];
		    $part['PartId'] = $result['Parts'][$key]['AvtotoData']['PartId'];
		    $part['Count'] = 1;
	    }

	    return view('front.pages.avtoto._search', ['products' => $result['Parts'],'client_id' => $this->client_id,'markup' => $this->markup])->render();
    }

	/**
	 * Поиск предложений (Код+Бренд)
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

    public function searchWithBrand(Request $request)
    {
	    $needle = $request->pin;
	    $brand = $request->brand;
	    $result = $this->avtoto->get_parts_brand($needle, $brand,  $this->search_analogs, $this->limit);

	    return view('front.pages.avtoto.search', ['products' => $result['Parts'], 'needle' => $needle, 'active' => 'search3']);
    }

	/**
	 * Поиск предложений (только бренды)
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

    public function searcOnlyBrands(Request $request)
    {
	    $needle = $request->pin;
	    $result = $this->avtoto->get_brands_by_code($needle);

	    return view('front.pages.avtoto.search', ['products' => $result['Parts'], 'needle' => $needle, 'active' => 'search3']);
    }

    public function avtotoBasket($goods)
    {

    	$this->avtoto->add_to_basket($goods);

	    //Обрабатываем данные
//	    if(isset($data['DoneInnerId']) && $data['DoneInnerId']) {
//
//		    //Обрабатываем список предложений
//		    foreach($data['DoneInnerId'] as $added_part) {
//
//			    $our_id = $added_part['RemoteID'];
//			    $avtoto_id = $added_part['InnerID'];
//
//			    $list = $this->productsList();
//			    return response()->json(['status' => true, 'data' => ['name' => $request['Name'],'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
//		    }
//	    }
    }

    public function addBasket(Request $request)
    {
	    Cart::add($request['RemoteID'], $request['Name'], $request['Count'], $request['Price'], [
	    	'maxcount' => $request['MaxCount'],
		    'mincount' => $request['BaseCount'],
		    'Code' => $request['Code'],
		    'Manuf' => $request['Manuf'],
		    'Name' => $request['Name'],
		    'Price' => $request['Price'],
		    'Storage' => $request['Storage'],
		    'Delivery' => $request['Delivery'],
		    'StorageDate' => $request['StorageDate'],
		    'DeliveryPercent' => $request['DeliveryPercent'],
		    'RemoteID' => $request['RemoteID'],
		    'SearchID' => $request['SearchID'],
		    'PartId' => $request['PartId'],
		    'Count' => $request['Count'],
		    'markup' => $request['markup'],
		    'startprice' => $request['startprice'],
		    'client_type' => 'avtoto'
	    ]);

	    $list = $this->productsList();

	    return response()->json(['status' => true, 'data' => ['name' => $request['Name'],'count' => Cart::count(),'total' => Cart::subtotal(), 'html' => $list]]);
    }

	protected function productsList(){

		return view('front.pages.cart._cart.productsList',['cartContent' => Cart::content(),'cartSubtotal' => Cart::subtotal()])->render();
	}
}
