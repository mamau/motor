<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Request $request)
    {
	    if($request->ajax()){

		    $manufacturers = News::select('id','title')->get();

		    $returnData = ['data' => []];

		    if($returnData){

			    foreach ($manufacturers as $manufacturer){

				    $returnData['data'][] = array(
					    view('admin.pages.news._parts.indexCheckbox',['id' => $manufacturer->id])->render(),
					    $manufacturer->id,
					    $manufacturer->title,
					    view('admin.pages.news._parts.indexEdit',['id' => $manufacturer->id])->render()
				    );
			    }

		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }

	    return view('admin.pages.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.pages.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
	    $dataSeo = [];
	    $news = News::create([
		    'title' => $request->title,
		    'description' => $request->description
	    ]);
	    if($request->hasFile('thumb')){
		    $news->setThumb($request->thumb, $request->type, $news->id);
	    }

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->description;

	    $news->saveSeo($dataSeo, $request->type, $news->id);

	    Session::flash('success','Новость успешно добавлена');
	    return redirect()->route('news.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$obj = News::find($id);
	    $news = News::where('id',$id)->with('seo')->first();
	    $data = [];
	    $data['id'] = $news->id;
	    $data['title'] = $news->title;
	    $data['description'] = $news->description;

	    if(!is_null($news->seo)){
		    $data['title_seo'] = $news->seo->title_seo;
		    $data['keywords_seo'] = $news->seo->keywords_seo;
		    $data['description_seo'] = $news->seo->description_seo;
	    }else{
		    $data['title_seo'] = '';
		    $data['keywords_seo'] = '';
		    $data['description_seo'] = '';
		    $data['full_description'] = '';
	    }


	    return view('admin.pages.news.edit', ['data' => $data, 'news' => $obj]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
	    $dataSeo = [];
	    $news = News::find($id);
	    $news->update([
		    'title' => $request->title,
		    'description' => $request->description
	    ]);

	    if(isset($request->thumb)){
		    $news->setThumb($request->thumb, $request->type, $news->id);
	    }

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->description;

	    $news->saveSeo($dataSeo, $request->type, $news->id);

	    Session::flash('success','Новость успешно обновлёна');
	    return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
	    $news_ids = json_decode($request->ids);

	    foreach ($news_ids as $id){

		    News::find($id)->delete();
	    }
	    return response()->json(['status' => true]);
    }
}
