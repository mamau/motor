<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UsersRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Helpers;
use Illuminate\Support\Facades\Session;
use Ultraware\Roles\Models\Role;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	if($request->ajax()){

    		$roleWithUsers = Role::where('slug','<>','admin')->with('users')->get();


		    $returnData = ['data' => []];

		    if($roleWithUsers){

		    	foreach ($roleWithUsers as $uLists){
				    foreach ($uLists->users as $user){


					    $returnData['data'][] = array(
						    view('admin.pages.users._parts.indexCheckbox',['id' => $user->id])->render(),
						    $user->id,
						    $user->name,
						    $user->email,
						    $user->phone,
						    $uLists->name,
						    view('admin.pages.users._parts.indexEdit',['id' => $user->id])->render()
					    );
				    }
			    }
		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }
        return view('admin.pages.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$roles = Role::all();
        return view('admin.pages.users.create',['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $user = User::create([
		            'name' => $request->name,
			        'email' => $request->email,
			        'phone' => Helpers::normalizePhone($request->phone),
			        'password' => Hash::make($request->password)
		        ]);
        $user->attachRole($request->role);
		Session::flash('success','Пользователь успешно добавлен');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $userRole = $user->getRoles()->first();

	    $roles = Role::all();
        return view('admin.pages.users.edit', ['user' => $user, 'roles' => $roles, 'userRole' => $userRole]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersRequest $request, $id)
    {
    	$user = User::find($id);
        $user->update([
		        'name' => $request->name,
		        'email' => $request->email,
		        'phone' => Helpers::normalizePhone($request->phone),
		        'password' => Hash::make($request->password)
	        ]);

	    $user->detachAllRoles();
	    $user->attachRole($request->role);
	    Session::flash('success','Пользователь успешно обновлён');
	    return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
	    Session::flash('danger','Пользователь успешно удалён');
	    return redirect()->route('users.index');
    }

	/**
	 * Удаление нескольких пользователей
	 *
	 * @param UsersRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function destroyAll(UsersRequest $request){

	    $offers_ids = json_decode($request->ids);
	    foreach ($offers_ids as $offer){


	    	if($request->force == 'true'){

	    		User::where('id',$offer)->onlyTrashed()->forceDelete();
		    }else{

		        User::find($offer)->delete();
		    }
	    }
	    return response()->json(['status' => true]);
    }

	/**
	 * Корзина с удалёнными пользователями
	 *
	 * @param Request $request
	 * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function trash(Request $request){

	    if($request->ajax()){

		    $users = User::onlyTrashed()->get();

		    $returnData = ['data' => []];

		    if($users){

			    foreach ($users as $user){

				    $returnData['data'][] = array(
					    view('admin.pages.users._parts.trashCheckbox',['id' => $user->id])->render(),
					    $user->id,
					    $user->name,
					    $user->email,
					    $user->phone,
					    view('admin.pages.users._parts.trashIdrow',['id' => $user->id])->render()
				    );
			    }

		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }

	    return view('admin.pages.users.trash');
    }

	/**
	 * Восстановление пользоваателей
	 *
	 * @param UsersRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function restoreUser(UsersRequest $request){

	    $offers_ids = json_decode($request->ids);
	    foreach ($offers_ids as $offer){

		    User::where('id',$offer)->onlyTrashed()->restore();
	    }
	    return response()->json(['status' => true]);
    }
}
