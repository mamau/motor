<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductsRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\StockStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

/**
 * Class ProductsController
 * @package App\Http\Controllers\Admin
 */
class ProductsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::with(['categories', 'stockStatus'])->paginate(10);
        $categories = Category::select('id', 'name')->orderBy('name')->get();

        return view('admin.pages.products.index', [
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request, Product $product)
    {
        $products = $product->newQuery();
        if ($request->has('id')) {
            $products->where('id', $request->input('id'));
        }
        if ($request->has('id1c')) {
            $products->where('id1c', $request->input('id1c'));
        }

        if ($request->has('name')) {
            $products->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if ($request->has('category') && $request->category != 0) {
            $products->whereHas('categories', function ($query) use ($request) {
                $query->where('id', $request->category);
            });
        }

        $products = $products->get();

        $categories = Category::select('id', 'name')->orderBy('name')->get();
        $products->links = false;

        return view('admin.pages.products.index', [
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    /**
     * Выводит список категорий которым принадлежит товар
     *
     * @param $product
     * @return string
     */
    protected function getCategories($product)
    {

        $cats = '';
        if (count($product->categories) > 0) {
            $count_elems = $product->categories->count();

            foreach ($product->categories as $key => $category) {

                if (($key + 1) != $count_elems) {
                    $cats .= $category->name . ', ';
                } else {
                    $cats .= $category->name;
                }

            }
        }
        return $cats;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $cats = Category::where('status', Category::STATUS_ACTIVE)->get();
        $manufacturers = Manufacturer::all();
        $stock_status = StockStatus::all();

        return view('admin.pages.products.create', [
            'categories' => $cats,
            'manufacturers' => $manufacturers,
            'stock_status' => $stock_status,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        $dataSeo = [];
        $product = Product::create([
            'name' => $request->name,
            'model_product' => $request->model_product,
            'sort_order' => $request->sort_order,
            'price' => $request->price,
            'minimum' => $request->minimum,
            'sku' => $request->sku,
            'status' => $request->status,
            'quantity' => $request->quantity,
            'stock_status_id' => $request->stock_status_id,
            'manufacturer_id' => $request->manufacturer_id,
            'date_available' => Carbon::parse($request->date_available),
            'special' => $request->special,
        ]);

        $product->categories()->attach($request->category_id);


        if (isset($request->thumb)) {
            $product->setThumb($request->thumb, $request->type, $product->id);
        }

        if (isset($request->attr_name) && isset($request->attr_value)) {
            $collect = collect($request->attr_name);
            $combine = $collect->combine($request->attr_value);
            foreach ($combine as $name => $val) {

                $attr = new Attribute();
                $attr->name = $name;
                $attr->value = $val;
                $product->attribute()->save($attr);
            }
        }

        $dataSeo['title_seo'] = $request->title_seo;
        $dataSeo['keywords_seo'] = $request->keywords_seo;
        $dataSeo['description_seo'] = $request->description_seo;
        $dataSeo['full_description'] = $request->full_description;

        $product->saveSeo($dataSeo, $request->type, $product->id);

        Session::flash('success', 'Товар успешно добавлен');
        return redirect()->route('products.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->with('seo', 'thumb', 'categories', 'manufacturer', 'attributes')->first();
        $manufacturers = Manufacturer::all();
        $stock_status = StockStatus::all();
        $categories = Category::where('status', Category::STATUS_ACTIVE)->get();

        $data = [];
        $data['id'] = $product->id;
        $data['name'] = $product->name;
        $data['special'] = $product->special;
        $data['model_product'] = $product->model_product;
        $data['sku'] = $product->sku;
        $data['price'] = $product->price;
        $data['quantity'] = $product->quantity;
        $data['minimum'] = $product->minimum;
        $data['manufacturer_id'] = $product->manufacturer_id;
        $data['stock_status_id'] = $product->stock_status_id;
        $data['date_available'] = Carbon::parse($product->date_available)->format('d.m.Y');
        $data['status'] = $product->status;
        $data['sort_order'] = $product->sort_order;

        if ($product->thumb !== null) {
            $data['thumb'] = asset($product->thumb->path . $product->thumb->name . '.' . $product->thumb->extension);
        } else {
            $data['thumb'] = asset('img/noimage.png');
        }

        if (count($product->attributes) > 0) {
            $data['attributes'] = $product->attributes->toArray();
        } else {
            $data['attributes'] = [];
        }

        if (count($product->seo) > 0) {
            $data['title_seo'] = $product->seo->title_seo;
            $data['keywords_seo'] = $product->seo->keywords_seo;
            $data['description_seo'] = $product->seo->description_seo;
            $data['full_description'] = $product->seo->full_description;
        } else {
            $data['title_seo'] = '';
            $data['keywords_seo'] = '';
            $data['description_seo'] = '';
            $data['full_description'] = '';
        }

        if (count($product->categories) > 0) {
            $data['categories'] = array_pluck($product->categories, 'id');
        } else {
            $data['categories'] = [];
        }


        return view('admin.pages.products.edit', [
            'data' => $data,
            'manufacturers' => $manufacturers,
            'stock_status' => $stock_status,
            'categories' => $categories,
        ]);
    }

    /**
     * @param ProductsRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductsRequest $request, $id)
    {
        $dataSeo = [];

        $product = Product::find($id);
        $product->update([
            'name' => $request->name,
            'model_product' => $request->model_product,
            'sort_order' => $request->sort_order,
            'price' => $request->price,
            'minimum' => $request->minimum,
            'sku' => $request->sku,
            'status' => $request->status,
            'quantity' => $request->quantity,
            'stock_status_id' => $request->stock_status_id,
            'manufacturer_id' => $request->manufacturer_id,
            'date_available' => Carbon::parse($request->date_available),
            'special' => $request->special,
        ]);
        $product->categories()->sync($request->category_id);


        if (isset($request->thumb)) {
            $product->setThumb($request->thumb, $request->type, $product->id);
        }

        if (isset($request->attr)) {

            foreach ($request->attr as $attr) {
                $currAttr = Attribute::where('id', $attr['id'])->first();
                $currAttr->update([
                    'name' => $attr['name'],
                    'value' => $attr['value'],
                ]);
            }
        }

        if (isset($request->attr_name) && isset($request->attr_value)) {
            $collect = collect($request->attr_name);
            $combine = $collect->combine($request->attr_value);
            foreach ($combine as $name => $val) {

                $attr = new Attribute();
                $attr->name = $name;
                $attr->value = $val;
                $product->attribute()->save($attr);
            }
        }

        $dataSeo['title_seo'] = $request->title_seo;
        $dataSeo['keywords_seo'] = $request->keywords_seo;
        $dataSeo['description_seo'] = $request->description_seo;
        $dataSeo['full_description'] = $request->full_description;

        $product->saveSeo($dataSeo, $request->type, $product->id);

        Session::flash('success', 'Товар успешно добавлен');
        return redirect()->route('products.index');
    }

    /**
     * Удаление нескольких товаров
     *
     * @param ProductsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll(ProductsRequest $request)
    {

        $products_ids = json_decode($request->ids);
        foreach ($products_ids as $product) {


            if ($request->force == 'true') {

                $product = Product::where('id', $product)->onlyTrashed()->first();
                $product->forceDelete();
            } else {

                Product::find($product)->delete();
            }
        }
        return response()->json(['status' => true]);
    }

    /**
     * Корзина с удалёнными товарами
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trash(Request $request)
    {

        if ($request->ajax()) {

            $products = Product::onlyTrashed()->with(['categories', 'stockStatus'])->get();

            $returnData = ['data' => []];

            if ($returnData) {

                foreach ($products as $product) {

                    $returnData['data'][] = [
                        view('admin.pages.products._parts.trashCheckbox', ['id' => $product->id])->render(),
                        $product->id,
                        $product->name,
                        $this->getCategories($product),
                        $product->model_product,
                        $product->price,
                        view('admin.pages.products._parts.trashIdrow', ['id' => $product->id])->render() . ((count($product->stockStatus) > 0) ? $product->stockStatus->name : ''),
                    ];

                }

            } else {

                $returnData['data'][] = '';
            }
            return $returnData;
        }

        return view('admin.pages.products.trash');
    }

    /**
     * Восстановление товаров
     *
     * @param ProductsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreProduct(ProductsRequest $request)
    {

        $products_ids = json_decode($request->ids);
        foreach ($products_ids as $product) {

            $product = Product::where('id', $product)->onlyTrashed()->first();
            $product->restore();
        }
        return response()->json(['status' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function extractZIP(Request $request)
    {
        $zip = new \ZipArchive();
        if ($request->file('files') && $zip->open($request->file('files'))) {
            $path = public_path('images/zip');
            if (File::exists($path)) {
                File::cleanDirectory($path);
            } else if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true);
            }
            $zip->extractTo($path);
            $zip->close();
            if ($this->parseImages($path)) {
                Session::flash('success', 'Изображения успешно загружены');
            } else {
                Session::flash('error', 'Изображения не загружены');
            }
        } else {
            Session::flash('error', 'Не могу разобрать архив');
        }
        return redirect()->route('price.index');
    }

    /**
     * @param $path
     * @return bool
     */
    private function parseImages($path)
    {
        $files = File::allFiles($path);
        foreach ($files as $file) {
            $fullName = trim($file->getBasename());
            $name = explode('.', $fullName)[0];
            $product = Product::where('id_1c', $name)->first();
            if ($product) {
                $product->saveImgFromZip($file, 'product', $product->id);
            }
        }
        File::cleanDirectory($path);
        return true;
    }
}
