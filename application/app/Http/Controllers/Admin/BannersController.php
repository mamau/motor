<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BannerRequest;
use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    if($request->ajax()){

		    $manufacturers = Banner::select('id','name','type')->get();

		    $returnData = ['data' => []];

		    if($returnData){

			    foreach ($manufacturers as $manufacturer){

				    $returnData['data'][] = array(
					    view('admin.pages.banners._parts.indexCheckbox',['id' => $manufacturer->id])->render(),
					    $manufacturer->id,
					    $manufacturer->name,
					    $manufacturer->type,
					    view('admin.pages.banners._parts.indexEdit',['id' => $manufacturer->id])->render()
				    );
			    }

		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }
        return view('admin.pages.banners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banners_type = config('settings.banners_type');
        return view('admin.pages.banners.create',[
        	'banners_type' => $banners_type
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $banner = Banner::create([
		            'name' => $request->name,
			        'type' => $request->type,
			        'url' => $request->url,
			        'description' => $request->description
		        ]);

	    if($request->hasFile('banner')){
		    $banner->setThumb($request->banner, $request->type_model, $banner->id);
	    }

	    Session::flash('success','Баннер успешно добавлен');
	    return redirect()->route('banners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$banner = Banner::find($id);
	    $banners_type = config('settings.banners_type');
	    return view('admin.pages.banners.edit',[
		    'banners_type' => $banners_type,
		    'banner' => $banner
	    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id)
    {
	    $banner = Banner::find($id);
	    $banner->update([
		    'name' => $request->name,
		    'type' => $request->type,
		    'url' => $request->url,
		    'description' => $request->description
	    ]);

	    if($request->hasFile('banner')){
		    $banner->setThumb($request->banner, $request->type_model, $banner->id);
	    }

	    Session::flash('success','Баннер успешно обновлён');
	    return redirect()->route('banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
	    $banners_ids = json_decode($request->ids);

	    foreach ($banners_ids as $id){

		    Banner::find($id)->delete();
	    }
	    return response()->json(['status' => true]);
    }
}
