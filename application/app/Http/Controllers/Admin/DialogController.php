<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\DialogRequest;
use App\Models\Dialog;
use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class DialogController extends Controller
{
    public function index()
    {
    	$feedback = Feedback::where('closed', 0)->with(['dialog' => function($query){
		    $query->where('viewed',0);
	    }])->get();

    	return view('admin.pages.feedback.index', ['feedback' => $feedback]);
    }

    public function closed()
    {
	    $feedback = Feedback::where('closed', 1)->with('dialog')->get();

	    return view('admin.pages.feedback.closed', ['feedback' => $feedback]);
    }

    public function show($id)
    {
    	$feedback = Feedback::where('id', $id)->with('dialog')->first();

	    foreach ($feedback->dialog as $dialog){
		    $dialog->update([
		    	'viewed' => 1
		    ]);
	    }
	    return view('admin.pages.feedback.show', ['feedback' => $feedback]);
    }

    public function store(DialogRequest $request, $id)
    {
		if($request->closed){
			Feedback::where('id',$id)->update([
				'closed' => 1
			]);
		}

		Dialog::create([
			'feedback_id' => $id,
			'author_id' => $request->author_id,
			'viewed' => 1,
			'text' => $request->text
		]);

	    Session::flash('success','Ответ успешно добавлен');
	    return redirect()->route('dialog.show',['id' => $id]);
    }
}
