<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Foreign_good;
use App\Models\Foreign_good_param;
use App\Models\Foreign_order;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Foreign_order::with(['user', 'statuz'])->orderBy('id', 'desc')->paginate(15);

        return view('admin.pages.orders.index', ['orders' => $orders]);
    }

    public function show($id)
    {
        $order = Foreign_order::where('id', $id)->with('user.userParams', 'foreignGoods.foreignGoodParams')->first();
        $statuses = Status::all();
        return view('admin.pages.orders.show', ['order' => $order, 'statuses' => $statuses]);
    }

    public function destroy($id)
    {
        $order = Foreign_order::find($id);
        $order->delete();
        Session::flash('success', 'Заказ успешно удалён');
        return redirect()->route('orders.index');
    }

    public function showDetail($id)
    {
        $product = Foreign_good::find($id);
        if ($product) {
            switch ($product->client_type) {
                case 'trinity':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');
                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'armtek':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');

                    $detailOrder = $detailOrder->except(['PARNR', 'KEYZAK', 'WAERS', 'ANALOG', 'MINBM', 'DLVDT']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'avtoto':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');
                    $detailOrder = $detailOrder->except(['StorageDate', 'DeliveryPercent', 'RemoteID', 'SearchID', 'PartId']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'stparts':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');
                    $detailOrder = $detailOrder->except(['availability', 'packing', 'deliveryPeriod', 'deliveryPeriodMax', 'deadlineReplace', 'distributorCode', 'markup', 'supplierCode', 'supplierColor', 'supplierDescription', 'weight', 'volume', 'deliveryProbability', 'lastUpdateTime', 'additionalPrice', 'noReturn', 'id', 'distributorId', 'grp']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;

                case 'favorit':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');
                    $detailOrder = $detailOrder->except(['id', 'no_name', 'created_at', 'updated_at']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'forumAuto':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');
                    $detailOrder = $detailOrder->except(['id', 'created_at', 'updated_at', 'minimum']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'wheel':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');

                    $detailOrder = $detailOrder->except(['id', 'code_1c', 'created_at', 'updated_at', 'vendor_id']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;
                case 'tire':
                    $detailOrder = Foreign_good_param::where('foreign_good_id', $id)->get();
                    $detailOrder = $detailOrder->pluck('value', 'param');

                    $detailOrder = $detailOrder->except(['id', 'code_1c', 'created_at', 'updated_at', 'vendor_id', 'multiplicity']);

                    $html = view('admin.pages.products._parts._details', ['details' => $detailOrder])->render();
                    return response()->json(['status' => true, 'data' => ['html' => $html]]);
                    break;

            }
        } else {
            return response()->json(['status' => false, 'message' => 'Нет такого удалённого товара']);
        }
    }

    public function changeStatus(Request $request)
    {
        $order = Foreign_order::find($request->id);
        $order->update([
            'status' => $request->status,
        ]);

        return response()->json(['status' => true]);
    }
}
