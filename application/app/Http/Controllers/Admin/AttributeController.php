<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AttributeRequest;
use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeController extends Controller
{
    public function deleteAttr(AttributeRequest $request)
    {
		$attr = Attribute::where('id', $request->id)->first();
	    $attr->delete();
	    return response()->json(['status' => true]);
    }
}
