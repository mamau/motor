<?php

namespace App\Http\Controllers\Admin;

use App\Models\ReturnGood;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RbackController extends Controller
{
    public function index()
    {

    	$returns = ReturnGood::with('user','order')->paginate(15);

    	return view('admin.pages.return.index',['returns' => $returns]);
    }

    public function show($id)
    {
    	$rback = ReturnGood::where('id',$id)->with('thumbs','user','order')->first();

	    return view('admin.pages.return.show',['rback' => $rback]);
    }
}
