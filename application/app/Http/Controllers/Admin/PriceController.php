<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use App\Models\FavoritGoods;
use App\Models\Markup;
use App\Models\Tire;
use App\Models\Wheel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Mamau\Ftp\Ftp;

class PriceController extends Controller
{
	public function index()
	{
		return view('admin.pages.price.index');
	}

    public function import()
    {

	    $ftp = new Ftp();
	    $ftp->conn();
	    $ftp->get('csv', '/');

		return response()->json(['status' => true]);
    }

    public function parseCsv($fie_path, $position = 0)
    {
	    $handle = fopen(public_path($fie_path), 'r');

//	    if($position != 0){
//	        fseek($handle, $position);
//	    }
	    if ($handle) {
		    while ($line = fgetcsv($handle, 1000, ";")) {

			    if($line[0] == "﻿Производитель"){
				    continue;
			    }else{

//				    if(ftell($handle) > ($position + 2000000)){
//				    	$pos = ftell($handle);
//					    fclose($handle);
//				    	$this->parseCsv($fie_path, $pos);
//				    }
				    FavoritGoods::create([
					    'proizvoditel' => $line[0],
					    'nomer_po_katalogu' => $line[1],
					    'naimenovanie' => $line[2],
					    'tsena_po_dogovoru' => $line[3],
					    'kolichestvo' => (int)$line[4],
					    'kratnost' => (int)$line[5],
					    'no_name' => (int)$line[6]
				    ]);
				    unset($line);
			    }

		    }
	    }

    }

    public function parse(Request $request)
    {
		switch ($request->type){
			case 'favorit':

				DB::table('favorit_goods')->truncate();
				$query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE favorit_goods FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES (`proizvoditel`, `nomer_po_katalogu`, `naimenovanie`, `tsena_po_dogovoru`,`kolichestvo`,`kratnost`,`no_name`,@`created_at`,@`updated_at`) SET created_at=NOW(),updated_at=NOW()", addslashes(public_path('csv/Favorit.csv')));

				DB::connection()->getpdo()->exec($query);


			    return response()->json(['status' => true]);
			break;
			case 'forum':
				DB::table('forum_goods')->truncate();
				$query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE forum_goods CHARACTER SET cp1251 FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES (`marka`, `catalog_num`, `name`, `price`,`count`,`minimum`,`code`,@`created_at`,@`updated_at`) SET created_at=NOW(),updated_at=NOW()", addslashes(public_path('csv/ForumAutoPrice.csv')));

				DB::connection()->getpdo()->exec($query);

				return response()->json(['status' => true]);
			break;
        }
    }

    public function uploadTires()
    {
	    ini_set('max_execution_time', 120);
        $client = new Client();
	    $response = $client->request('GET', 'http://virbactd.ru/bitrix/catalog_export/tires_prices_russia.php', [
		    'headers' => ['Accept' => 'application/xml'],
		    'timeout' => 120
	    ])->getBody()->getContents();

	    $responseXml = simplexml_load_string($response);
	    $markup = Markup::select('value')->where('param','tiresWheels')->first();

	    DB::table('tires')->truncate();
	    foreach($responseXml->shop->offers->offer as $offer){

	    	if($offer->param[2] == 0 || $offer->param[3] == 0 || (int)$offer->param[12] == 0){
	    		continue;
		    }
	    	Tire::create([
                'vendor_id' => $offer['id'],
			    'name' => $offer->name,
			    'code_1c' => $offer->param[0],
			    'code' => $offer->param[1],
			    'width' => $offer->param[2],
			    'height' => $offer->param[3],
			    'diameter' => $offer->param[4],
			    'index_speed' => $offer->param[5],
			    'index_load' => $offer->param[6],
			    'type' => $offer->param[7],
			    'season' => $offer->param[8],
			    'brand' => $offer->param[9],
			    'model' => $offer->param[10],
			    'multiplicity' => $offer->param[12],
			    'price' => Helpers::markup((int)$offer->param[12], $markup->value)
		    ]);

	    }

	    return response()->json(['status' => true]);
    }

	public function uploadWheels()
	{
		ini_set('max_execution_time', 120);
		$client = new Client();
		$response = $client->request('GET', 'http://virbactd.ru/bitrix/catalog_export/wheels_prices.php', [
			'headers' => ['Accept' => 'application/xml'],
			'timeout' => 120
		])->getBody()->getContents();

		$responseXml = simplexml_load_string($response);

		$markup = Markup::select('value')->where('param','tiresWheels')->first();

		DB::table('wheels')->truncate();
		foreach($responseXml->shop->offers->offer as $offer){

			if($offer->param[1] == 0 || (int)$offer->param[11] == 0){
				continue;
			}

			Wheel::create([
				'vendor_id' => $offer['id'],
				'name' => $offer->name,
				'code_1c' => $offer->param[0],
				'width' => $offer->param[1],
				'diameter' => $offer->param[2],
				'diameter_hole' => $offer->param[3],
				'outreach' => $offer->param[4],
				'diameter_central' => $offer->param[5],
				'color1' => $offer->param[6],
				'color2' => $offer->param[7],
				'type' => $offer->param[8],
				'brand' => $offer->param[9],
				'model' => $offer->param[10],
				'price' => Helpers::markup((int)$offer->param[11], $markup->value)
			]);

		}

		return response()->json(['status' => true]);
	}

    /**
     * @return void
     */
    public function parserMotor() {
        Artisan::queue('parse:xml');
        // /usr/bin/php7.2 ~/motor/public_html/artisan queue:work --once
        return response()->json(['status' => true]);
    }
}
