<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::check()){
		    if(Auth::user()->hasRole(['admin','manager'])) {

			    return redirect()->route('profile.index');
		    }
	    }else{
		    return view('admin.auth.login', ['form' => 'login']);
	    }
    }
}
