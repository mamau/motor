<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Intervention\Image\Facades\Image;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\File;

/**
 * Class ParseController
 * @package App\Http\Controllers\Admin
 */
class ParseController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.parse.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
//        Artisan::queue('parse:news');
        // /usr/bin/php7.2 ~/motor/public_html/artisan queue:work --once
        return response()->json(['status' => true]);
//        return view('admin.pages.parse.index');
    }

    /**
     * Change links
     */
    public function links()
    {
        $news = News::with('seo')->get();
        if ($news) {
            $pattern = '/((href=(\'|\")+\/[0-9a-z\.\-_\/=]+(\'|\")))/i';
            foreach ($news as $article) {
                if ($article->seo) {
                    $text = preg_replace_callback($pattern, 'self::cbReplace', $article->seo->full_description);
                    $article->seo->full_description = $text;
                    $article->seo->save();
                }
            }
        }
        return view('admin.pages.parse.index');
    }
}
