<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ManufacturerRequest;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    if($request->ajax()){

		    $manufacturers = Manufacturer::select('id','name','sort')->get();

		    $returnData = ['data' => []];

		    if($returnData){

			    foreach ($manufacturers as $manufacturer){

				    $returnData['data'][] = array(
					    view('admin.pages.manufacturer._parts.indexCheckbox',['id' => $manufacturer->id])->render(),
					    $manufacturer->id,
					    $manufacturer->name,
					    $manufacturer->sort,
					    view('admin.pages.manufacturer._parts.indexEdit',['id' => $manufacturer->id])->render()
				    );
			    }

		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }

        return view('admin.pages.manufacturer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.pages.manufacturer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ManufacturerRequest $request)
    {
        $dataSeo = [];
    	$manufacturer = Manufacturer::create([
				            'name' => $request->name,
					        'sort' => $request->sort
				        ]);
		if(isset($request->thumb)){
    	    $manufacturer->setThumb($request->thumb, $request->type, $manufacturer->id);
		}

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->full_description;

		$manufacturer->saveSeo($dataSeo, $request->type, $manufacturer->id);

	    Session::flash('success','Производитель успешно добавлен');
	    return redirect()->route('manufacturer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$manufacturer = Manufacturer::where('id',$id)->with('seo','thumb')->first();
    	$data = [];
    	$data['id'] = $manufacturer->id;
    	$data['name'] = $manufacturer->name;
    	$data['sort'] = $manufacturer->sort;

    	if(!is_null($manufacturer->thumb)){
		    $data['thumb'] = asset($manufacturer->thumb->path.$manufacturer->thumb->name.'.'.$manufacturer->thumb->extension);
	    }else{
    	    $data['thumb'] = asset('img/noimage.png');
	    }

		if(!is_null($manufacturer->seo)){
		    $data['title_seo'] = $manufacturer->seo->title_seo;
		    $data['keywords_seo'] = $manufacturer->seo->keywords_seo;
		    $data['description_seo'] = $manufacturer->seo->description_seo;
		    $data['full_description'] = $manufacturer->seo->full_description;
		}else{
			$data['title_seo'] = '';
			$data['keywords_seo'] = '';
			$data['description_seo'] = '';
			$data['full_description'] = '';
		}


	    return view('admin.pages.manufacturer.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ManufacturerRequest $request, $id)
    {
	    $dataSeo = [];
	    $manufacturer = Manufacturer::find($id);
	    $manufacturer->update([
					    'name' => $request->name,
					    'sort' => $request->sort
				    ]);

	    if(isset($request->thumb)){
		    $manufacturer->setThumb($request->thumb, $request->type, $manufacturer->id);
	    }

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->full_description;

	    $manufacturer->saveSeo($dataSeo, $request->type, $manufacturer->id);

	    Session::flash('success','Производитель успешно обновлён');
	    return redirect()->route('manufacturer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
	    $manufacturers_ids = json_decode($request->ids);

	    foreach ($manufacturers_ids as $id){

		    Manufacturer::find($id)->delete();
	    }
	    return response()->json(['status' => true]);
    }
}
