<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoriesRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

	    if($request->ajax()){

		    $categories = Category::where('parent_cat',0)->with('childrenCategory')->get();

		    $returnData = ['data' => []];

		    if($returnData){

			    foreach ($categories as $category){

				    $returnData['data'][] = array(
					    view('admin.pages.categories._parts.indexCheckbox',['id' => $category->id])->render(),
					    $category->id,
					    $category->name,
					    $category->sort,
					    view('admin.pages.categories._parts.indexEdit',['id' => $category->id])->render()
				    );

					$this->childCats($category, $returnData['data']);
			    }

		    }else{

			    $returnData['data'][] = '';
		    }
		    return $returnData;
	    }

        return view('admin.pages.categories.index');
    }

	//Выводим дочерние категории
    public function childCats($category, &$data, $topName = null){

	    if(!empty($category->childrenCategory)){
		    foreach ($category->childrenCategory as $child){

		    	if(is_null($topName)){
				    $firstName = $category->name;
			    }else{

				    $firstName = $topName;
			    }

			    $data[] = [
				    view('admin.pages.categories._parts.indexCheckbox',['id' => $child->id])->render(),
				    $child->id,
				    $firstName.' <i class="fa fa-long-arrow-right"></i> '.$child->name,
				    '<span data-id-row="'.$child->id.'">'.$child->sort.'</span>',
				    view('admin.pages.categories._parts.childCat',['id' => $child->id])->render()

			    ];
			    $topInnerName = $firstName.' <i class="fa fa-long-arrow-right"></i> '.$child->name;


		        $this->childCats($child, $data, $topInnerName);

		    }
	    }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$cats = Category::where('status',1)->get();
	    return view('admin.pages.categories.create',['categories' => $cats]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
	    $dataSeo = [];
	    $category = Category::create([
		    'name' => $request->name,
		    'sort' => $request->sort,
		    'parent_cat' => $request->parent_cat,
		    'status' => $request->status
	    ]);
	    if(isset($request->thumb)){
		    $category->setThumb($request->thumb, $request->type, $category->id);
	    }

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->full_description;

	    $category->saveSeo($dataSeo, $request->type, $category->id);

	    Session::flash('success','Категория успешно добавлена');
	    return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$categories = Category::where('status',1)->get();
	    $category = Category::where('id',$id)->with('seo','thumb')->first();
	    $data = [];
	    $data['id'] = $category->id;
	    $data['name'] = $category->name;
	    $data['parent_cat'] = $category->parent_cat;
	    $data['sort'] = $category->sort;
	    $data['status'] = $category->status;
	    $data['categories'] = $categories;

	    if(!is_null($category->thumb)){
		    $data['thumb'] = asset($category->thumb->path.$category->thumb->name.'.'.$category->thumb->extension);
	    }else{
		    $data['thumb'] = asset('img/noimage.png');
	    }

	    if(!is_null($category->seo)){
		    $data['title_seo'] = $category->seo->title_seo;
		    $data['keywords_seo'] = $category->seo->keywords_seo;
		    $data['description_seo'] = $category->seo->description_seo;
		    $data['full_description'] = $category->seo->full_description;
	    }else{
		    $data['title_seo'] = '';
		    $data['keywords_seo'] = '';
		    $data['description_seo'] = '';
		    $data['full_description'] = '';
	    }


	    return view('admin.pages.categories.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, $id)
    {
	    $dataSeo = [];
	    $category = Category::find($id);
	    $category->update([
		    'name' => $request->name,
		    'sort' => $request->sort,
		    'parent_cat' => $request->parent_cat,
		    'status' => $request->status
	    ]);

	    if(isset($request->thumb)){
		    $category->setThumb($request->thumb, $request->type, $category->id);
	    }

	    $dataSeo['title_seo'] = $request->title_seo;
	    $dataSeo['keywords_seo'] = $request->keywords_seo;
	    $dataSeo['description_seo'] = $request->description_seo;
	    $dataSeo['full_description'] = $request->full_description;

	    $category->saveSeo($dataSeo, $request->type, $category->id);

	    Session::flash('success','Категория успешно обновлёна');
	    return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


	/**
	 * Удаление нескольких категорий
	 *
	 * @param CategoriesRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroyAll(CategoriesRequest $request){

		$categories_ids = json_decode($request->ids);
		foreach ($categories_ids as $category){

			if($request->force == 'true'){
				$categor = Category::where('id',$category)->onlyTrashed()->first();
				$categor->forceDelete();
			}else{

				Category::find($category)->delete();
			}
		}
		return response()->json(['status' => true]);
	}

	/**
	 * Восстановление категорий
	 *
	 * @param CategoriesRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function restoreUser(CategoriesRequest $request){

		$categories_ids = json_decode($request->ids);
		foreach ($categories_ids as $category){

			Category::where('id',$category)->onlyTrashed()->restore();
		}
		return response()->json(['status' => true]);
	}

	//Выводим родительские категории
	protected function parentCats($category, &$topName){


		if(count($category['parent_category'])>0){
			foreach ($category['parent_category'] as $parent){

				$topName = $parent['name'].' <i class="fa fa-long-arrow-right"></i> '.$topName;

				$this->parentCats($parent, $topName);

			}
		}
	}

	/**
	 * Корзина с удалёнными категориями
	 *
	 * @param Request $request
	 * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function trash(Request $request){

		if($request->ajax()){

			$categories = Category::with(['parentCategory' => function($query){

				$query->withTrashed();
			}])->onlyTrashed()->get();

			$returnData = ['data' => []];

			if($returnData){

				foreach ($categories as $category){

					$name = $category->name;
					$this->parentCats($category->toArray(), $name);

					$returnData['data'][] = array(
						view('admin.pages.categories._parts.trashCheckbox',['id' => $category->id])->render(),
						$category->id,
						$name,
						'<span data-id-row="'.$category->id.'">'.$category->sort.'</span>',
					);

				}

			}else{

				$returnData['data'][] = '';
			}
			return $returnData;
		}

		return view('admin.pages.categories.trash');
	}
}
