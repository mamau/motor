<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\MarkupRequest;
use App\Models\Markup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MarkupController extends Controller
{
    public function index()
    {
    	$markup = Markup::all();
    	$data = array_pluck($markup,'value', 'param');
    	return view('admin.pages.markup.index', ['data' => $data]);
    }

    public function store(MarkupRequest $request)
    {
    	$data = $request->except('_token');
		foreach ($data as $key => $val){
			$mark = Markup::where('param',$key)->first();
			if(!is_null($mark)){
				$mark->update([
					'value' => $val
				]);
			}else{
				Markup::create([
					'param' => $key,
					'value' => $val
				]);
			}
		}

	    Session::flash('success','Наценки успешно обновлены');
	    return redirect()->route('markup.index');
    }
}
