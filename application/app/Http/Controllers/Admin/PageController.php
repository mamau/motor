<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PagesRequest;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $pages = Page::where('parent_page', 0)->with('childrenPage')->get();

            $returnData = ['data' => []];

            if ($returnData) {

                foreach ($pages as $page) {

                    $returnData['data'][] = [
                        view('admin.pages.pages._parts.indexCheckbox', ['id' => $page->id])->render(),
                        $page->id,
                        $page->name,
                        $page->menu,
                        view('admin.pages.pages._parts.indexEdit', ['id' => $page->id])->render(),
                    ];

                    $this->childPages($page, $returnData['data']);
                }

            } else {

                $returnData['data'][] = '';
            }
            return $returnData;
        }

        return view('admin.pages.pages.index');
    }

    //Выводим дочерние страницы
    public function childPages($page, &$data, $topName = null)
    {

        if (!empty($page->childrenPage)) {
            foreach ($page->childrenPage as $child) {

                if (is_null($topName)) {
                    $firstName = $page->name;
                } else {

                    $firstName = $topName;
                }

                $data[] = [
                    view('admin.pages.pages._parts.indexCheckbox', ['id' => $child->id])->render(),
                    $child->id,
                    $firstName . ' <i class="fa fa-long-arrow-right"></i> ' . $child->name,
                    '<span data-id-row="' . $child->id . '">' . $child->menu . '</span>',
                    view('admin.pages.pages._parts.childCat', ['id' => $child->id])->render(),

                ];
                $topInnerName = $firstName . ' <i class="fa fa-long-arrow-right"></i> ' . $child->name;


                $this->childPages($child, $data, $topInnerName);

            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::select('id', 'name')->get();
        $menus = config('settings.menus');
        return view('admin.pages.pages.create', ['pages' => $pages, 'menus' => $menus]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PagesRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagesRequest $request)
    {
        $dataSeo = [];
        $page = Page::create([
            'name' => $request->name,
            'sort' => $request->sort,
            'parent_page' => $request->parent_page,
            'menu' => $request->menu,
            'url' => $request->url,
            'alias' => $request->alias,
        ]);

        $dataSeo['title_seo'] = $request->title_seo;
        $dataSeo['keywords_seo'] = $request->keywords_seo;
        $dataSeo['description_seo'] = $request->description_seo;
        $dataSeo['full_description'] = $request->full_description;

        $page->saveSeo($dataSeo, $request->type, $page->id);

        Session::flash('success', 'Страница успешно добавлена');
        return redirect()->route('pages.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::where('id', $id)->with('seo')->first();
        $pages = Page::select('id', 'name')->get();
        $menus = config('settings.menus');

        if (!is_null($page->seo)) {
            $data['title_seo'] = $page->seo->title_seo;
            $data['keywords_seo'] = $page->seo->keywords_seo;
            $data['description_seo'] = $page->seo->description_seo;
            $data['full_description'] = $page->seo->full_description;
        } else {
            $data['title_seo'] = '';
            $data['keywords_seo'] = '';
            $data['description_seo'] = '';
            $data['full_description'] = '';
        }

        return view('admin.pages.pages.edit', [
            'currentPage' => $page,
            'pages' => $pages,
            'menus' => $menus,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PagesRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PagesRequest $request, $id)
    {
        $dataSeo = [];
        $page = Page::find($id);
        $page->update([
            'name' => $request->name,
            'sort' => $request->sort,
            'parent_page' => $request->parent_page,
            'menu' => $request->menu,
            'url' => $request->url,
            'alias' => $request->alias,
        ]);

        $dataSeo['title_seo'] = $request->title_seo;
        $dataSeo['keywords_seo'] = $request->keywords_seo;
        $dataSeo['description_seo'] = $request->description_seo;
        $dataSeo['full_description'] = $request->full_description;

        $page->saveSeo($dataSeo, $request->type, $page->id);

        Session::flash('success', 'Страница успешно обновлена');
        return redirect()->route('pages.index');
    }

    /**
     * Удаление нескольких страниц
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll(Request $request)
    {
        $pages_ids = json_decode($request->ids);
        foreach ($pages_ids as $page) {

            if ($request->force == 'true') {
                $curr_page = Page::where('id', $page)->onlyTrashed()->first();
                $curr_page->forceDelete();
            } else {

                Page::find($page)->delete();
            }
        }
        return response()->json(['status' => true]);
    }

}
