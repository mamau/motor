<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers{
	    logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


	public function showLoginForm(){

		return view('admin.auth.login', ['form' => 'login']);
	}

	public function logout(Request $request)
	{
		$this->performLogout($request);
		return redirect()->route('admin');
	}

	public function redirectPath()
	{
		if (method_exists($this, 'redirectTo')) {
			return $this->redirectTo();
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin/orders';
	}

	public function redirectTo(){

		$user = Auth::user();
		if(Auth::user()->hasRole(['admin','manager'])){

			return '/admin/orders';
		}
	}
}
