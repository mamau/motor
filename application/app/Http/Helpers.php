<?php
namespace App\Http;


use App\Models\City;
use App\Models\Dialog;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\File;

/**
 * Class Helpers
 * @package App\Http
 */
class Helpers {

    /**
     * @param $sPhone
     * @return string
     */
	public static function normalizePhone($sPhone){

		return strtr($sPhone, array(' ' => '', '+' => '', '-' => '', '(' => '', ')' => ''));
	}

    /**
     * @param $sPhone
     * @return null|string|string[]
     */
	public static function intPhone($sPhone){

		return preg_replace('/^(8)/', '+7', $sPhone);
	}

    /**
     * @param $obj
     * @param null $full
     * @return string
     */
    public static function thumbSm($obj, $full = null){

        if(!is_null($obj->thumb)){
            if(is_null($full)){
                if(File::exists($obj->thumb->path.$obj->thumb->name.'.sm.'.$obj->thumb->extension)){
                    return asset($obj->thumb->path.$obj->thumb->name.'.sm.'.$obj->thumb->extension);
                }else{
                    return asset($obj->thumb->path.$obj->thumb->name.'.'.$obj->thumb->extension);
                }
            }else{
                if(File::exists($obj->thumb->path.$obj->thumb->name.'.lg.'.$obj->thumb->extension)){
                    return asset($obj->thumb->path.$obj->thumb->name.'.lg.'.$obj->thumb->extension);
                }else{
                    return asset($obj->thumb->path.$obj->thumb->name.'.'.$obj->thumb->extension);
                }
            }
        }else{
            return asset('img/noimage.png');
        }
    }

    /**
     * @param $obj
     * @return string
     */
	public static function thumbXs($obj){

		if(!is_null($obj->thumb)){
			if(File::exists($obj->thumb->path.$obj->thumb->name.'.xs.'.$obj->thumb->extension)){
				return asset($obj->thumb->path.$obj->thumb->name.'.xs.'.$obj->thumb->extension);
			}else{
				return asset($obj->thumb->path.$obj->thumb->name.'.'.$obj->thumb->extension);
			}
		}else{
			return asset('img/noimage.png');
		}
	}

    /**
     * @param $category
     * @param $breadcrumbs
     * @param array $middle_breadcrumbs
     * @return mixed
     */
	public static function parentCategory($category, $breadcrumbs, $middle_breadcrumbs = []){


		if(!is_null($category->parentCategory)){
			$current_bread = [];

			$ancestor = $category->parentCategory;

			$current_bread['title'] = $ancestor->name;
			$current_bread['url'] = route('front.categories.show', $ancestor->id);
			$current_bread['first'] = false;
			$current_bread['last'] = false;
			array_push($middle_breadcrumbs,(object)$current_bread);

			self::parentCategory($ancestor,$breadcrumbs, $middle_breadcrumbs);
		}else{
			foreach (array_reverse($middle_breadcrumbs) as $middle){

				$breadcrumbs->push($middle->title, $middle->url);
			}
			return $breadcrumbs;
		}
		return $breadcrumbs;
	}

    /**
     * @param $productId
     * @return string
     */
	public static function getThumbById($productId){

		$product = Product::find($productId);
		return self::thumbXs($product);

	}

    /**
     * @param $id
     * @return mixed
     */
	public static function userById($id)
	{
		$user = User::find($id);
		$name = $user->name;
		return $name;
	}

    /**
     * @param $id
     * @return mixed
     */
	public static function getCity($id)
	{
		$city = City::find($id);
		$name = $city->name;
		return $name;
	}

    /**
     * @return mixed
     */
	public static function unViewedMess()
	{
		$dialog = Dialog::where('viewed',0)->count();
		return $dialog;
	}

    /**
     * @param $price
     * @param $markup
     * @return float
     */
	public static function markup($price, $markup)
	{
		return round((($price/100)*$markup)+$price,2);
	}

    /**
     * @param $thumb
     * @return string
     */
	public static function buildImage($thumb)
	{
		return asset($thumb->path.$thumb->name.'.'.$thumb->extension);
	}
}
