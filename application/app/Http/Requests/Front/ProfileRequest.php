<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'email' => 'required|max:255|email',
            'phone' => 'required|max:20',
            'params.surname' => 'max:100|min:2',
            'params.city' => 'numeric',
            'params.gender' => 'in:male,female',
            'params.birthday' => 'date_format:d.m.Y',
            'params.address' => 'min:10',
        ];
    }
}
