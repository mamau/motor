<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ReturnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orders' => 'required|exists:foreign_orders,id|numeric',
	        'cause' => 'required|max:255|min:3',
	        'description' => 'required|min:10',
	        'thumb.*' => 'sometimes|required|mimes:jpeg,png'
        ];
    }
}
