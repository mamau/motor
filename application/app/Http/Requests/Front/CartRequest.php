<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class CartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'id' => 'required|numeric',
	        'name' => 'required|max:255',
	        'quantity' => 'required|numeric',
	        'price' => 'required|numeric',
	        'min' => 'required|numeric',
	        'max' => 'required|numeric',
        ];
    }
}
