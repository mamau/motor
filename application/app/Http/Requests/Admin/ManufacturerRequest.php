<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ManufacturerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'name' => 'sometimes|required|max:255|min:3',
	        'thumb' => 'sometimes|required|mimes:jpeg,png',
	        'sort' => 'numeric',
	        'title_seo' => 'sometimes|required|max:255|min:3',
	        'keywords_seo' => 'max:255',
	        'description_seo' => 'min:3',
	        'full_description' => 'min:3'
        ];
    }
}
