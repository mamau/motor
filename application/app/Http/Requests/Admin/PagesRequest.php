<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:3',
	        'parent_page' => 'required|numeric',
	        'menu' => 'required|in:top,left,0',
	        'sort' =>  'required|numeric',
	        'alias' =>  'required|min:3|max:100',
	        'url' => 'url',
	        'title_seo' => 'required|max:255|min:3',
	        'keywords_seo' => 'max:255',
	        'description_seo' => 'min:3',
	        'full_description' => 'min:3',
	        'type' => 'sometimes|required|in:page',
        ];
    }
}
