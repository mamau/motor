<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|max:255|min:3',
            'model_product' => 'sometimes|required|max:255|min:3',
            'sku' => 'sometimes|required|max:255|min:3',
            'price' => 'sometimes|required|numeric',
            'quantity' => 'sometimes|required|numeric',
            'minimum' => 'sometimes|required|numeric',
            'stock_status_id' => 'sometimes|required|numeric',
            'date_available' => 'sometimes|required|date_format:d.m.Y',
            'status' => 'sometimes|required|numeric',
            'sort_order' => 'sometimes|required|numeric',
            'thumb' => 'sometimes|required|mimes:jpeg,png',
            'category_id' => 'sometimes|required|array',
            'manufacturer_id' => 'sometimes|required|numeric',
            'type' => 'sometimes|required|in:product',
            'title_seo' => 'sometimes|required|max:255|min:3',
            'keywords_seo' => 'max:255',
            'description_seo' => 'min:3',
            'full_description' => 'min:3',
            'ids' => 'sometimes|required',
            'force' => 'sometimes|required|in:true,false',
            'attr_name' => 'sometimes|array',
            'attr_value' => 'sometimes|array',
            'attr' => 'sometimes|array',
            'special' => 'sometimes',
        ];
    }
}
