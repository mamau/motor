<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'name' => 'sometimes|required|max:100',
	        'email' => 'sometimes|required|max:255|email',
	        'phone' => 'max:20',
	        'role' => 'numeric|exists:roles,id',
	        'password' => 'sometimes|required|min:6',
	        'confirm' => 'sometimes|required|same:password',
	        'ids' => 'sometimes|required',
	        'force' => 'sometimes|required|in:true,false'
        ];
    }
}
