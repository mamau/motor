<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'title' => 'required|max:255|min:3',
	        'thumb' => 'sometimes|required|mimes:jpeg,png',
	        'title_seo' => 'required|max:255|min:3',
	        'type' => 'required|in:news',
	        'keywords_seo' => 'max:255',
	        'description_seo' => 'min:3',
	        'description' => 'min:3'
        ];
    }
}
