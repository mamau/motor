<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'name' => 'sometimes|required|max:255|min:3',
	        'thumb' => 'sometimes|required|mimes:jpeg,png',
	        'sort' => 'sometimes|required|numeric',
	        'parent_cat' => 'sometimes|required|numeric',
	        'status' => 'sometimes|required|numeric',
	        'type' => 'sometimes|required|in:category',
	        'title_seo' => 'sometimes|required|max:255|min:3',
	        'keywords_seo' => 'max:255',
	        'description_seo' => 'min:3',
	        'full_description' => 'min:3',
	        'ids' => 'sometimes|required',
	        'force' => 'sometimes|required|in:true,false'
        ];
    }
}
