<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MarkupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avtoto' => 'required|numeric',
	        'armtek' => 'required|numeric',
	        'forumAuto' => 'required|numeric',
	        'favoritAuto' => 'required|numeric',
	        'tiresWheels' => 'required|numeric',
	        'stparts' => 'required|numeric',

        ];
    }
}
