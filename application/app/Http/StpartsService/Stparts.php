<?php

namespace App\Http\StpartsService;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class Stparts
{
    protected $username;
    protected $password;
    protected $base_url;
    protected $client;

    public function __construct()
    {
        $this->username = config('settings.stparts_login');
        $this->password = md5(config('settings.stparts_password'));
        $this->base_url = config('settings.base_url');
        $this->client = new Client();
    }

    /**
     *
     *  Поиск брендов по номеру
     *  Операция: search/brands
     *  Метод: GET
     *  Осуществляет поиск по номеру детали и возвращает массив найденных брендов, имеющих деталь с искомым номером.
     * Аналог этапа выбора бренда на сайте.
     */
    public function searchBrands($code)
    {
       try{
            $url = $this->makeUrl('search/brands/', $code);
            $result = $this->client->request('GET', $url);

            return json_decode($result->getBody());
       }catch (GuzzleException $exception){
           Log::error($exception->getMessage());
       }
    }

    /**
     * Поиск детали по номеру и бренду
     * Операция: search/articles
     * Метод: GET
     * Осуществляет поиск по номеру производителя и бренду. Возвращает массив найденных деталей. Так как один и тот же
     * производитель может иметь несколько общепринятых наименований (например, GM и General Motors), система постарается
     * это учесть, используя собственную базу синонимов брендов.
     */
    public function searchBrandsAndCodes($code, $brand)
    {
        try{
            $url = $this->makeUrl('search/articles/', $code, $brand);
            $result = $this->client->request('GET', $url);

            return json_decode($result->getBody());
        }catch (GuzzleException $exception){
            Log::error($exception->getMessage());
        }
    }

    protected function makeUrl($needle, $number, $brand = null)
    {
        $nonRequire = !is_null($brand)?'&brand='.$brand:'';

        $url = $this->base_url.$needle.
                '?userlogin='.$this->username.
                '&userpsw='.$this->password.
                '&number='.$number.$nonRequire;

        return $url;
    }
}
