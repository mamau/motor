<?php
return [
    'stparts_login' => env('STPARTS_LOGIN', 'login'),
    'stparts_password' => env('STPARTS_PASSW', 'ur password'),
    'base_url' => 'http://stparts.ru.public.api.abcp.ru/',

	'admin_email' => 'mopckou_pak48rus@mail.ru',
	'payment_kassa_shop_id' => env('YANDEX_SHOP_ID', ''),
	'payment_kassa_shop_scid' => env('YANDEX_SCID', ''),
	'payment_kassa_password' => env('YANDEX_PASSWORD', ''),
	'pay_for_changes' => 5000,
	'menus' => ['top'=>'Верхнее','left' => 'Левое'],
	'banners_type' => ['slider'=>'Слайдер','banner' => 'Баннер'],

	//Filter tires
	'width' => [15,27,30,31,32,33,35,37,38,40,135,145,155,165,175,185,195,205,215,225,235,240,245,255,265,275,285,290,295,305,315,320,325,335,345],
	'height' => [11,13,25,30,35,40,45,50,55,60,65,70,75,80,82,85,90],
	'diameter' => [12,'12C',13,'13C',14,'14C',15,'15C',16,'16C',17,'17C',18,19,20,21,22,23,24],
	'season' => ['Зима','Лето'],
	'brand' => ['BARUM','BFGoodrich','BRIDGESTONE','CONTINENTAL','Cordiant','DUNLOP','DUNLOP(EU)','FEDERAL','FORMULA','GENERAL TIRE','GISLAVED','GOODYEAR','HANKOOK','KUMHO','MATADOR','MAXXIS','MICHELIN','NEXEN','NITTO','NOKIAN','NORDMAN','OVATION','PIRELLI','RIKEN','Rosava','SAVA','TIGAR','TOYO','TRIANGLE','VIATTI','YOKOHAMA','Волтайр','Кама','Сибур'],

	//Filter wheels
	'w_diameter' => [12,13,14,15,16,17,18,19,20,21,22,24,'17,5'],
	'w_width' => ['4j','4,5j','5j','5,5j','6j','6,5j','7,0j','7j','7,5j','8j','8,5j','9j','9,5j','10j','10,5j','11j','11,5j','12j'],
	'w_diameter_hole' => ['6/130','5/105','4/100','4/98/100','4/98','5/108','4/100/114,3','4/114,3','5/114,3','6/205','5/120','5/100','4/108','5/112','6/139,7','5/100/112','5/130','5/108/112','5/110','5/139,7','5/150','5/108/114,3','4/100/108','3/98','5/160','5/115','6/170','5/110/114,3','5/118','6/222,25','10/100/112','6/120','3/112','5/120/130','6/180','5/115/120','5/100/108','5/108/120','5/98/108','8/100/114,3','10/100/114,3','5/100/114,3','5/112/114,3','5/120,65','6/114,3','4/108/114,3','4/100/5/100','4/114,3/5/114,3','5/110/112','5/108/110/114,3','5/112/120','6/127','4/98/108','5/108/110','5/98','5/127','5/114,3/120','3/256','5/114,3/127','5/114,3/130','5/100/110','5/114,3/115','10/114,3/120','8/165','10/130/120'],
	'w_outreach' => ['-25','-24','-22','-19','-16','-15','-13','-10','-6','-5',0,2,3,4,5,6,7,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,'34.5',35,36,'36.5',37,'37.5',38,'38.5',39,'39.5',40,'40.7',41,'41.3','41.5',42,'42.5',43,'43.5','43.8',44,45,'45.5',46,47,'47.5',48,'48.5',49,50,'50.5','50.8',51,52,'52.5',53,54,55,56,'56.4',57,58,59,60,62,'63.3','63.4',64,65,66,68,83,105,106,115,130],
	'w_diameter_central' => [52,'54,0','54,1','56,1','56,5','56,6','56,7',57,'57,1','58,1','58,5','58,6','58,7',59,60,'60,1','60,2','60,5','60,8','63,1','63,3','63,35','63,4','63,5','63,7','64','64,1',65,'65,1',66,'66,1','66,5','66,56','66,6',67,'67,1','67,2','69,1','69,2',70,'70,1','70,2','70,3','70,6','71,1','71,4','71,5','71,6',72,'72,5','72,6',73,'71,1','73,2','73,9','74,1',75,'75,1','76,1','77,8','77,9','78,1',82,'83,7',84,'84,1','84,2','85,3','89,1','92,3','92,5','92,6','93,1','95,3','95,5','95,6',98,'98,5','98,6',100,'100,1','100,6',101,102,'104,1',106,'106,1','106,2','106,3','106,5','106,6','107,1','107,5','107,6',108,'108,1','108,2','108,3','108,4','108,5','108,6','108,7','109,1','109,5','109,7',110,'110,2','110,3','110,5','110,6',111,'111,6',112,'113,1','114,3',130,'130,1','138,8',150],
	'w_type' => ['Диск легкосплавный','Диск стальной'],
	'w_brand' => ['4GO','AEZ','ASTERRO','Alcasta','Arrivo','BETTER','CROSS STREET','ENZO','GR','HARP','IFREE','IJI','IJITSU','IJITSU NIPPON','KFZ','LENSO','LS','Lada','MAK','MEGA WHEELS','MW','N20','NEXT','NZ','Nitro','OFF-ROAD-WHEELS','RACING','REPLICA TD','REPLICA TD Special Series','Replica','Replica KH','Replica Lege Artis','Replica Reline',' Replica Replay', 'SLIK','SanFox','TREBL','TechLine','USW','VIRBAC','VISSOL','WIGER','X-RACE','YAMATO','YOKATTA','YST','Автоваз','ГАЗ','Евродиск','КиК','КиК Replica','Кременчуг','СКАД','ТЗСК']
];
