<?php
return [
	'user_login' => env('ARMTEK_LOGIN', ''),
	'user_password' => env('ARMTEK_PASSWORD', '')
];
