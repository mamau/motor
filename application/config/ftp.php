<?php
return [

	'host' => env('FTP_HOST', null),
	'username' => env('FTP_USERNAME', null),
	'password' => env('FTP_PASSWORD', null),


	//Config
	'passive_mode' => true, // true | false
	'transfer_mode' => FTP_BINARY, // FTP_ASCII | FTP_BINARY
	'reattempts' => 3, // Number of time to re-attempt connection
	'log_path' => '/logs', // Path to log file
	'verbose' => true, //true | false
	'create_mask' => '0755' // default: 0777

];
