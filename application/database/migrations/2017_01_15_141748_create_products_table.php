<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->string('model_product')->nullable()->index();
            $table->string('sku')->nullable()->index()->unique();
            $table->float('price',10,2)->nullable()->index();
            $table->mediumInteger('quantity')->nullable()->unsigned();
            $table->tinyInteger('minimum')->nullable()->unsigned();
            $table->tinyInteger('stock_status_id')->nullable()->unsigned();
            $table->timestamp('date_available')->nullable();
            $table->boolean('status')->default(true);
            $table->mediumInteger('sort_order')->nullable()->unsigned()->default(1);
            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
