<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorit_goods', function (Blueprint $table) {
	        $table->engine = 'MYISAM';
            $table->increments('id');
            $table->string('proizvoditel',100);
            $table->string('nomer_po_katalogu',100);
            $table->string('naimenovanie',100);
            $table->decimal('tsena_po_dogovoru',10,2)->default(0);
            $table->unsignedMediumInteger('kolichestvo')->default(0);
            $table->unsignedMediumInteger('kratnost')->default(1);
            $table->unsignedMediumInteger('no_name')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorit_goods');
    }
}
