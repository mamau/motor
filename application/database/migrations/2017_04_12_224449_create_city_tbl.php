<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
	        $table->engine = 'MYISAM';
            $table->increments('id');
            $table->unsignedMediumInteger('country_id')->index();
            $table->string('city',100)->index();
            $table->string('state',100);
            $table->string('region',100);
            $table->boolean('biggest_city')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
