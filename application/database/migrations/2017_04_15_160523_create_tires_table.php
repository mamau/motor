<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tires', function (Blueprint $table) {
	        $table->engine = 'MYISAM';
        	$table->increments('id');
			$table->string('name',150);
			$table->string('code_1c');
			$table->string('code')->nullable();
			$table->unsignedMediumInteger('width')->index()->nullable();
			$table->unsignedMediumInteger('height')->index()->nullable();
			$table->unsignedMediumInteger('diameter')->index()->nullable();
			$table->string('index_speed',20)->nullable();
			$table->string('index_load',100)->nullable();
			$table->string('type',100)->index()->nullable();
			$table->string('season',100)->index()->nullable();
			$table->string('brand',150)->index()->nullable();
			$table->string('model',150)->nullable();
			$table->string('multiplicity',150)->nullable();
			$table->decimal('price',10,2)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tires');
    }
}
