<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDialogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dialog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feedback_id')->unsigned()->index();
            $table->foreign('feedback_id')->references('id')->on('feedback')->onDelete('cascade');
            $table->text('question');
            $table->text('answer');
            $table->mediumInteger('asking_id')->unsigned()->index();
            $table->mediumInteger('respondent_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dialog');
    }
}
