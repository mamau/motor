<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDialogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dialog', function (Blueprint $table) {
            $table->dropColumn('question');
            $table->dropColumn('answer');
            $table->dropColumn('asking_id');
            $table->dropColumn('respondent_id');
            $table->text('text')->after('feedback_id');
            $table->mediumInteger('author_id')->unsigned()->index()->after('feedback_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dialog', function (Blueprint $table) {
	        $table->text('question');
	        $table->text('answer');
	        $table->mediumInteger('asking_id')->unsigned()->index();
	        $table->mediumInteger('respondent_id')->unsigned()->index();
	        $table->dropColumn('text');
	        $table->dropColumn('author_id');
        });
    }
}
