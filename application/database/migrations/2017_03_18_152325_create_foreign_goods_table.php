<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_goods', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('foreign_order_id')->unsigned()->index();
            $table->string('name');
            $table->decimal('price',10,2)->index();
            $table->smallInteger('count')->unsigned()->index()->default(1);
            $table->string('client_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_goods');
    }
}
