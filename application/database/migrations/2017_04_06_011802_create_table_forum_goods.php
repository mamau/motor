<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForumGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_goods', function (Blueprint $table) {
	        $table->engine = 'MYISAM';
	        $table->charset = 'cp1251';
	        $table->collation = 'cp1251_general_ci';
            $table->increments('id');
            $table->string('marka',100)->nullable()->index();
            $table->string('catalog_num',100)->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('price',10,2)->default(0);
            $table->unsignedMediumInteger('count')->default(0);
            $table->unsignedMediumInteger('minimum')->default(0);
            $table->unsignedMediumInteger('code')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_goods');
    }
}
