<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('seoable_id')->unsigned()->nullable();
            $table->string('seoable_type')->nullable();
            $table->string('title_seo')->nullable();
            $table->string('keywords_seo')->nullable();
            $table->mediumText('description_seo')->nullable();
            $table->text('full_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo');
    }
}
