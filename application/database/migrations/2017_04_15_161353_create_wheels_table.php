<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWheelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wheels', function (Blueprint $table) {
	        $table->engine = 'MYISAM';
            $table->increments('id');
	        $table->string('name',150);
	        $table->string('code_1c');
	        $table->string('width')->index()->nullable();
	        $table->string('diameter',100)->index()->nullable();
	        $table->string('diameter_hole',100)->index()->nullable();
	        $table->unsignedMediumInteger('outreach')->index()->nullable();
	        $table->unsignedMediumInteger('diameter_central')->index()->nullable();
	        $table->string('color1')->nullable();
	        $table->string('color2')->nullable();
	        $table->string('type')->nullable();
	        $table->string('brand')->nullable()->index();
	        $table->string('model')->nullable()->index();
	        $table->decimal('price',10,2)->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wheels');
    }
}
