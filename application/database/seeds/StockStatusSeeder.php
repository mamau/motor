<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StockStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('stock_status')->insert([
		    ['name' => 'В наличии','slug' => 'have', 'description' =>'Есть в наличии на складе','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
		    ['name' => 'Нет в наличии','slug' => 'nothave', 'description' =>'Нет в наличии на складе','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
		    ['name' => 'Ожидание 2-3 дня','slug' => 'waiting', 'description' =>'Ожидание товара 2-3 дня','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
		    ['name' => 'Предзаказ','slug' => 'preorder', 'description' =>'Предсказ товара','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
	    ]);
    }
}
