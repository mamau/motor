<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleuserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('role_user')->insert([
		    ['role_id' => 1,'user_id' => 1, 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
		    ['role_id' => 1,'user_id' => 2, 'created_at' => Carbon::now(),'updated_at' => Carbon::now()]
	    ]);
    }
}
