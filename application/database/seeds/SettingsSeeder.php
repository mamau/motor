<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'shop_name' => 'Мотор',
                'address' => 'г. Грязи ул. Гагарина д. 1 р-н мини-рынка',
                'email' => 'motor_211@mail.ru',
                'phone' => '+7 (920) 506 38 38, +7 (920) 248 84 84',
                'work_time' => 'пн-вс: 9:00-19:00, без выходных',
                'welcome' => '<p style="font-family: Roboto; background-color: rgba(255, 255, 255, 0.901961);">Наша компания осуществляет продажу автозапчастей. На нашем сайте вы сможете найти абсолютно любую деталь. </p><p style="font-family: Roboto; background-color: rgba(255, 255, 255, 0.901961);">У нас имеется большой ассортимент запчастей в наличии. Также вы можете заказать запчасть, вбив её номер или номер vin в поисковую строку, после деталь положите себе в корзину и оплатите удобным для вас способом. Если возникнут трудности, вы легко можете задать вопрос через личный кабинет или нажав на ссылку внизу сайта.</p><p style="font-family: Roboto; background-color: rgba(255, 255, 255, 0.901961);">Приятного времяпрепровождения!</p>',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);
    }
}
