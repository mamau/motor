<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

#Admin Routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/', ['as' => 'admin.index', 'middleware' => 'guest', 'uses' => 'IndexController@index']);
    Route::post('login', ['as' => 'admin.login', 'uses' => 'AuthController@login']);
    Route::post('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@logout']);

    Route::group(['middleware' => ['auth', 'role:admin']], function () {
        Route::get('products/filter', ['as' => 'admin.products.filter', 'uses' => 'ProductsController@filter']);
        Route::post('products/uploadZip', ['as' => 'admin.products.uploadZip', 'uses' => 'ProductsController@extractZIP']);
        Route::get('products/trash', ['as' => 'products.trash', 'uses' => 'ProductsController@trash']);
        Route::get('users/trash', ['as' => 'users.trash', 'uses' => 'UsersController@trash']);
        Route::get('categories/trash', ['as' => 'categories.trash', 'uses' => 'CategoriesController@trash']);
        Route::get('dialog/list', ['as' => 'dialog.list', 'uses' => 'DialogController@index']);
        Route::get('dialog/closed', ['as' => 'dialog.closed', 'uses' => 'DialogController@closed']);
        Route::get('dialog/show/{id}', ['as' => 'dialog.show', 'uses' => 'DialogController@show']);
        Route::post('dialog/store/{id}', ['as' => 'dialog.store', 'uses' => 'DialogController@store']);


        Route::get('returns', ['as' => 'rback.index', 'uses' => 'RbackController@index']);
        Route::get('returns/{id}', ['as' => 'rback.show', 'uses' => 'RbackController@show']);

        Route::get('markup', ['as' => 'markup.index', 'uses' => 'MarkupController@index']);
        Route::post('markup', ['as' => 'markup.store', 'uses' => 'MarkupController@store']);

        Route::get('orders', ['as' => 'orders.index', 'uses' => 'OrderController@index']);
        Route::get('orders/detail/{id}', ['as' => 'orders.detail', 'uses' => 'OrderController@showDetail']);
        Route::get('orders/show/{id}', ['as' => 'orders.show', 'uses' => 'OrderController@show']);
        Route::get('orders/delete/{id}', ['as' => 'orders.delete', 'uses' => 'OrderController@destroy']);
        Route::post('order/changeStatus', ['as' => 'orders.changeStatus', 'uses' => 'OrderController@changeStatus']);

        Route::resource('profile', 'ProfileController');
        Route::resource('users', 'UsersController');
        Route::post('users/delete', ['as' => 'admin.users.delete', 'uses' => 'UsersController@destroyAll']);
        Route::post('users/restore', ['as' => 'admin.users.restore', 'uses' => 'UsersController@restoreUser']);
        Route::resource('manufacturer', 'ManufacturerController');
        Route::resource('categories', 'CategoriesController');
        Route::post('categories/delete', ['as' => 'admin.categories.delete', 'uses' => 'CategoriesController@destroyAll']);
        Route::post('categories/restore', ['as' => 'admin.categories.restore', 'uses' => 'CategoriesController@restoreUser']);
        Route::resource('products', 'ProductsController');
        Route::post('products/delete', ['as' => 'admin.products.delete', 'uses' => 'ProductsController@destroyAll']);
        Route::post('products/restore', ['as' => 'admin.products.restore', 'uses' => 'ProductsController@restoreProduct']);

        Route::post('delete/attr', ['as' => 'admin.attribute.delete', 'uses' => 'AttributeController@deleteAttr']);
        Route::resource('pages', 'PageController');
        Route::post('pages/delete', ['as' => 'admin.pages.delete', 'uses' => 'PageController@destroyAll']);
        Route::get('parse', ['as' => 'parse.index', 'uses' => 'ParseController@index']);
        Route::post('parse', ['as' => 'parse.store', 'uses' => 'ParseController@store']);
        Route::post('parseLinks', ['as' => 'parse.links', 'uses' => 'ParseController@links']);
        Route::resource('settings', 'SettingsController');
        Route::resource('banners', 'BannersController');
        Route::resource('news', 'NewsController');
        Route::resource('status', 'StatusController');

        Route::get('price/index', ['as' => 'price.index', 'uses' => 'PriceController@index']);
        Route::post('price/import', ['as' => 'price.import', 'uses' => 'PriceController@import']);
        Route::post('price/parse', ['as' => 'price.parse', 'uses' => 'PriceController@parse']);
        Route::post('price/tires', ['as' => 'price.tires', 'uses' => 'PriceController@uploadTires']);
        Route::post('price/wheels', ['as' => 'price.wheels', 'uses' => 'PriceController@uploadWheels']);
        Route::post('price/motor', ['as' => 'price.motor', 'uses' => 'PriceController@parserMotor']);
    });
});

# Front Routes
Route::group(['namespace' => 'Front'], function () {

    Route::get('/', ['as' => 'index', 'middleware' => 'guest', 'uses' => 'IndexController@index']);
    Route::post('login', ['as' => 'front.login', 'uses' => 'AuthController@login']);
    Route::post('logout', ['as' => 'front.logout', 'uses' => 'AuthController@logout']);
    Route::get('register', ['as' => 'front.register.index', 'uses' => 'AuthController@registerIndex']);
    Route::post('register', ['as' => 'front.register', 'uses' => 'AuthController@register']);

    Route::get('forgot', ['as' => 'front.forgot.index', 'uses' => 'AuthController@forgotIndex']);
    Route::post('forgot', ['as' => 'front.forgot', 'uses' => 'AuthController@forgot']);

    Route::get('/password/reset', 'AuthController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AuthController@showResetForm');
    Route::post('/password/reset', 'ResetPasswordController@reset');

    Route::get('categories', ['as' => 'front.categories', 'uses' => 'CategoriesController@index']);
    Route::get('categories/{category}', ['as' => 'front.categories.show', 'uses' => 'CategoriesController@show']);
    Route::get('products/{product}', ['as' => 'front.products.show', 'uses' => 'ProductsController@show']);
    Route::post('cart/add', ['as' => 'front.cart.add', 'uses' => 'CartController@addCart']);

    Route::post('stparts/brand', ['as' => 'stparts.brand', 'uses' => 'SearchController@searchStparts']);
    Route::get('stparts/codebrand/{brand}/{code}', ['as' => 'stparts.codebrand', 'uses' => 'SearchController@codeBrand']);


    Route::post('cart/update', ['as' => 'front.cart.update', 'uses' => 'CartController@updateCart']);
    Route::post('order/make', ['as' => 'front.order.make', 'uses' => 'OrderController@makeOrder']);

    Route::post('cart/content', ['as' => 'front.cart.content', 'uses' => 'CartController@getCart']);
    Route::post('cart/remove', ['as' => 'front.cart.remove', 'uses' => 'CartController@destroyProduct']);
    Route::post('cart/products', ['as' => 'front.cart.products', 'uses' => 'CartController@productsList']);

    Route::resource('news', 'NewsController');
    Route::get('cabinet', ['as' => 'cabinet.index', 'uses' => 'CabinetController@index'])->middleware(['auth']);
    Route::get('page/{id}', ['as' => 'front.page.show', 'uses' => 'PageController@show']);
    Route::get('/{alias}', ['as' => 'front.page.alias', 'uses' => 'PageController@alias']);

//    Route::get('news', ['as' => 'front.news.index', 'uses' => 'NewsController@index']);
//    Route::get('news/{id}', ['as' => 'front.news.show', 'uses' => 'NewsController@show']);
    Route::post('search/any', ['as' => 'front.search.any', 'uses' => 'SearchController@searchByAny']);
    Route::post('search/sku', ['as' => 'front.search.sku', 'uses' => 'SearchController@searchBySku']);
    Route::post('search/avtotek', ['as' => 'front.search.avtotek', 'uses' => 'SearchController@searchAvtotek']);
    Route::get('search/{article}/{brand}', ['as' => 'front.search.articleBrand', 'uses' => 'SearchController@articleBrand']);
    Route::post('trinity/basket/add', ['as' => 'front.trinity.basket.add', 'uses' => 'TrinityController@addBasket']);

    Route::post('index/orderCall', ['as' => 'front.index.orderCall', 'uses' => 'IndexController@orderCall']);
    Route::post('index/feedback', ['as' => 'front.index.feedback', 'uses' => 'IndexController@feedback']);

    Route::post('armtek/search', ['as' => 'armtek.search', 'uses' => 'ArmtekController@search']);
    Route::post('armtek/basket/add', ['as' => 'armtek.basket.add', 'uses' => 'ArmtekController@addBasket']);

    Route::post('favorit/basket/add', ['as' => 'favorit.basket.add', 'uses' => 'FavoritController@addBasket']);
    Route::post('forumauto/basket/add', ['as' => 'forumauto.basket.add', 'uses' => 'ForumAutoController@addBasket']);

//    Route::get('tires/index', ['as' => 'front.tires.index', 'uses' => 'TiresWheelsController@indexTires']);
//    Route::post('tires/tire', ['as' => 'front.tires.tire', 'uses' => 'TiresWheelsController@showTire']);
//    Route::post('tires/basket/add', ['as' => 'front.tires.basket', 'uses' => 'TiresWheelsController@basketTires']);
//    Route::get('tires/filter', ['as' => 'front.tires.filter', 'uses' => 'TiresWheelsController@filterTires']);

//    Route::get('wheels/filter', ['as' => 'front.wheels.filter', 'uses' => 'TiresWheelsController@filterWheels']);
//
//    Route::get('wheels/index', ['as' => 'front.wheels.index', 'uses' => 'TiresWheelsController@indexWheels']);
//    Route::post('wheels/wheel', ['as' => 'front.wheels.wheel', 'uses' => 'TiresWheelsController@showWheel']);
//    Route::post('wheel/basket/add', ['as' => 'front.wheels.basket', 'uses' => 'TiresWheelsController@basketWheel']);


    Route::post('stparts/basket/add', ['as' => 'stparts.basket', 'uses' => 'StpartsController@basket']);


    Route::get('avtoto/search', ['as' => 'front.avtoto.search', 'uses' => 'AvtotoController@search']);
    Route::post('avtoto/basket/add', ['as' => 'front.avtoto.basket.add', 'uses' => 'AvtotoController@addBasket']);

    Route::any('payment/check', ['as' => 'front.payment.check', 'uses' => 'PayController@checkorder']);
    Route::any('payment/aviso', ['as' => 'front.payment.aviso', 'uses' => 'PayController@paymentaviso']);
    Route::any('payment/success', ['as' => 'front.payment.aviso', 'uses' => 'PayController@paysuccess']);
    Route::any('payment/fail', ['as' => 'front.payment.aviso', 'uses' => 'PayController@payfail']);

    Route::group(['middleware' => ['auth', 'level:2']], function () {

        Route::post('city/search', ['as' => 'cabinet.city.search', 'uses' => 'CityController@search']);
        Route::get('cart/index', ['as' => 'front.cart.index', 'uses' => 'CartController@index']);
        Route::get('cabinet/profile', ['as' => 'cabinet.profile', 'uses' => 'CabinetController@profile']);

        Route::get('cabinet/return', ['as' => 'cabinet.return', 'uses' => 'CabinetController@returnIndex']);
        Route::get('cabinet/return/create', ['as' => 'return.create', 'uses' => 'CabinetController@returnCreate']);
        Route::get('cabinet/return/show/{id}', ['as' => 'return.show', 'uses' => 'CabinetController@returnShow']);
        Route::post('cabinet/return', ['as' => 'cabinet.return', 'uses' => 'CabinetController@returnAction']);

        Route::get('cabinet/orders', ['as' => 'cabinet.orders', 'uses' => 'CabinetController@orders']);
        Route::get('cabinet/orders/show/{id}', ['as' => 'cabinet.orders.show', 'uses' => 'CabinetController@orderShow']);
        Route::get('cabinet/feedback', ['as' => 'cabinet.feedback', 'uses' => 'CabinetController@feedback']);
        Route::get('cabinet/dialog/{id}', ['as' => 'dialog.index', 'uses' => 'CabinetController@dialogIndex']);
        Route::get('cabinet/password', ['as' => 'cabinet.password', 'uses' => 'CabinetController@password']);
        Route::post('cabinet/password/update/{id}', ['as' => 'cabinet.password.update', 'uses' => 'CabinetController@updatePassword']);
        Route::post('cabinet/profile/update/{id}', ['as' => 'cabinet.profile.update', 'uses' => 'CabinetController@update']);
        Route::post('cabinet/feedback/store', ['as' => 'cabinet.feedback.store', 'uses' => 'CabinetController@question']);
        Route::post('cabinet/dialog/store/{id}', ['as' => 'cabinet.dialog.store', 'uses' => 'CabinetController@dialogStore']);
    });

    Route::get('/social_login/{provider}', 'SocialController@login');
    Route::get('/social_login/{provider}/callback', 'SocialController@callback');
});

