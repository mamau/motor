<?php
/**
 * Created by PhpStorm.
 * User: mamau
 * Date: 28.01.17
 * Time: 20:32
 */
// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
	$breadcrumbs->push('Главная', route('index'));
});

Breadcrumbs::register('categories', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push(trans('interface.panel.categories.title'), route('front.categories'));
});

Breadcrumbs::register('page', function($breadcrumbs, $page)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push($page->name, route('front.page.show',['id' => $page->id]));
});

Breadcrumbs::register('cabinet', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push(trans('interface.page.cabinet.title'), route('cabinet.index'));
});

Breadcrumbs::register('profile', function($breadcrumbs)
{
	$breadcrumbs->parent('cabinet');
	$breadcrumbs->push('Изменить профиль', route('cabinet.profile'));
});

Breadcrumbs::register('return', function($breadcrumbs)
{
	$breadcrumbs->parent('cabinet');
	$breadcrumbs->push('Возвраты', route('cabinet.return'));
});

Breadcrumbs::register('wheels', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push(trans('interface.page.wheels.title'), route('front.wheels.index'));
});

Breadcrumbs::register('tires', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push(trans('interface.page.tires.title'), route('front.tires.index'));
});


Breadcrumbs::register('return_show', function($breadcrumbs, $name)
{
	$breadcrumbs->parent('return');
	$breadcrumbs->push($name);
});

Breadcrumbs::register('return_create', function($breadcrumbs)
{
	$breadcrumbs->parent('return');
	$breadcrumbs->push('Создать возврат');
});

Breadcrumbs::register('orders', function($breadcrumbs)
{
	$breadcrumbs->parent('cabinet');
	$breadcrumbs->push('История заказов', route('cabinet.orders'));
});

Breadcrumbs::register('order_show', function($breadcrumbs, $order)
{
	$breadcrumbs->parent('orders');
	$breadcrumbs->push('Заказ №'.$order->id.' от '.$order->created_at->format('d.m.Y H:i'), route('cabinet.orders'));
});

Breadcrumbs::register('feedback', function($breadcrumbs)
{
	$breadcrumbs->parent('cabinet');
	$breadcrumbs->push('Обратная связь', route('cabinet.feedback'));
});

Breadcrumbs::register('dialog', function($breadcrumbs, $dialog)
{
	$breadcrumbs->parent('feedback');
	$breadcrumbs->push($dialog->title, route('dialog.index',['id' => $dialog->id]));
});

Breadcrumbs::register('password', function($breadcrumbs)
{
	$breadcrumbs->parent('cabinet');
	$breadcrumbs->push('Изменить пароль', route('cabinet.password'));
});

Breadcrumbs::register('news', function($breadcrumbs, $news)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push($news->title, route('front.news.show',['id' => $news->id]));
});

Breadcrumbs::register('search', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Поиск');
});

Breadcrumbs::register('category', function($breadcrumbs, $category)
{
	$breadcrumbs->parent('categories');

	 Helpers::parentCategory($category, $breadcrumbs);

	$breadcrumbs->push($category->name, route('front.categories.show', $category->id));
});

Breadcrumbs::register('product', function($breadcrumbs, $product)
{
	$prev = explode('/',url()->previous());
	$parent_cat = $product->categoryById(end($prev));
	if(!is_null($parent_cat)){
		$breadcrumbs->parent('category',$parent_cat);
	}else{
		$breadcrumbs->parent('categories');
	}
	$breadcrumbs->push($product->name, route('front.products.show', $product->id));
});

