<?php
/**
 * Created by PhpStorm.
 * User: mamau
 * Date: 15.12.16
 * Time: 16:29
 */
return[
	//Страницы
	'page.balance.title' => 'Баланс',
	'page.orders.title' => 'Заказы',
	'page.orders.main.title' => 'Заказы из приложения',
	'page.news.title' => 'Новости и акции',
	'page.myapp.title' => 'Мое приложение',
	'page.settings.title' => 'Настройки',
	'page.main.title' => 'Запчасти для иномарок. Интернет-магазин Motor. Заказа любых запчастей.',
	'user.newAuth' => 'Новый пользователь',


	'panel.profile.title' => 'Профиль',
	'panel.orders.title' => 'Заказы',
	'panel.users.title' => 'Пользователи',
	'panel.users_list.title' => 'Список пользователей',
	'panel.create_users.title' => 'Добавить пользователя',
	'panel.users_trash.title' => 'Корзина пользователей',
	'panel.manufacturer.title' => 'Производители',
	'panel.manufacturer_list.title' => 'Список производителей',
	'panel.create_manufacturer.title' => 'Добавить производителя',
	'panel.categories.title' => 'Категории',
	'panel.categories_list.title' => 'Список категорий',
	'panel.create_categories.title' => 'Добавить категорию',
	'panel.categories_trash.title' => 'Корзина категорий',
	'panel.products.title' => 'Товары',
	'panel.products_list.title' => 'Список товаров',
	'panel.create_products.title' => 'Добавить товар',
	'panel.products_trash.title' => 'Корзина товаров',
	'panel.pages.title' => 'Страницы',
	'panel.pages_list.title' => 'Список страниц',
	'panel.pages_create.title' => 'Создать страницу',
	'panel.pages_edit.title' => 'Редактировать страницу',
	'panel.settings.title' => 'Настройки',
	'panel.main_page.title' => 'Основная инфа',
	'panel.banner.title' => 'Баннеры',
	'panel.create_banner.title' => 'Создать баннер',
	'panel.edit_banner.title' => 'Редактировать баннер',

	'panel.price.index' => 'Прайсы',
	'panel.rback.index' => 'Возвраты',
	'panel.markup.index' => 'Наценка',
	'panel.status.index' => 'Статусы заказа',
	'panel.status.create' => 'Создать статус',

    'panel.parse.index' => 'Парсинг новостей',
    'panel.parse.title' => 'Парсинг новостей с сайта',

	'page.cabinet.title' => 'Личный кабинет',


	'panel.news.title' => 'Новости',
	'panel.news_list.title' => 'Список новостей',
	'panel.create_news.title' => 'Добавить новость',
	'panel.edit_news.title' => 'Редактировать новость',

	'page.categories.title' => 'Категории автозапчастей.',
	'page.cart.title' => 'Корзина',
	'page.wheels.title' => 'Диски на автомобиль',
	'page.tires.title' => 'Шины на автомобиль',
];
