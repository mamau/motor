@extends('admin.layouts.app')
@section('title')
    Регистрация
@stop
@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/chosen/chosen.css')}}">
    @parent
    <link rel="stylesheet" href="{{asset('ace/components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css')}}">
@endsection

@section('content')
@include("admin.layouts._main.header")
    <div class="container">
        <div class="row">
            <div class="widget-box">
                <div class="widget-header widget-header-blue widget-header-flat">
                    <h4 class="widget-title lighter">Заполните анкету</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div id="fuelux-wizard-container">
                            <div>
                                <ul class="steps">
                                    <li data-step="1" class="active">
                                        <span class="step">1</span>
                                        <span class="title">О компании</span>
                                    </li>

                                    <li data-step="2">
                                        <span class="step">2</span>
                                        <span class="title">Внешний вид</span>
                                    </li>

                                    <li data-step="3">
                                        <span class="step">3</span>
                                        <span class="title">Каталог товаров</span>
                                    </li>

                                    <li data-step="4">
                                        <span class="step">4</span>
                                        <span class="title">Корзина и заказ</span>
                                    </li>

                                    <li data-step="5">
                                        <span class="step">5</span>
                                        <span class="title">AppStore и GooglePlay</span>
                                    </li>
                                </ul>
                            </div>
                            <hr />
                            <div class="step-content pos-rel" data-current-step="{{$data->step}}">
                                <!--Шаг 1-->
                                    <div class="step-pane active" data-step="1">
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">
                                                <i class="ace-icon fa fa-times"></i>
                                            </button>
                                            <strong>Анкета организации!</strong> Заполните эти данные, чтобы ваши покупатели знали с кем имеют дело.
                                            <br />
                                        </div>
                                    <form class="form-horizontal" id="from-step1">
                                        <div class="form-group">
                                            <label for="inputWarning" class="col-xs-12 col-sm-3 control-label no-padding-right">
                                                Название компании
                                            </label>
                                            <div class="col-xs-12 col-sm-5">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="text" name="name" id="name" value="{{$data->name}}"  class="width-100" />
                                                    <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputError" class="col-xs-12 col-sm-3 col-md-3 control-label no-padding-right">
                                                Сайт
                                            </label>

                                            <div class="col-xs-12 col-sm-5">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="text" name="url" id="url" value="{{$data->url}}" class="width-100" />
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSuccess" class="col-xs-12 col-sm-3 control-label no-padding-right">
                                                Email
                                            </label>
                                            <div class="col-xs-12 col-sm-5">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="email" name="email" id="email" value="{{$data->email}}" class="width-100" />
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--Шаг 1-->

                                <!--Шаг 2-->
                                    <div class="step-pane" data-step="2">

                                        <div class="row">
                                            <div class="col-xs-8">
                                                <form id="from-step2" >
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-12">
                                                            <label class="control-label no-padding-left">
                                                                Цвет кнопок меню
                                                            </label>
                                                            <div class="form-group">
                                                                <span class="block input-icon input-icon-right">
                                                                    <input type="text" name="menu_btn_color" id="menu_btn_color" value="{{$data->menu_btn_color}}" class="form-control " />
                                                                    <i class="ace-icon fa fa-stop" style="color:{{$data->menu_btn_color}}"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <label class="control-label no-padding-left">
                                                                Цвет навбара
                                                            </label>
                                                            <div class="form-group">
                                                                <span class="block input-icon input-icon-right">
                                                                    <input type="text" name="menu_nav_color" id="menu_nav_color" value="{{$data->menu_nav_color}}" class="form-control" />
                                                                    <i class="ace-icon fa fa-stop" style="color:{{$data->menu_nav_color}}"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <label class="control-label no-padding-left">
                                                                Цвет элементов навбара
                                                            </label>
                                                            <div class="form-group">
                                                                <span class="block input-icon input-icon-right">
                                                                    <input type="text" name="menu_elem_color" id="menu_elem_color" value="{{$data->menu_elem_color}}" class="form-control" />
                                                                    <i class="ace-icon fa fa-stop" style="color:{{$data->menu_elem_color}}"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div id="check-img-s2">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="control-label no-padding-left">
                                                                Логотип
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 image-required">

                                                            <div class="full-width-editable">
                                                                <img src="{{$logo}}" class="editable img-responsive" id="logotype"  data-value="{{$logo}}">
                                                                <div class="loading-logotype" style="display: none;">
                                                                    <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div id="menu_elem_color-error" class="help-block-image hidden">Пожалуйста загрузите изображение.</div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="space-8"></div>
                                                        <div class="col-xs-12">
                                                            <label class="control-label no-padding-left">
                                                                Промо-изображения
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12 text-center promo-imgs image-required">
                                                            <div class="full-width-editable">
                                                                <img src="{{$promo1}}" class="editable img-responsive" id="promo1"  data-value="{{$promo1}}">
                                                                <div class="loading-promo1" style="display: none;">
                                                                    <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                                </div>
                                                            </div>

                                                            <div id="menu_elem_color-error" class="help-block-image hidden">Пожалуйста загрузите изображение.</div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12 text-center promo-imgs image-required">

                                                            <div class="full-width-editable">
                                                                <img src="{{$promo2}}" class="editable img-responsive" id="promo2"  data-value="{{$promo2}}">
                                                                <div class="loading-promo2" style="display: none;">
                                                                    <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                                </div>
                                                            </div>

                                                            <div id="menu_elem_color-error" class="help-block-image hidden">Пожалуйста загрузите изображение.</div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12 text-center promo-imgs image-required">
                                                            <div class="full-width-editable">
                                                                <img src="{{$promo3}}" class="editable img-responsive" id="promo3"  data-value="{{$promo3}}">
                                                                <div class="loading-promo3" style="display: none;">
                                                                    <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div id="menu_elem_color-error" class="help-block-image hidden">Пожалуйста загрузите изображение.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 wizard-prototype">
                                                <img src="{{asset('img/prototype.jpg')}}" class="img-responsive">
                                            </div>
                                        </div>

                                    </div>
                                <!--Шаг 2-->

                                <!--Шаг 3-->
                                <div class="step-pane" data-step="3">
                                    <form id="from-step3">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-12">
                                                <label class="control-label no-padding-left">
                                                    Ссылка на файл с товарами для Яндекс.Маркет
                                                </label>
                                                <div class="form-group">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="url_yml" id="url_yml" value="{{$data->url_yml}}" class="form-control" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--Шаг 3-->

                                <!--Шаг 4-->
                                    <div class="step-pane" data-step="4">
                                        <form id="from-step4">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        E-mail для заказов
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="email_order" id="email_order" value="{{$data->email_order}}" class="form-control" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Контактный телефон
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="phone_order" id="phone_order" value="{{$data->phone_order}}" class="form-control input-mask-phone" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Сумма минимального заказа
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="number" name="min_amount" id="min_amount" value="{{$data->min_amount}}" class="form-control" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Цены и виды доставки
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <textarea class="form-control" name="delivery" id="delivery">{{$data->delivery}}</textarea>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Адреса магазинов розничной торговли
                                                        или пунктов самовывоза
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <textarea class="form-control" name="address" id="address">{{$data->address}}</textarea>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                <!--Шаг 4-->

                                <!--Шаг 5-->
                                    <div class="step-pane" data-step="5">
                                        <form id="from-step5">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Основной язык приложения
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <select class="chosen-select form-control" id="lang_id" name="lang_id" data-placeholder="Выбрать язык">
                                                                @foreach($languages as $lang)
                                                                    <option value="{{$lang->id}}">{{$lang->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Валюта, которая будет использоваться в приложении
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <select class="chosen-select form-control" id="currency_id" name="currency_id" data-placeholder="Выбрать валюту">
                                                                @foreach($currencies as $curr)
                                                                    <option value="{{$curr->id}}">{{$curr->code}}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Название в телефоне (до 15 символов)
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" value="{{$data->phone_title}}" name="phone_title" id="phone_title" class="form-control limited" maxlength="15">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Название в магазине (до 30 символов)
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" value="{{$data->store_title}}" name="store_title" id="store_title" class="form-control limited" maxlength="30">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Краткое описание (до 80 символов)
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <textarea name="store_short_desc" id="store_short_desc" class="form-control limited" maxlength="80">{{$data->store_short_desc}}</textarea>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Полное описание (до 4000 символов)
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <textarea name="store_desc" id="store_desc" class="form-control limited" maxlength="4000">{{$data->store_desc}}</textarea>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label class="control-label no-padding-left">
                                                        Ключевые слова (через запятую, до 100 символов)
                                                    </label>
                                                    <div class="form-group">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="store_keywords" id="store_keywords" value="{{$data->store_keywords}}"  >
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                            <div class="row" id="check-img-s5">
                                                <div class="col-sm-6 col-xs-12 text-left promo-imgs">
                                                    <label class="control-label no-padding-left">
                                                        Иконка приложения
                                                    </label>
                                                    <div class="form-group image-required">
                                                        <div class="full-width-editable">
                                                            <img id="app_icon" class="editable img-responsive" alt="company app_icon" src="{{$app_icon}}" data-value="{{$app_icon}}" />
                                                            <div class="loading-app_icon" style="display: none;">
                                                                <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                            </div>
                                                        </div>
                                                        <div class="help-block-image hidden">
                                                            Пожалуйста загрузите изображение.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 text-left promo-imgs">
                                                    <label class="control-label no-padding-left">
                                                        Изображение для раздела Рекомендуемые
                                                    </label>
                                                    <div class="form-group image-required">
                                                        <div class="full-width-editable">
                                                            <img id="recomend_img" class="editable img-responsive" alt="company recomend_img" src="{{$recomend_img}}" data-value="{{$recomend_img}}" />
                                                            <div class="loading-recomend_img" style="display: none;">
                                                                <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                            </div>
                                                        </div>
                                                        <div class="help-block-image hidden">
                                                            Пожалуйста загрузите изображение.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                <!--Шаг 5-->
                            </div>
                        </div>
                        <hr />
                        <div class="wizard-actions">
                            <button class="btn btn-prev pull-left">
                                <i class="ace-icon fa fa-arrow-left"></i>
                                Назад
                            </button>
                            <button class="btn btn-info btn-save" data-last="Сохранить">
                                <i class="ace-icon fa fa-check"></i>
                                Сохранить
                            </button>
                            <button class="btn btn-success btn-next" data-last="Завершить">
                                Далее
                                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{asset('ace/components/fuelux/js/wizard.min.js')}}"></script>
    <script src="{{asset('ace/components/jquery-validation/dist/jquery.validate.js')}}"></script>
    <script src="{{asset('ace/components/jquery-validation/dist/additional-methods.js')}}"></script>
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.wizard.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('ace/components/_mod/x-editable/bootstrap-editable.js')}}"></script>
    <script src="{{asset('ace/components/_mod/x-editable/ace-editable.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/jquery-inputlimiter/jquery.inputlimiter.js')}}"></script>
    <script src="{{asset('ace/components/jquery.maskedinput/dist/jquery.maskedinput.js')}}"></script>
    <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('ace/components/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('js/wizard/wizard.js')}}"></script>
@endsection
