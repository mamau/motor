@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.page.balance.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.useSidebar', ['active' => 'balance'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h1>
                                {{ trans('interface.page.balance.title') }}
                            </h1>
                        </div>
                        <div class="well form-inline">
                            <div class="pull-right">
                                <div class="form-group">
                                    <span class="input-icon">
                                        <input type="number" data-number-only="" id="balanceCount" name="balance" autocomplete="off" value="" placeholder="Введите сумму" class="form-control">
                                        <i class="ace-icon fa fa-sign-out blue"></i>
                                    </span>
                                    <a href="#" class="btn btn-info btn-sm checkout-action" onclick="showBalanceModal()">Пополнить</a>
                                </div>
                            </div>
                            <h1 class="blue">
                                <i class="ace-icon fa fa-rub"></i> {{Auth::user()->getBalanceSum()}}
                            </h1>
                        </div>

                        <div class="space-30"></div>
                        <div class="widget-box transparent">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-credit-card green"></i>&nbsp;
                                    Операции со счетом
                                </h4>


                                <ul class="nav nav-pills nav-xs pull-right">
                                    <li>
                                        <a href="#">Посмотреть все</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main no-padding iterations">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead class="thin-border-bottom">
                                            <tr>
                                                <th><i class="ace-icon fa fa-caret-right blue"></i>
                                                    Дата
                                                </th>
                                                <th><i class="ace-icon fa fa-caret-right blue"></i>
                                                    Операция
                                                </th>
                                                <th><i class="ace-icon fa fa-caret-right blue"></i>
                                                    Сумма
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                                @forelse($balances as $bal)
                                                    <tr>
                                                        <td>{{ $bal->created_at->format('d.m.Y') }}</td>
                                                        <td>
                                                            {{ $bal->comment }}
                                                        </td>
                                                        <td>
                                                            {{ $bal->sum }}
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td class="text-center" colspan="3">
                                                            У вас нет операций со счетом
                                                        </td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('admin.user.dialog.balance')
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('js/balance/balance.js')}}"></script>

@endsection
