@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.page.myapp.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/chosen/chosen.css')}}">
    @parent
    <link rel="stylesheet" href="{{asset('ace/components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css')}}">
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.useSidebar', ['active' => 'myapp'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h1>
                                        {{ trans('interface.page.myapp.title') }}
                                    </h1>
                                </div>
                                <div class="col-sm-6 col-xs-12 text-right">

                                    <button class="btn btn-sm btn-info {{!Auth::user()->edit?'hidden':''}}" onclick="publicChanges({{Auth::user()->id}})" id="publicBtn">Опубликовать изменения</button>

                                    <button class="btn btn-sm btn-info {{!Auth::user()->edit?'':'hidden'}}" onclick="payReload('{{config('settings.pay_for_changes')}}','{{Auth::user()->getBalanceSum()}}','{{Auth::user()->id}}')" id="changeBtn">
                                        Внести изменения
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                            <ul class="nav nav-tabs padding-16">
                                <li class="{{(session('tab') == 'main')?'active':''}}">
                                    <a href="#main" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-user bigger-125"></i>
                                        О компании
                                    </a>
                                </li>

                                <li class="{{(session('tab') == 'facade')?'active':''}}">
                                    <a href="#facade" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-mobile bigger-125"></i>
                                        Внешний вид
                                    </a>
                                </li>
                                <li class="{{(session('tab') == 'more')?'active':''}}">
                                    <a href="#more" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-id-card-o bigger-125"></i>
                                        Каталог товаров
                                    </a>
                                </li>
                                <li class="{{(session('tab') == 'shop')?'active':''}}">
                                    <a href="#shop" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-shopping-cart bigger-125"></i>
                                        Корзина и заказ
                                    </a>
                                </li>
                                <li class="{{(session('tab') == 'store')?'active':''}}">
                                    <a href="#store" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-apple bigger-125"></i>
                                        AppStore и GooglePlay
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content profile-edit-tab-content">

                                <!--О компании-->
                                <div class="tab-pane {{!Auth::user()->edit?'disable-all':''}} {{(session('tab') == 'main')?'active':''}}" id="main">
                                    <h4 class="header blue bolder smaller">
                                        О компании
                                    </h4>
                                    <form method="post" action="{{url('myapp', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="1">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Имя
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <input type="text" class="form-control" name="name" placeholder="Вашe имя" value="{{$data->name}}">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('name') }}</small>
                                                        </strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Сайт
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <input type="text" class="form-control" name="url" placeholder="Ваш сайт" value="{{$data->url}}">
                                                @if ($errors->has('url'))
                                                    <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('url') }}</small>
                                                        </strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                E-mail
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <input type="email" class="form-control" name="email" placeholder="Ваш email адрес" value="{{$data->email}}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('email') }}</small>
                                                        </strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <!--О компании-->

                                <!--Внешний вид-->
                                <div class="tab-pane {{!Auth::user()->edit?'disable-all':''}} {{(session('tab') == 'facade')?'active':''}}" id="facade">
                                    <h4 class="header blue bolder smaller">
                                        Внешний вид
                                    </h4>
                                    <form method="post" action="{{url('myapp', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="2">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Цвет кнопок меню
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control" id="menu_btn_color" name="menu_btn_color" placeholder="Цвет кнопок меню" value="{{$data->menu_btn_color}}">
                                                <i class="ace-icon fa fa-stop" style="color:{{$data->menu_btn_color}}"></i>
                                                    @if ($errors->has('menu_btn_color'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('menu_btn_color') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Цвет навбара
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control" id="menu_nav_color" name="menu_nav_color" placeholder="Цвет навбара" value="{{$data->menu_nav_color}}">
                                                <i class="ace-icon fa fa-stop" style="color:{{$data->menu_nav_color}}"></i>
                                                    @if ($errors->has('menu_nav_color'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('menu_nav_color') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Цвет элементов навбара
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control" id="menu_elem_color" name="menu_elem_color" placeholder="Цвет навбара" value="{{$data->menu_elem_color}}">
                                                <i class="ace-icon fa fa-stop" style="color:{{$data->menu_elem_color}}"></i>
                                                    @if ($errors->has('menu_elem_color'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('menu_elem_color') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Логотип
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="thumb-myapp">

                                                    <img id="logotype" class="editable img-responsive" alt="company logotype" src="{{$logo}}" />
                                                    <div class="loading-logotype" style="display: none;">
                                                        <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                    </div>

                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Промо-изображения
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-12">
                                                        <span class="thumb-myapp">
                                                            <img id="promo1" class="editable img-responsive" alt="company promo1" src="{{$promo1}}" />
                                                            <div class="loading-promo1" style="display: none;">
                                                                <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <span class="thumb-myapp">
                                                            <img id="promo2" class="editable img-responsive" alt="company promo2" src="{{$promo2}}" />
                                                            <div class="loading-promo2" style="display: none;">
                                                                <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <span class="thumb-myapp">
                                                            <img id="promo3" class="editable img-responsive" alt="company promo3" src="{{$promo3}}" />
                                                            <div class="loading-promo3" style="display: none;">
                                                                <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--Внешний вид-->

                                <!--Каталог товаров-->
                                <div class="tab-pane {{!Auth::user()->edit?'disable-all':''}} {{(session('tab') == 'more')?'active':''}}" id="more">
                                    <h4 class="header blue bolder smaller">
                                        Каталог товаров
                                    </h4>
                                    <form method="post" action="{{url('myapp', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="3">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Ссылка на файл с товарами для Яндекс.Маркет
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control" id="url_yml" name="url_yml" placeholder="Ссылка на файл с товарами для Яндекс.Маркет" value="{{$data->url_yml}}">
                                                    @if ($errors->has('url_yml'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('url_yml') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--Каталог товаров-->

                                <!--Корзина и заказ-->
                                <div class="tab-pane {{!Auth::user()->edit?'disable-all':''}} {{(session('tab') == 'shop')?'active':''}}" id="shop">
                                    <h4 class="header blue bolder smaller">
                                        Корзина и заказ
                                    </h4>
                                    <form method="post" action="{{url('myapp', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="4">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                E-mail для заказов
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="email" class="form-control" id="email_order" name="email_order" placeholder="E-mail для заказов" value="{{$data->email_order}}">
                                                    @if ($errors->has('email_order'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('email_order') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Контактный телефон
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control input-mask-phone" id="phone_order" name="phone_order" placeholder="Контактный телефон" value="{{$data->phone_order}}">
                                                    @if ($errors->has('phone_order'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('phone_order') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Сумма минимального заказа
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="number" class="form-control" id="min_amount" name="min_amount" placeholder="Сумма минимального заказа" value="{{$data->min_amount}}">
                                                    @if ($errors->has('min_amount'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('min_amount') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Цены и виды доставки
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <textarea class="form-control" name="delivery" id="delivery">{{$data->delivery}}</textarea>
                                                    @if ($errors->has('delivery'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('delivery') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Адреса магазинов розничной торговли или пунктов самовывоза
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <textarea class="form-control" name="address" id="address">{{$data->address}}</textarea>
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('address') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <!--Корзина и заказ-->

                                <!--AppStore и GooglePlay-->
                                <div class="tab-pane {{!Auth::user()->edit?'disable-all':''}} {{(session('tab') == 'store')?'active':''}}" id="store">
                                    <h4 class="header blue bolder smaller">
                                        AppStore и GooglePlay
                                    </h4>
                                    <form method="post" action="{{url('myapp', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="5">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Основной язык приложения
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                    <select class="chosen-select form-control"  id="lang_id" name="lang_id" data-placeholder="Выбрать язык">
                                                        @foreach($languages as $lang)
                                                            <option value="{{$lang->id}}"
                                                                    {{($lang->id==$data->lang_id)?'selected':''}}
                                                            >{{$lang->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('lang_id'))
                                                        <span class="help-block">
                                                            <strong>
                                                                <small>{{ $errors->first('lang_id') }}</small>
                                                            </strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Валюта, которая будет использоваться в приложении
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                    <select class="chosen-select form-control" id="currency_id" name="currency_id" data-placeholder="Выбрать валюту">
                                                                @foreach($currencies as $curr)
                                                            <option value="{{$curr->id}}"
                                                                    {{($curr->id==$data->currency_id)?'selected':''}}
                                                            >{{$curr->code}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('currency_id'))
                                                        <span class="help-block">
                                                            <strong>
                                                                <small>{{ $errors->first('currency_id') }}</small>
                                                            </strong>
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Название в телефоне (до 15 символов)
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control limited" maxlength="15" id="phone_title" name="phone_title" placeholder="Название в телефоне (до 15 символов)" value="{{$data->phone_title}}">
                                                    @if ($errors->has('phone_title'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('phone_title') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Название в магазине (до 30 символов)
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control limited" maxlength="30" id="store_title" name="store_title" placeholder="Название в магазине (до 30 символов)" value="{{$data->store_title}}">
                                                    @if ($errors->has('store_title'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('store_title') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Краткое описание (до 80 символов)
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <textarea class="form-control limited" maxlength="80" name="store_short_desc" id="store_short_desc">{{$data->store_short_desc}}</textarea>
                                                    @if ($errors->has('store_short_desc'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('store_short_desc') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Полное описание (до 4000 символов)
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">
                                                <textarea class="form-control limited" maxlength="4000" name="store_desc" id="store_desc">{{$data->store_desc}}</textarea>
                                                    @if ($errors->has('store_desc'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('store_desc') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Ключевые слова (через запятую, до 100 символов)
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="block input-icon input-icon-right">

                                                    <input type="text" name="store_keywords" id="store_keywords" value="{{$data->store_keywords}}"  >

                                                    @if ($errors->has('store_keywords'))
                                                        <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('store_keywords') }}</small>
                                                        </strong>
                                                    </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Иконка приложения
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="thumb-myapp">

                                                    <img id="app_icon" class="editable img-responsive" alt="company app_icon" src="{{$app_icon}}" />
                                                    <div class="loading-app_icon" style="display: none;">
                                                        <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                    </div>

                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Изображение для раздела Рекомендуемые
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <span class="thumb-myapp">

                                                    <img id="recomend_img" class="editable img-responsive" alt="company recomend_img" src="{{$recomend_img}}" />
                                                    <div class="loading-recomend_img" style="display: none;">
                                                        <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
                                                    </div>

                                                </span>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <!--AppStore и GooglePlay-->
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix form-actions {{!Auth::user()->edit?'disable-all':''}}">
                                <div class="col-xs-12 text-center">
                                    <button type="button" id="sendActive" class="btn btn-info" data-loading-text="сохранение">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('ace/components/_mod/x-editable/bootstrap-editable.js')}}"></script>
    <script src="{{asset('ace/components/_mod/x-editable/ace-editable.js')}}"></script>
    <script src="{{asset('ace/components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/jquery-inputlimiter/jquery.inputlimiter.js')}}"></script>
    <script src="{{asset('ace/components/jquery.maskedinput/dist/jquery.maskedinput.js')}}"></script>
    <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('ace/components/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('js/myapp/myapp.js')}}"></script>
@endsection
