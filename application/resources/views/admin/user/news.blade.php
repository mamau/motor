@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.page.news.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    @parent
    <link rel="stylesheet" href="{{asset('ace/components/_mod/jquery-ui.custom/jquery-ui.custom.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('ace/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}">


@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.useSidebar', ['active' => 'news'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h1>
                                        {{ trans('interface.page.news.title') }}
                                    </h1>
                                </div>
                                <div class="col-sm-6 col-xs-12 text-right">
                                    <button class="btn btn-info" onclick="addNews({{Auth::user()->id}})">Добавить новость</button>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Дата и время</th>
                                    <th>Название</th>
                                    <th>Push-уведомление</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($news as $new)
                                    <tr>
                                        <td>
                                            {{$new->created_at->format('d.m.Y H:i')}}
                                        </td>
                                        <td>
                                            {{$new->title}}
                                        </td>
                                        <td>
                                            {{($new->push_notification)?'Отправлено':'Без уведомления'}}
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/_mod/jquery-ui.custom/jquery-ui.custom.js')}}"></script>
    <script src="{{asset('ace/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('ace/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js')}}"></script>
    <script src="{{asset('ace/components/moment/moment.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('js/news/news.js')}}"></script>

@endsection
