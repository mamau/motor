<div id="modalBalanceTopUpDialog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title blue bigger">Пополнить баланс</h4>
            </div>
            <div class="modal-body">
                <div class="space-6"></div>
                <div class="">

                    <h4 class="text-center topay">К оплате <span class="green">0</span></h4>

                    <div class="space-10"></div>
                    <form action="https://money.yandex.ru/eshop.xml" method="post" autocomplete="off">

                        <!-- Обязательные поля -->
                        <input name="shopId" value="{{ config('settings.payment_kassa_shop_id') }}" type="hidden"/>
                        <input name="scid" value="{{ config('settings.payment_kassa_shop_scid') }}" type="hidden"/>
                        <input name="sum" value="" type="hidden">
                        <input name="customerNumber" value="" type="hidden"/>
                        <input name="shopSuccessURL" value="{{ URL::current().'?paymentSuccess=yes' }}" type="hidden"/>
                        <input name="shopFailURL" value="{{ URL::current().'?paymentFail=yes' }}" type="hidden"/>
                        <!-- Необязательные поля -->
                        <h3 class="text-center">Выберите способ оплаты</h3>
                        <div class="space-12"></div>
                        <div class="payments bigger-120 col-xs-10 col-xs-offset-1">
                            <div>
                                <input id="paymentTypeAC" type="radio" name="paymentType" value="AC" checked>
                                <label for="paymentTypeAC">Кредитная карта&nbsp;&nbsp;&nbsp;<img src="{{ asset('img/payments/visa.png') }}">&nbsp;<img src="{{ asset('img/payments/mastercard.png') }}"></label>
                                <div class="space-8"></div>
                            </div>
                            <div style="display: none">
                                <input id="paymentTypeSB" type="radio" name="paymentType" value="SB">
                                <label for="paymentTypeSB">Сбербанк&nbsp;&nbsp;&nbsp;<img src="{{ asset('img/payments/sberbank.png') }}"></label>
                                <div class="space-8"></div>
                            </div>
                            <div>
                                <input id="paymentTypeAB" type="radio" name="paymentType" value="AB">
                                <label for="paymentTypeAB">Альфа-Клик&nbsp;&nbsp;&nbsp;<img src="{{ asset('img/payments/alfabank-white.png') }}"></label>
                                <div class="space-8"></div>
                            </div>
                            <div>
                                <input id="paymentTypePC" type="radio" name="paymentType" value="PC">
                                <label for="paymentTypePC">Яндекс деньги&nbsp;&nbsp;&nbsp;<img src="{{ asset('img/payments/yandexmoney.png') }}"></label>
                                <div class="space-8"></div>
                            </div>
                            <div>
                                <input id="paymentTypeWM" type="radio" name="paymentType" value="WM">
                                <label for="paymentTypeWM">Webmoney&nbsp;&nbsp;&nbsp;<img src="{{ asset('img/payments/webmoney-white.png') }}"></label>
                                <div class="space-8"></div>
                            </div>
                        </div>
                        <input name="orderNumber" value="" type="hidden"/>
                        <div class="space-10"></div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-lg main-bg" data-loading-text-manual="Перейти к оплате">Перейти к оплате</button>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default cancel" type="button" data-dismiss="modal" aria-hidden="true"><i class="ace-icon fa fa-times"></i>&nbsp;Закрыть</button>
            </div>
        </div>
    </div>
</div>
