@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.page.settings.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.useSidebar', ['active' => 'settings'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h1>
                                {{ trans('interface.page.settings.title') }}
                            </h1>
                        </div>

                        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                            <ul class="nav nav-tabs padding-16">
                                <li class="active">
                                    <a href="#edit-password" data-toggle="tab">
                                        <i class="blue ace-icon fa fa-key bigger-125"></i>
                                        Сменить пароль
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content profile-edit-tab-content">

                                <div class="tab-pane active" id="edit-password">
                                    <h4 class="header blue bolder smaller">
                                        Сменить пароль
                                    </h4>
                                    <form method="post" action="{{url('settings', ['id'=>$data->id])}}" class="form-horizontal">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" id="userId" value="{{$data->id}}">
                                        <input type="hidden" name="step" value="1">
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Смена пароля
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <input type="password" name="password" class="form-control" placeholder="Укажите новый пароль">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>
                                                            <small>{{ $errors->first('password') }}</small>
                                                        </strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="control-label col-sm-3 col-xs-12">
                                                Пароль еще раз
                                            </div>
                                            <div class="col-sm-9 col-xs-12">
                                                <input type="password" name="passwordConfirm" class="form-control" placeholder="Повторите новый пароль">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix form-actions">
                                <div class="col-xs-12 text-center">
                                    <button type="button" id="sendActive" class="btn btn-info" data-loading-text="сохранение">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('js/settings/settings.js')}}"></script>
@endsection
