@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.page.orders.title') }}
@stop
@section('styles')
    @parent

@endsection
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.useSidebar', ['active' => 'orders'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h1>
                                {{ trans('interface.page.orders.main.title') }}
                            </h1>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 text-right">
                                <a class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="" >
                                    <span>
                                        <i class="fa fa-database bigger-110 orange"></i>
                                        <span class="hidden">Экспорт в CSV</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <table id="codesCheckTable" class="table table-striped table-bordered table-hover user-admin-table">
                            <thead class="thin-border-bottom">
                            <tr>
                                <th class="center sorting_disabled">
                                    <label class="pos-rel">
                                        <input type="checkbox" class="ace">
                                        <span class="lbl"></span>
                                    </label>
                                </th>
                                <th>
                                    Дата и время
                                </th>
                                <th style="width: 20%">
                                    Номер заказа
                                </th>
                                <th style="width: 50%">
                                    Наименований
                                </th>
                                <th style="width: 15%">
                                    Сумма заказа
                                </th>
                                <th class="text-center">
                                    Комиссия сервиса
                                </th>
                            </tr>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/datatables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('ace/components/_mod/datatables/jquery.dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('ace/components/datatables.net-select/js/dataTables.select.js')}}"></script>
    <script src="{{asset('js/order/order.js')}}"></script>
@endsection
