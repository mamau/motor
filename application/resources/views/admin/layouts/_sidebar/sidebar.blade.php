<div class="sidebar responsive">
    <ul class="nav nav-list">
        <li class="{{ ($active == 'products' || $active == 'create_products' || $active == 'products_list' || $active == 'products_trash') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-cubes"></i>
                <span class="menu-text"> {{ trans('interface.panel.products.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <i class="arrow"></i>

            <ul class="submenu nav-show">
                <li class="{{ $active == 'products_list' ? 'active' : ''}}">
                    <a href="{{route('products.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.products_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'create_products' ? 'active' : ''}}">
                    <a href="{{route('products.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.create_products.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="{{ $active == 'products_trash' ? 'active' : ''}}">
                    <a href="{{route('products.trash')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.products_trash.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{ ($active == 'categories' || $active == 'create_categories' || $active == 'categories_list' || $active == 'categories_trash') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-tasks"></i>
                <span class="menu-text"> {{ trans('interface.panel.categories.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <i class="arrow"></i>

            <ul class="submenu nav-show">
                <li class="{{ $active == 'categories_list' ? 'active' : ''}}">
                    <a href="{{route('categories.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.categories_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'create_categories' ? 'active' : ''}}">
                    <a href="{{route('categories.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.create_categories.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="{{ $active == 'categories_trash' ? 'active' : ''}}">
                    <a href="{{route('categories.trash')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.categories_trash.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{ ($active == 'manufacturer' || $active == 'create_manufacturer' || $active == 'manufacturer_list') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-tags"></i>
                <span class="menu-text"> {{ trans('interface.panel.manufacturer.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <i class="arrow"></i>

            <ul class="submenu nav-show">
                <li class="{{ $active == 'manufacturer_list' ? 'active' : ''}}">
                    <a href="{{route('manufacturer.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.manufacturer_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'create_manufacturer' ? 'active' : ''}}">
                    <a href="{{route('manufacturer.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.create_manufacturer.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{ ($active == 'news' || $active == 'edit_news' || $active == 'create_news' || $active == 'news_list') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text"> {{ trans('interface.panel.news.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <i class="arrow"></i>

            <ul class="submenu nav-show">
                <li class="{{ $active == 'news_list' ? 'active' : ''}}">
                    <a href="{{route('news.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.news_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'create_news' ? 'active' : ''}}">
                    <a href="{{route('news.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.create_news.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{ ($active == 'users' || $active == 'create_users' || $active == 'users_list' || $active == 'users_trash') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> {{ trans('interface.panel.users.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <i class="arrow"></i>
            <ul class="submenu nav-show">
                <li class="{{ $active == 'users_list' ? 'active' : ''}}">
                    <a href="{{route('users.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.users_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'create_users' ? 'active' : ''}}">
                    <a href="{{route('users.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.create_users.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="{{ $active == 'users_trash' ? 'active' : ''}}">
                    <a href="{{route('users.trash')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{ trans('interface.panel.users_trash.title') }}
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{ ($active == 'pages' || $active == 'pages_list' || $active == 'pages_create' || $active == 'pages_edit') ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-files-o"></i>
                <span class="menu-text"> {{ trans('interface.panel.pages.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <i class="arrow"></i>
            <ul class="submenu nav-show">
                <li class="{{ $active == 'pages_list' ? 'active' : ''}}">
                    <a href="{{route('pages.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.pages_list.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active == 'pages_create' ? 'active' : ''}}">
                    <a href="{{route('pages.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.pages_create.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>

            </ul>
        </li>

        <li
                class="{{
($active === 'settings'
|| $active === 'main_page'
|| $active === 'banners'
|| $active === 'create_banner'
|| $active === 'edit_banner'
|| $active === 'price'
|| $active === 'markup'
|| $active ===  'status'
|| $active ===  'parse'
) ? 'open' : ''}}">
            <a
                    href="#"
                    class="dropdown-toggle">
                <i class="menu-icon fa fa-cogs"></i>
                <span class="menu-text"> {{ trans('interface.panel.settings.title') }} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <i class="arrow"></i>
            <ul class="submenu nav-show">
                <li class="{{ $active === 'main_page' ? 'active' : ''}}">
                    <a href="{{route('settings.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.main_page.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active === 'banners' ? 'active' : ''}}">
                    <a href="{{route('banners.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.banner.title') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active === 'price' ? 'active' : ''}}">
                    <a href="{{route('price.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.price.index') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active === 'markup' ? 'active' : ''}}">
                    <a href="{{route('markup.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.markup.index') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active === 'status' ? 'active' : ''}}">
                    <a href="{{route('status.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.status.index') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>
                <li class="{{ $active === 'parse' ? 'active' : ''}}">
                    <a href="{{route('parse.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{ trans('interface.panel.parse.index') }} </span>
                    </a>
                    <i class="arrow"></i>
                </li>

            </ul>
        </li>

        <li class="{{ $active == 'orders' ? 'active' : ''}}">

            <a href="{{route('orders.index')}}">
                <i class="menu-icon fa fa-handshake-o"></i>
                <span class="menu-text"> {{ trans('interface.panel.orders.title') }} </span>
            </a>
            <i class="arrow"></i>
        </li>
        <li class="{{ $active == 'rback' ? 'active' : ''}}">

            <a href="{{route('rback.index')}}">
                <i class="menu-icon fa fa-thumbs-o-down"></i>
                <span class="menu-text"> {{ trans('interface.panel.rback.index') }} </span>
            </a>
            <i class="arrow"></i>
        </li>
        <li
                class="{{ $active == 'profile' ? 'active' : ''}}"
                style="display: none;">

            <a href="{{route('profile.index')}}">
                <i class="menu-icon fa fa-vcard-o"></i>
                <span class="menu-text"> {{ trans('interface.panel.profile.title') }} </span>
            </a>
            <i class="arrow"></i>
        </li>
    </ul>
</div>
