@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    @parent
@stop
<nav class="navbar navbar-default navbar-static-top">
    <div class="container no-padding">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand admin-logo" href="{{ route('admin.index') }}">
                <i class="icon-logo"></i>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <ul class="nav navbar-nav">
                &nbsp;
            </ul>


            <ul class="nav ace-nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li>
                        <a href="{{ route('admin.index') }}">Войти</a>
                    </li>
                @else
                    <li class="green dropdown-modal">
                        <a href="{{route('dialog.list')}}">
                            <i class="ace-icon fa fa-envelope {{(Helpers::unViewedMess() > 0)?'drin-this':''}}"></i>
                            <span class="badge badge-success">{{Helpers::unViewedMess()}}</span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }}
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('admin.logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
