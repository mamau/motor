<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @section('title')
            {{ config('app.name', 'FoodApp') }}
        @show
    </title>
    @section('styles')
        <link rel="stylesheet" href="{{ asset('css/admin/app.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/app_custom.min.css') }}">
    @show
</head>
<body class="no-skin">
    @yield('content')
    @section('scripts')
        <script type="text/javascript" src="{{ asset('js/admin/app.min.js') }}"></script>
        <script src="{{asset('ace/assets/js/ace.js')}}"></script>
    @show
</body>
</html>
