@extends('admin.layouts.app')

@section('title')
    Авторизация
@endsection
@section('content')
    @include("admin.layouts._main.header")
        <div class="login-layout default-style-login light-login">
            <div class="login-container">
                <div class="position-relative">
                    <!--Логин-->
                    <div id="login-box" class="login-box {{ $form == 'login' ? ' visible' : '' }} widget-box no-border">
                        <div class="widget-body">
                            <div class="widget-main">
                                <h4 class="header blue lighter bigger">
                                    <i class="ace-icon fa fa-coffee green"></i>
                                    Пожалуйста, авторизуйтесь
                                </h4>

                                <div class="space-6"></div>

                                <form method="POST" action="{{ route('admin.login') }}">
                                    {{ csrf_field() }}
                                    <fieldset>
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="email" class="form-control" placeholder="E-mail" value="{{ old('email') }}" required autofocus name="email" />
                                                        <i class="ace-icon fa fa-user"></i>
                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                                                <strong>
                                                                    <small>{{ $errors->first('email') }}</small>
                                                                </strong>
                                                            </span>
                                                        @endif
                                                    </span>

                                            </label>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" class="form-control" placeholder="Пароль" required name="password" />
                                                        <i class="ace-icon fa fa-lock"></i>
                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                                                <strong>
                                                                    <small>{{ $errors->first('password') }}</small>
                                                                </strong>
                                                            </span>
                                                        @endif
                                                    </span>
                                            </label>
                                        </div>

                                        <div class="space"></div>

                                        <div class="clearfix">
                                            <label class="inline">
                                                <input type="checkbox" class="ace" name="remember" />
                                                <span class="lbl"> Запомнить</span>
                                            </label>

                                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                <i class="ace-icon fa fa-key"></i>
                                                <span class="bigger-110">Войти</span>
                                            </button>
                                        </div>

                                        <div class="space-4"></div>
                                    </fieldset>
                                </form>
                            </div>

                            <div class="toolbar clearfix">
                                <div class="text-center">Motor <i class="fa fa-copyright"></i> 2017</div>
                            </div>
                        </div>
                    </div>
                    <!--Логин-->
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{asset('js/admin/auth.js')}}"></script>
@endsection
