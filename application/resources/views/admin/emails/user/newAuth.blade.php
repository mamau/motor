@extends('admin.emails.layouts.simple')
@section('caption')
    Новый пользователь
@stop
@section('content')
	<h3>Зарегистрировался пользователь: {{$user->name}}</h3>
    <table>
        <tr>
            <td style="text-align: left">
                E-mail:
            </td>
            <td style="text-align: left">
                <strong>{{$user->email}}</strong>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <strong>Телефон:</strong>
            </td>
            <td style="text-align: left">
                {{$user->phone_order}}
            </td>
        </tr>
    </table>
@endsection
