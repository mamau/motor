@extends('admin.layouts.app')
@section('title')
    {{$feedback->title}}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => ''])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{$feedback->title}}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <ol class="breadcrumb">
                            <li><a href="{{route('dialog.list')}}">Назад</a></li>
                        </ol>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif
                        <div class="wrapper-dialog">
                            @foreach($feedback->dialog as $dialog)
                                <div class="{{($feedback->author_id == $dialog->author_id)?'user-manager':'user-self'}}">
                                    <div class="alert alert-{{($feedback->author_id == $dialog->author_id)?'success text-left ':'info text-right'}}">
                                        <div class="header-quest">
                                            <strong>{{Helpers::userById($dialog->author_id)}}:</strong> <small>{{$dialog->updated_at->format('d.m.Y H:i')}}</small>
                                        </div>
                                        {{$dialog->text}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <h3>Ответить опоненту</h3>
                        <form action="{{route('dialog.store',['id' => $feedback->id])}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Ответ</label>
                                <input type="hidden" name="author_id" value="{{Auth::user()->id}}">
                                <textarea class="form-control" name="text" placeholder="Ваш ответ">{{old('text')}}</textarea>
                                @if ($errors->has('text'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('text') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <h5>{{($feedback->closed)?'Тема закрыта':'Закрыть тему'}}</h5>
                                <label>
                                    <input name="closed" class="ace ace-switch ace-switch-6" type="checkbox" {{($feedback->closed)?'checked':''}}>
                                    <span class="lbl"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-sm" type="submit">Ответить</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
