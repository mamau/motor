@extends('admin.layouts.app')
@section('title')
    Список вопросов
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => ''])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        Список вопросов
                                    </h1>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div class="pull-right tableTools-container">
                                    <a href="{{route('dialog.list')}}" class="btn btn-xs btn-success tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Показать открытые темы">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped tbl-dialog">
                            <tr>
                                <th>#</th>
                                <th>Автор</th>
                                <th>Тема</th>
                            </tr>
                            @foreach($feedback as $feed)
                                <tr>
                                    <td>{{$feed->id}}</td>
                                    <td>{{Helpers::userById($feed->author_id)}}</td>
                                    <td><a href="{{route('dialog.show',['id' => $feed->id])}}">{{$feed->title}}</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
