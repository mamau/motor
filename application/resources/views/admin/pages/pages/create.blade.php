@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.pages_create.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('ace/components/chosen/chosen.css') }}">
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'pages_create'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.pages_create.title') }}
                                    </h1>
                                </div>

                            </div>
                        </div>

                        <form action="{{route('pages.store')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" value="page" name="type">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Название</label>
                                        <input type="text" name="name" placeholder="О нас" class="form-control" value="{{ old('name') }}" autofocus required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('name') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('parent_page') ? ' has-error' : '' }}">
                                        <label>Родительская страница</label>
                                        <select class="form-control chosen-select" name="parent_page" data-placeholder="Выбрать страницу">
                                            <option value="0">Выбрать страницу</option>
                                            @foreach($pages as $page)

                                                <option value="{{$page->id}}" {{(old('parent_page') == $page->id ? 'selected':'')}}>{{$page->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('parent_page'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('parent_page') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                        <label>Порядок сортировки</label>
                                        <input type="number" name="sort" placeholder="1" class="form-control" value="{{ old('sort') }}">
                                        @if ($errors->has('sort'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('sort') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('menu') ? ' has-error' : '' }}">
                                        <label>Меню</label>
                                        <select class="form-control chosen-select" name="menu" data-placeholder="Выбрать Меню">
                                            <option value="0">Выбрать страницу</option>
                                            @foreach($menus as $key => $menu)

                                                <option value="{{$key}}" {{(old('menu') == $key ? 'selected':'')}}>{{$menu}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('menu'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('menu') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                        <label>Ссылка <small>Если внешний источник</small></label>
                                        <input type="text" name="url" value="{{old('url')}}" id="url" class="form-control" placeholder="http://ya.ru">
                                        @if ($errors->has('url'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('url') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('alias') ? ' has-error' : '' }}">
                                        <label>Алиас</label>
                                        <input type="text" name="alias" value="{{old('alias')}}" id="alias" class="form-control" placeholder="o_nas">
                                        @if ($errors->has('alias'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('alias') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <h3>Seo данные: </h3>
                            <div class="form-group{{ $errors->has('title_seo') ? ' has-error' : '' }}">
                                <label>Название (title)</label>
                                <input type="text" name="title_seo" placeholder="О нас" class="form-control" value="{{ old('title_seo') }}" required>
                                @if ($errors->has('title_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('title_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('keywords_seo') ? ' has-error' : '' }}">
                                <label>Ключевые слова (keywords)</label>
                                <div>
                                    <input type="text" name="keywords_seo" id="keywords_seo" class="form-control" value="{{ old('keywords_seo') }}">
                                </div>
                                @if ($errors->has('keywords_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('keywords_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description_seo') ? ' has-error' : '' }}">
                                <label>Описание (description)</label>
                                <textarea name="description_seo" class="form-control">{{ old('description_seo') }}</textarea>
                                @if ($errors->has('description_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('description_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="editor-wrapper form-group{{ $errors->has('full_description') ? ' has-error' : '' }}">
                                <label>Описание для страницы</label>
                                <div class="wysiwyg-editor" id="full_description">{{ old('full_description') }}</div>
                                <textarea name="full_description" class="form-control">{{ old('full_description') }}</textarea>
                                @if ($errors->has('full_description'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('full_description') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('ace/components/_mod/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('ace/components/jquery.hotkeys/index.min.js')}}"></script>
    <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.wysiwyg.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('js/admin/pages.js')}}"></script>
@endsection
