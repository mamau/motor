@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.price.index') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'price'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.price.index') }}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <div class="row wrapper-price">
                            <div class="preloader">
                                <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Favorit-auto</h3>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-download"></i> Получить новый прайс</h5>
                                            <button class="btn btn-primary btn-sm" onclick="uploadFavorit()">
                                                <i class="fa fa-download"></i> Получить
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-file-excel-o"></i> Разобрать файл</h5>
                                            <button class="btn btn-success btn-sm" onclick="parseFavorit('favorit')">
                                                <i class="fa fa-file-excel-o"></i> Спарсить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Forum-auto</h3>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">

                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-file-excel-o"></i> Разобрать файл</h5>
                                            <button class="btn btn-success btn-sm" onclick="parseFavorit('forum')">
                                                <i class="fa fa-file-excel-o"></i> Спарсить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="space-10"></div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Загризить диски и шины от virbactd.ru</h3>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-download"></i> Загризить диски</h5>
                                            <button class="btn btn-pink btn-sm" onclick="uploadTires()">
                                                <i class="fa fa-download"></i> Получить
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-download"></i> Загризить шины</h5>
                                            <button class="btn btn-purple btn-sm" onclick="uploadWheels()">
                                                <i class="fa fa-download"></i> Получить
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Motor 1C</h3>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">

                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <h5><i class="fa fa-file-excel-o"></i> Разобрать файл</h5>
                                            <button class="btn btn-success btn-sm" onclick="parseMotor()">
                                                <i class="fa fa-file-excel-o"></i> Спарсить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="space-10"></div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Загрузить изображения товаров</h3>
                                <form enctype="multipart/form-data" action="{{ route('admin.products.uploadZip') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <h5><i class="fa fa-image"></i> Загрузить архив zip</h5>
                                        <input type="file" class="form-control" name="files">
                                    </div>
                                    <button type="submit" class="btn btn-xs btn-primary">Отправить</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('js/admin/price/index.js')}}"></script>
@endsection
