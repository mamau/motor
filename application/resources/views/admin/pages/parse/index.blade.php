@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.parse.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include('admin.layouts._main.header')
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'parse'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.parse.index') }}
                                    </h1>
                                </div>

                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif
                        талеб антихрупкость шкура на кону
                        <form
                                action="{{route('parse.store')}}"
                                method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Страница</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        value="https://www.zr.ru/news/"
                                        placeholder="https://www.zr.ru/news/"
                                        name="url"
                                        disabled
                                >
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts._main.footer')
@endsection
@section('scripts')
    @parent
@endsection
