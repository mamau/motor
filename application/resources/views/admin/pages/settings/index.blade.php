@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.main_page.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'main_page'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.main_page.title') }}
                                    </h1>
                                </div>

                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <form action="{{route('settings.update',['id' => $settings['id']])}}" method="post">
                            {{csrf_field()}}
                            {{method_field('put')}}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('shop_name') ? ' has-error' : '' }}">
                                        <label>Название магазина</label>
                                        <input type="text" name="shop_name" class="form-control" value="{{ $settings['shop_name'] }}">
                                        @if ($errors->has('shop_name'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('shop_name') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label>Телефоны</label>
                                        <div>
                                            <input type="text" name="phone" id="phone" class="form-control" value="{{ $settings['phone'] }}">
                                        </div>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('phone') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('work_time') ? ' has-error' : '' }}">
                                        <label>Время работы</label>
                                        <input type="text" name="work_time" class="form-control" value="{{ $settings['work_time'] }}">
                                        @if ($errors->has('work_time'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('work_time') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>E-mail администратора</label>
                                        <input type="email" name="email" class="form-control" value="{{ $settings['email'] }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('email') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label>Адрес магазина</label>
                                        <textarea name="address" class="form-control" rows="5">{{$settings['address']}}</textarea>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('address') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="editor-wrapper form-group{{ $errors->has('welcome') ? ' has-error' : '' }}">
                                <label>Текст приветствия на главной</label>
                                <div class="wysiwyg-editor" id="description">{!! $settings['welcome'] !!}</div>
                                <textarea name="welcome" class="form-control">{{ $settings['welcome'] }}</textarea>
                                @if ($errors->has('welcome'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('welcome') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Обновить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('ace/components/_mod/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('ace/components/jquery.hotkeys/index.min.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.wysiwyg.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('js/admin/settings.js')}}"></script>
@endsection
