@extends('admin.layouts.app')
@section('title')
    {{ $rback->cause }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'rback'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ $rback->cause }}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="well">
                            <h4 class="blue">{{$rback->user->name}}</h4>
                            <p>Хочет сделать возврат <a target="_blank" href="{{route('orders.show',['id' => $rback->order->id])}}">по заказу № <strong>{{$rback->order->id}}</strong></a> датированный от <strong>{{$rback->order->created_at->format('d.m.Y H:i')}}</strong></p>
                            <p>Связаться с покупателем можно по телефону <strong>{{$rback->user->phone}}</strong> или написать на почту <strong>{{$rback->user->email}}</strong></p>
                        </div>
                        <div class="well">
                            <h4 class="green">Описание проблемы</h4>
                            <i>{{$rback->description}}</i>
                        </div>

                        <p>Прикреплённые изображения:</p>
                        <div class="row">
                            @forelse($rback->thumbs as $thumb)
                                <div class="col-sm-6 col-xs-12">
                                    <img src="{{Helpers::buildImage($thumb)}}" class="img-responsive">
                                </div>
                            @empty
                                <div class="col-xs-12">
                                    <p>Нет изображений</p>
                                </div>
                            @endforelse
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('js/admin/price/index.js')}}"></script>
@endsection
