@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.status.index') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'status'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.status.index') }}
                                    </h1>
                                </div>
                                <div class="col-sm-6 col-xs-12 text-right">
                                    <a href="{{route('status.create')}}" class="btn btn-sm btn-primary">Добавить</a>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <div class="wrapper-statuses">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Название</td>
                                    <td>Описание</td>
                                    <td><i class="fa fa-cogs"></i></td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($statuses as $status)
                                    <tr id="status_{{$status->id}}">
                                        <td>{{$status->id}}</td>
                                        <td>{{$status->name}}</td>
                                        <td>{{$status->description}}</td>
                                        <td>
                                            <a href="{{route('status.edit',['id' => $status->id])}}" class="btn btn-primary btn-xs tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Редактировать статус"><i class="fa fa-pencil"></i></a>
                                            <a href="javascript:void(0)" onclick="deleteStatus('{{$status->id}}')" class="btn btn-danger btn-xs tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Удалить статус"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Нет статусов</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{$statuses->links()}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('js/admin/status/index.js')}}"></script>
@endsection
