@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.status.create') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'status'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.status.create') }}
                                    </h1>
                                </div>
                                <div class="col-sm-6 col-xs-12 text-right">
                                    <a href="{{route('status.index')}}" class="btn btn-sm btn-primary">Список статусов</a>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <div class="wrapper-statuses">
                            <form action="{{route('status.store')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label>Название статуса</label>
                                            <input type="text" name="name" placeholder="Принят" class="form-control" value="{{ old('name') }}" autofocus required>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>
                                                        <small>{{ $errors->first('name') }}</small>
                                                    </strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label>Описание статуса</label>
                                            <textarea name="description" placeholder="Статус для пояснения" class="form-control">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>
                                                        <small>{{ $errors->first('description') }}</small>
                                                    </strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-sm">Создать</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('js/admin/status/index.js')}}"></script>
@endsection
