@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.create_products.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('ace/components/chosen/chosen.css') }}">
    @parent
    <link rel="stylesheet" href="{{ asset('ace/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
    <link rel="stylesheet" href="{{asset('ace/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}">
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'create_products'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.create_products.title') }}
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Название</label>
                                        <input type="text" name="name" id="name" placeholder="Лампочка" class="form-control" value="{{ old('name') }}" autofocus required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('name') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('model_product') ? ' has-error' : '' }}">
                                        <label>Модель</label>
                                        <input type="text" name="model_product" placeholder="H11" class="form-control" value="{{ old('model_product') }}" required>
                                        @if ($errors->has('model_product'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('model_product') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="img-upload-wrapper form-group{{ $errors->has('thumb') ? ' has-error' : '' }}">
                                        <label>Изображение</label>
                                        <img src="{{asset('img/noimage.png')}}" class="img-responsive" id="toggleFile">
                                        <input type="file" name="thumb" id="thumb" class="form-control" value="{{ old('thumb') }}" disabled>
                                        <input type="hidden" name="type" value="product">
                                        @if ($errors->has('thumb'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('thumb') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('sort_order') ? ' has-error' : '' }}">
                                        <label>Порядок сортировки</label>
                                        <input type="number" name="sort_order" placeholder="1" class="form-control" value="{{ old('sort_order') }}">
                                        @if ($errors->has('sort_order'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('sort_order') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                        <label>Цена</label>
                                        <input type="text" name="price" placeholder="1500" class="form-control" value="{{ old('price') }}">
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('price') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('manufacturer_id') ? ' has-error' : '' }}">
                                        <label>Производитель</label>
                                        <select class="form-control chosen-select tag-input-style" name="manufacturer_id" data-placeholder="Выбрать производителя">
                                            <option value="0">Выбрать производителя</option>
                                            @foreach($manufacturers as $manufacturer)

                                                <option value="{{$manufacturer->id}}" {{(old('manufacturer_id') == $manufacturer->id ? 'selected':'')}}>{{$manufacturer->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('manufacturer_id'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('manufacturer_id') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('minimum') ? ' has-error' : '' }}">
                                        <label>Минимум: <small>Указание минимального количества в заказе</small></label>
                                        <input type="number" name="minimum" placeholder="1" class="form-control" value="{{ old('minimum') | '1' }}">
                                        @if ($errors->has('minimum'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('minimum') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-sm-6 col-xs-12 right-col-products">
                                    <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
                                        <label>SKU (артикул)</label>
                                        <input type="text" name="sku" placeholder="00012d" class="form-control" value="{{ old('sku') }}" required>
                                        @if ($errors->has('sku'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('sku') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                        <label>Статус</label>
                                        <select class="form-control chosen-select" name="status" data-placeholder="Выбрать статус">
                                            <option value="0" {{(old('status') == 0 ? 'selected':'')}}>Отключено</option>
                                            <option value="1" {{(old('status') == 1 ? 'selected':'')}}>Включено</option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('status') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label>Количество</label>
                                        <input type="number" name="quantity" placeholder="3" class="form-control" value="{{ old('quantity') }}">
                                        @if ($errors->has('quantity'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('quantity') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('stock_status_id') ? ' has-error' : '' }}">
                                        <label>Остутствие на складе <small>Статус, показываемый, когда товара нет на складе</small></label>
                                        <select class="form-control chosen-select" name="stock_status_id" data-placeholder="Выбрать статус">
                                            <option value="0">Выбрать статус</option>
                                            @foreach($stock_status as $status)

                                                <option value="{{$status->id}}" {{(old('stock_status_id') == $status->id ? 'selected':'')}}>{{$status->name}}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('stock_status_id'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('stock_status_id') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        <label>Показывать в категориях</label>
                                        <select multiple="multiple" class="form-control chosen-select tag-input-style" name="category_id[]" data-placeholder="Выбрать категории">
                                            <option value="0">Выбрать категории</option>
                                            @foreach($categories as $cats)

                                                <option value="{{$cats->id}}" {{(old('category_id') == $cats->id ? 'selected':'')}}>{{$cats->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('category_id') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('date_available') ? ' has-error' : '' }}">
                                        <label>Дата поступления</label>
                                        <div class="input-group">
                                            <input type="text" name="date_available" placeholder="11.02.2017" class="form-control date-picker" value="{{ old('date_available') }}">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                        @if ($errors->has('date_available'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('date_available') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    Спец. товар
                                    <label class="pos-rel">
                                        <input type="checkbox" class="ace" name="special">
                                        <span class="lbl"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center">
                                    <a href="javascript:void(0)" onclick="addAttr()">Добавить аттрибут <i class="fa fa-plus"></i></a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="wrapp_attrs">

                                </div>
                            </div>

                            <h3>Seo данные: </h3>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('title_seo') ? ' has-error' : '' }}">
                                        <label>Название (title)</label>
                                        <input type="text" name="title_seo" id="title" placeholder="Подвеска" class="form-control" value="{{ old('title_seo') }}" required>
                                        @if ($errors->has('title_seo'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('title_seo') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('keywords_seo') ? ' has-error' : '' }}">
                                        <label>Ключевые слова (keywords)</label>
                                        <div>
                                            <input type="text" name="keywords_seo" id="keywords_seo" class="form-control" value="{{ old('keywords_seo') }}">
                                        </div>
                                        @if ($errors->has('keywords_seo'))
                                            <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('keywords_seo') }}</small>
                                        </strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description_seo') ? ' has-error' : '' }}">
                                <label>Описание (description)</label>
                                <textarea name="description_seo" class="form-control">{{ old('description_seo') }}</textarea>
                                @if ($errors->has('description_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('description_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="editor-wrapper form-group{{ $errors->has('full_description') ? ' has-error' : '' }}">
                                <label>Описание для страницы</label>
                                <div class="wysiwyg-editor" id="full_description">{{ old('full_description') }}</div>
                                <textarea name="full_description" class="form-control">{{ old('full_description') }}</textarea>
                                @if ($errors->has('full_description'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('full_description') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/_mod/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('ace/components/jquery.hotkeys/index.min.js')}}"></script>
    <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.wysiwyg.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('ace/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('ace/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js')}}"></script>
    <script src="{{asset('ace/components/moment/moment.js')}}"></script>

    <script src="{{asset('js/admin/products.js')}}"></script>
@endsection
