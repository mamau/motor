<table class="table table-striped">
    <tr>
        <th>Название</th>
        <th class="text-left">Значение</th>
    </tr>
    @forelse($details as $key => $detail)
        <tr>
            <td class="text-left">
                @if($key == 'maxCount')
                    Максимум
                @elseif($key == 'minCount')
                    Минимум
                @elseif($key == 'Code' || $key == 'PIN' || $key == 'code')
                    Номер артикула
                @elseif($key == 'nomer_po_katalogu' || $key == 'catalog_num')
                    Номер по каталогу
                @elseif($key == 'Manuf' || $key == 'BRAND' || $key == 'proizvoditel' || $key == 'marka' || $key == 'brand')
                    Производитель
                @elseif($key == 'Name' || $key == 'NAME' || $key == 'naimenovanie' || $key == 'name' || $key == 'description')
                    Название
                @elseif($key == 'Price' || $key == 'PRICE' || $key == 'tsena_po_dogovoru' || $key == 'price')
                    Цена
                @elseif($key == 'ARTID')
                    Уникальный идентификационный номер
                @elseif($key == 'RVALUE')
                    Доступное количество
                @elseif($key == 'RDPRF' || $key == 'kratnost')
                    Кратность
                @elseif($key == 'VENSL')
                    Вероятность поставки
                @elseif($key == 'Storage')
                    Склад
                @elseif($key == 'Delivery')
                    Доставка
                @elseif($key == 'Count' || $key == 'COUNT' || $key == 'kolichestvo' || $key == 'count')
                    Количество
                @elseif($key == 'client_type')
                    Диллер
                @elseif($key == 'width')
                    Ширина
                @elseif($key == 'height')
                    Высота
                @elseif($key == 'diameter')
                    Диаметр в дюймах
                @elseif($key == 'index_speed')
                    Индекс скорости
                @elseif($key == 'index_load')
                    Индекс нагрузки
                @elseif($key == 'season')
                    Сезон
                @elseif($key == 'diameter_hole')
                    Число/диаметр расположения крепежных отверстий
                @elseif($key == 'outreach')
                    Вылет диска в мм.(ЕТ)
                @elseif($key == 'diameter_central')
                    Диаметр центрального отверстия
                @elseif($key == 'color1')
                    Цвет диска 1
                @elseif($key == 'color2')
                    Цвет диска 2
                @elseif($key == 'type')
                    Тип
                @elseif($key == 'model')
                    Модель
                @elseif($key == 'distributorId')
                    Идентификатор поставщика
                @elseif($key == 'grp')
                    Группа поставщика
                @elseif($key == 'number')
                    Код детали
                @elseif($key == 'numberFix')
                    "Очищенный" код детали

                @else
                    {{$key}}
                @endif
            </td>
            <td class="text-left">{{$detail}}</td>
        </tr>
    @empty
        <tr>
            <td colspan="2">Нет информации</td>
        </tr>
    @endforelse
</table>
