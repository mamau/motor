<div class="widget-box">
    <div class="widget-header">
        <h4 class="widget-title">Фильтр</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <form action="{{route('admin.products.filter')}}" method="get">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div class="form-group">
                            <label>ID</label>
                            <input class="form-control" name="id" value="{{ app('request')->input('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="form-group">
                            <label>ID 1C</label>
                            <input class="form-control" name="id1c" value="{{ app('request')->input('id1c') }}">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" name="name" value="{{ app('request')->input('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label>Категория</label>
                            <select class="form-control chosen-select tag-input-style" name="category" data-placeholder="Выбрать категорию">
                                <option value="0">Все</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                    @if (app('request')->input('category') == $category->id)
                                        selected
                                    @endif
                                    >{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 text-right">
                        <a href="{{ route('products.index') }}" class="btn btn-danger btn-sm">Сбросить</a>
                        <button class="btn btn-primary btn-sm" type="submit">Применить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
