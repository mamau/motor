@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.products_list.title') }}
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('ace/components/chosen/chosen.css') }}">
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'products_list'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.products_list.title') }}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div class="pull-right tableTools-container">
                                    <button class="btn btn-xs btn-danger tooltip-info" data-rel="tooltip"
                                            data-placement="top" data-original-title="Удалить выбранные"
                                            onclick="deleteProducts(false);">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @include("admin.pages.products._parts._filter", ['categories' => $categories])
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thin-border-bottom">
                            <tr>
                                <th class="center sorting_disabled">
                                    <label class="pos-rel">
                                        <input type="checkbox" class="ace">
                                        <span class="lbl"></span>
                                    </label>
                                </th>
                                <th>
                                    id 1c
                                </th>
                                <th>
                                    Название
                                </th>
                                <th>
                                    Категория
                                </th>
                                <th>
                                    Модель
                                </th>
                                <th>
                                    Цена
                                </th>
                                <th class="text-center">
                                    <i class="ace-icon fa fa-cogs"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td class="center sorting_disabled">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace">
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
                                    <td>{{$product->id_1c}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->categories->first() ? $product->categories->first()->name : ''}}</td>
                                    <td>{{$product->model_product}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>
                                        <a href="{{ route('products.edit', ['id' => $product->id]) }}" class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(!isset($products->links))
                            {{ $products->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/datatables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('ace/components/_mod/datatables/jquery.dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('ace/components/datatables.net-select/js/dataTables.select.js')}}"></script>
    <script src="{{asset('js/admin/products.js')}}"></script>
@endsection
