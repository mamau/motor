@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.create_manufacturer.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'create_manufacturer'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.create_manufacturer.title') }}
                                    </h1>
                                </div>

                            </div>
                        </div>

                        <form action="{{route('manufacturer.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Производитель</label>
                                <input type="text" name="name" placeholder="Китай" class="form-control" value="{{ old('name') }}" autofocus required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="img-upload-wrapper form-group{{ $errors->has('thumb') ? ' has-error' : '' }}">
                                <label>Изображение</label>
                                <img src="{{asset('img/noimage.png')}}" class="img-responsive" id="toggleFile">
                                <input type="file" name="thumb" id="thumb" class="form-control" value="{{ old('thumb') }}" disabled>
                                <input type="hidden" name="type" value="manufacturer">
                                @if ($errors->has('thumb'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('thumb') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label>Порядок сортировки</label>
                                <input type="number" name="sort" placeholder="1" class="form-control" value="{{ old('sort') }}">
                                @if ($errors->has('sort'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('sort') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <h3>Seo данные: </h3>
                            <div class="form-group{{ $errors->has('title_seo') ? ' has-error' : '' }}">
                                <label>Название (title)</label>
                                <input type="text" name="title_seo" placeholder="Китай" class="form-control" value="{{ old('title_seo') }}" required>
                                @if ($errors->has('title_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('title_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('keywords_seo') ? ' has-error' : '' }}">
                                <label>Ключевые слова (keywords)</label>
                                <div>
                                    <input type="text" name="keywords_seo" id="keywords_seo" class="form-control" value="{{ old('keywords_seo') }}">
                                </div>
                                @if ($errors->has('keywords_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('keywords_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description_seo') ? ' has-error' : '' }}">
                                <label>Описание (description)</label>
                                <textarea name="description_seo" class="form-control">{{ old('description_seo') }}</textarea>
                                @if ($errors->has('description_seo'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('description_seo') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="editor-wrapper form-group{{ $errors->has('full_description') ? ' has-error' : '' }}">
                                <label>Описание для страницы</label>
                                <div class="wysiwyg-editor" id="full_description">{{ old('full_description') }}</div>
                                <textarea name="full_description" class="form-control">{{ old('full_description') }}</textarea>
                                @if ($errors->has('full_description'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('full_description') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('ace/components/_mod/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('ace/components/jquery.hotkeys/index.min.js')}}"></script>

    <script src="{{asset('ace/assets/js/src/elements.wysiwyg.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.colorpicker.js')}}"></script>
    <script src="{{asset('ace/components/_mod/bootstrap-tag/bootstrap-tag.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.typeahead.js')}}"></script>
    <script src="{{asset('js/admin/manufacturer.js')}}"></script>
@endsection
