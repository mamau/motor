@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.create_banner.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'create_banner'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.create_banner.title') }}
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <form action="{{route('banners.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="type_model" value="banner">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Название</label>
                                        <input type="text" name="name" id="name" placeholder="Лампочка" class="form-control" value="{{ old('name') }}" autofocus required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('name') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                        <label>Тип изображения</label>
                                        <select class="form-control" name="type" required>
                                            @foreach($banners_type as $key => $type)
                                                <option value="{{$key}}" {{(old('type') == $key)?'checked':''}}>{{$type}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('type') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('banner') ? ' has-error' : '' }}">
                                        <label>Изображение</label>
                                        <input type="file" id="banner" name="banner">
                                        @if ($errors->has('banner'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('banner') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Описание</label>
                                        <textarea class="form-control" rows="5" name="description">{{old('description')}}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('description') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                        <label>Ссылка</label>
                                        <input type="text" name="url" class="form-control" value="{{old('url')}}">
                                        @if ($errors->has('url'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('url') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('js/admin/banners.js')}}"></script>
@endsection
