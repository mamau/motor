@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.news_list.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'news_list'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.news_list.title') }}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif

                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div class="pull-right tableTools-container">
                                    <button class="btn btn-xs btn-danger tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Удалить выбранные" onclick="deleteNews()">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table id="newsTable" class="table table-striped table-bordered table-hover">
                            <thead class="thin-border-bottom">
                            <tr>
                                <th class="center sorting_disabled">
                                    <label class="pos-rel">
                                        <input type="checkbox" class="ace">
                                        <span class="lbl"></span>
                                    </label>
                                </th>
                                <th>
                                    id
                                </th>
                                <th>
                                    Название
                                </th>
                                <th class="text-center">
                                    <i class="ace-icon fa fa-cogs"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/datatables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('ace/components/_mod/datatables/jquery.dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('ace/components/datatables.net-select/js/dataTables.select.js')}}"></script>
    <script src="{{asset('js/admin/news.js')}}"></script>
@endsection
