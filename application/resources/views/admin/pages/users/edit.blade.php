@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.users.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'users'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        Обновить пользователя
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <form action="{{route('users.update',['id' => $user->id])}}" method="post">
                            {{csrf_field()}}
                            {{method_field('put')}}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Имя пользователя</label>
                                <input type="text" name="name" placeholder="Василий" class="form-control" value="{{ $user->name }}" autofocus required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>E-mail</label>
                                <input type="email" name="email" placeholder="mail@mail.ru" class="form-control" value="{{ $user->email }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('email') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label>Телефон</label>
                                <input type="text" name="phone" placeholder="+7 (999) 999-99-99" class="form-control input-mask-phone" value="{{$user->phone }}">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('phone') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                <label>Роль</label>
                                <select name="role" class="form-control">

                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" {{ ($userRole->id == $role->id ? "selected":"") }}>{{$role->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('role') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Пароль</label>
                                <input type="password" class="form-control" placeholder="Пароль" name="password" disabled>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('password') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('confirm') ? ' has-error' : '' }}">
                                <label>Повторите пароль</label>
                                <input type="password" class="form-control" placeholder="Повторите пароль" name="confirm" disabled>
                                @if ($errors->has('confirm'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('confirm') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Обновить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/jquery.maskedinput/dist/jquery.maskedinput.js')}}"></script>
    <script src="{{asset('js/admin/users.js')}}"></script>
@endsection
