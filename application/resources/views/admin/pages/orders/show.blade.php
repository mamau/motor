@extends('admin.layouts.app')
@section('title')
    Заказ #{{$order->id}}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'orders'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h1>
                                Заказ от {{$order->user->name}} на сумму {{$order->sum}} <i class="fa fa-rub"></i>
                            </h1>
                        </div>
                        <div class="widget-box transparent">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-cubes orange"></i>
                                    Заказанные товары от {{$order->created_at->format('d.m.Y H:i')}}
                                </h4>

                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="order-row">
                                                <th>#</th>
                                                <th>Название</th>
                                                <th>Цена</th>
                                                <th>Количество</th>
                                                <th>Диллер</th>
                                                <th>Сменить статус</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($order->foreignGoods as $good)
                                            <tr>
                                                <td>{{$good->product_id}}</td>
                                                <td>
                                                    <a href="javascript:void(0)" onclick="showDetailProduct({{$good->id}})">{{$good->name}}</a>
                                                </td>
                                                <td>{{$good->price}} <i class="fa fa-rub"></i></td>
                                                <td>{{$good->count}}</td>
                                                <td>{{$good->client_type}}</td>
                                                <td>
                                                    <select class="form-control" id="statuses" name="statuses" onchange="changeStatusOrder({{$good->foreign_order_id}})">
                                                        @foreach($statuses as $status)
                                                            @if($status->id == $order->status)
                                                                <option value="{{$status->id}}" selected>{{$status->name}}</option>
                                                            @else
                                                                <option value="{{$status->id}}">{{$status->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6">Нет товаров</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                        @if(!empty($order->foreignGoods))
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" class="sum-total">Итого:</td>
                                                <td>{{$order->sum}} <i class="fa fa-rub"></i></td>
                                            </tr>
                                        </tfoot>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="widget-box transparent">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-rss orange"></i>
                                    Информация о пользователе
                                </h4>

                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <h4 class="blue">Имя</h4>
                                            <div>{{$order->user->name}}</div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <h4 class="blue">Фамилия</h4>
                                            <div>{{$order->user->userParams->surname}}</div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <h4 class="blue">Дата рождения</h4>
                                            <div>{{$order->user->userParams->birthday->format('d.m.Y')}}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <h4 class="blue">Город</h4>
                                            <div>{{Helpers::getCity($order->user->userParams->city_id)}}</div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <h4 class="blue">Телефон</h4>
                                            <div>{{$order->user->phone}}</div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <h4 class="blue">Почта</h4>
                                            <div>{{$order->user->email}}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="blue">Адрес</h4>
                                            <div>{{$order->user->userParams->address}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('js/admin/orders.js')}}"></script>
@endsection
