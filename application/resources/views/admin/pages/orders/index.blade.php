@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.orders.title') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'orders'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h1>
                                {{ trans('interface.panel.orders.title') }}
                            </h1>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Имя клиента</td>
                                    <td>Итого</td>
                                    <td>Дата</td>
                                    <td>Статус</td>
                                    <td><i class="fa fa-cogs"></i></td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($orders as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->user->name}}</td>
                                        <td>{{$order->sum}}</td>
                                        <td>{{$order->created_at->format('d.m.Y H:i')}}</td>
                                        <td>
                                            @if(!is_null($order->statuz))
                                                <span class="label label-success arrowed-in arrowed-in-right">
                                                    {{$order->statuz->name}}
                                                </span>
                                            @else
                                                @if($order->status)
                                                    <span class="label label-success arrowed-in arrowed-in-right">
                                                        Оплачено
                                                    </span>
                                                @else
                                                <span class="label label-danger arrowed-in arrowed-in-right">Не оплачено</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('orders.show',['id' => $order->id])}}" class="btn btn-primary btn-xs tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Детали заказа"><i class="fa fa-search"></i></a>
                                            <a href="{{route('orders.delete',['id' => $order->id])}}" class="btn btn-danger btn-xs tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Удалить заказ"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                <tr>
                                    <td colspan="6">Нет заказов</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{$orders->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection
