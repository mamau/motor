@extends('admin.layouts.app')
@section('title')
    {{ trans('interface.panel.markup.index') }}
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container">
        @include('admin.layouts._sidebar.sidebar', ['active' => 'markup'])
        <div class="content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 text-left">
                                    <h1>
                                        {{ trans('interface.panel.markup.index') }}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                        @endif
                        <h3>Все значения устанавливаются в процентах <i class="fa fa-percent"></i></h3>
                        <div class="wrapper-markup">
                            <form action="{{route('markup.store')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('avtoto') ? ' has-error' : '' }}">
                                            <label>Avtoto</label>
                                            <input type="number" class="form-control" name="avtoto" value="{{isset($data['avtoto'])?$data['avtoto']:0}}" placeholder="3">
                                            @if ($errors->has('avtoto'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('avtoto') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('armtek') ? ' has-error' : '' }}">
                                            <label>Armtek</label>
                                            <input type="number" class="form-control" name="armtek" value="{{isset($data['armtek'])?$data['armtek']:0}}" placeholder="4">
                                            @if ($errors->has('armtek'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('armtek') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('forumAuto') ? ' has-error' : '' }}">
                                            <label>Forum Auto</label>
                                            <input type="number" class="form-control" name="forumAuto" value="{{isset($data['forumAuto'])?$data['forumAuto']:0}}" placeholder="14">
                                            @if ($errors->has('forumAuto'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('forumAuto') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('favoritAuto') ? ' has-error' : '' }}">
                                            <label>Favorit Auto</label>
                                            <input type="number" class="form-control" name="favoritAuto" value="{{(isset($data['favoritAuto']))?$data['favoritAuto']:0}}" placeholder="27">
                                            @if ($errors->has('favoritAuto'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('favoritAuto') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('tiresWheels') ? ' has-error' : '' }}">
                                            <label>Шины и диски</label>
                                            <input type="number" class="form-control" name="tiresWheels" value="{{(isset($data['tiresWheels']))?$data['tiresWheels']:0}}" placeholder="27">
                                            @if ($errors->has('tiresWheels'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('tiresWheels') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('stparts') ? ' has-error' : '' }}">
                                            <label>St Parts</label>
                                            <input type="number" class="form-control" name="stparts" value="{{(isset($data['stparts']))?$data['stparts']:0}}" placeholder="27">
                                            @if ($errors->has('stparts'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('stparts') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group{{ $errors->has('trinity') ? ' has-error' : '' }}">
                                            <label>Trinity</label>
                                            <input type="number" class="form-control" name="trinity" value="{{(isset($data['trinity']))?$data['trinity']:0}}" placeholder="27">
                                            @if ($errors->has('trinity'))
                                                <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('trinity') }}</small>
                                                </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary">Установить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("admin.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('js/admin/price/index.js')}}"></script>
@endsection
