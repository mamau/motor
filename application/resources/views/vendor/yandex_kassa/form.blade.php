{{--
  -- For more information about form fields
  -- you can visit Yandex Kassa documentation page
  --
  -- @see https://tech.yandex.com/money/doc/payment-solution/payment-form/payment-form-http-docpage/
  --}}
<form action="{{yandex_kassa_form_action()}}" method="{{yandex_kassa_form_method()}}" class="form-horizontal" id="ya_form">
    <input name="scId" type="hidden" value="{{yandex_kassa_sc_id()}}">
    <input name="shopId" type="hidden" value="{{yandex_kassa_shop_id()}}">
    <input name="sum" id="yandex_money_sum" type="hidden" class="form-control">
    <input name="customerNumber" id="yandex_money_customer_number" type="hidden" class="form-control">
    <input name="orderNumber" type="hidden" id="orderNumber">
    <input name="paymentType" type="hidden" value="">
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10 text-right">
            <button type="submit" class="btn btn-primary btn-sm" id="sendForm">{{trans('yandex_kassa::form.button.pay')}}</button>
        </div>
    </div>

</form>
