<div class="news-block">
    @if ($type === 'col')
        <span class="hidden-xs">
            <h4>Новости</h4>
            @forelse($news as $new)
                <div class="row">
                    <div class="col-sm-12">
                        <a href="{{route('news.show', ['id' => $new->id])}}" class="title-news">
                            <img src="{{Helpers::thumbSm($new)}}" class="img-responsive" alt="{{$new->title}}"
                                 title="{{$new->title}}">
                            {{$new->title}}
                        </a>
                    </div>
                </div>
            @empty
                <p>Нет новостей</p>
            @endforelse
        </span>
    @else
        <h3>Наши новости</h3>
        <div class="row">
            @forelse($news as $new)
                <div class="col-sm-4">
                    <a href="{{route('news.show', ['id' => $new->id])}}" class="title-news">
                        <img src="{{Helpers::thumbSm($new)}}" class="img-responsive" alt="{{$new->title}}"
                             title="{{$new->title}}">
                        {{$new->title}}
                    </a>
                    <p>
                        {!! $new->description !!}
                    </p>
                    <div class="time-add">
                        <i class="fa fa-clock-o"></i> <span>{{$new->updated_at->format('d.m H:i')}}</span>
                    </div>
                </div>
                @if (($loop->index+1) % 3 === 0)
                    </div><div class="row">
                @endif
            @if ($loop->last)
                </div>
            @endif
            @empty
                <p>Нет новостей</p>
            @endforelse

    @endif
</div>
