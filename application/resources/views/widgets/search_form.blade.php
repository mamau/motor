<div class="search-form">
    <div class="tabbable">
        <ul class="nav nav-tabs" id="myTab">
            <li class="{{($active == 'search1')?'active':''}}" style="display: none">
                <a data-toggle="tab" href="#search1" aria-expanded="true">
                    <i class="green ace-icon fa fa-search bigger-120"></i>
                    Поиск по номеру
                </a>
            </li>

            <li class="{{($active == 'search2')?'active':''}}">
                <a data-toggle="tab" href="#search2" aria-expanded="false">
                    Поиск по названию
                </a>
            </li>
            <li class="{{($active == 'search3')?'active':''}}">
                <a data-toggle="tab" href="#search3" aria-expanded="false">
                    <i class="green ace-icon fa fa-search bigger-120"></i>
                    Поиск
                </a>
            </li>
            <li class="{{($active == 'search4')?'active':''}}" style="display: none">
                <a data-toggle="tab" href="#search4" aria-expanded="false">
                    <i class="green ace-icon fa fa-search bigger-120"></i>
                    Поиск по бренду
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="search1" class="tab-pane fade {{($active == 'search1')?'active in':''}}">
                <form method="post" action="{{route('front.search.sku')}}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="needle" placeholder="Введите код запчасти" value="{{$needle}}">
                    </div>
                    <div class="form-group text-right">
                        {{csrf_field()}}
                        <div class="wrapp-btn">
                            <button class="btn btn-danger" type="submit">
                                Найти
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div id="search2" class="tab-pane fade {{($active == 'search2')?'active in':''}}">
                <form method="post" action="{{route('front.search.any')}}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="needle" placeholder="Введите ключевое слово" value="{{$needle}}">
                    </div>
                    <div class="form-group text-right">
                        {{csrf_field()}}
                        <div class="wrapp-btn">
                            <button class="btn btn-danger" type="submit">
                                Найти
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div id="search3" class="tab-pane fade {{($active == 'search3')?'active in':''}}">
                <form method="post" action="{{route('front.search.avtotek')}}">
                    {{csrf_field()}}
                    <div class="form-group" style="width: 90%">
                    <input type="text" class="form-control" name="pin" placeholder="Введите код запчасти" value="{{$needle}}">
                    </div>
                    <div class="form-group text-right">
                        <div class="wrapp-btn">
                            <button class="btn btn-danger" type="submit">
                                Найти
                            </button>
                        </div>
                    </div>

                    <div class="search-analog" style="display: none">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input name="analog" type="checkbox" class="ace"
                                        @if(isset($analog))
                                            @if($analog == 1)
                                                checked
                                            @endif
                                        @endif
                                        >
                                        <span class="lbl"> Искать аналоги</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="search4" class="tab-pane fade {{($active == 'search4')?'active in':''}}">
                <form method="post" action="{{route('armtek.search')}}">
                    {{csrf_field()}}
                    <div class="form-group" style="width: 90%">
                        <input type="text" class="form-control" name="pin" placeholder="Введите код запчасти" value="{{$needle}}" id="searchBrand" autocomplete="off">
                        <div class="wrapper-needle-search">
                            <table class="table table-hover table-bordered needle-search">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
