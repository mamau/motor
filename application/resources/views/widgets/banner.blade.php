<div class="banner-wrapper">
    @foreach($banners as $banner)
        @if(empty($banner->url))
            <img src="{{Helpers::thumbSm($banner, true)}}" class="img-responsive">
        @else
            <a href="{{$banner->url}}" target="_blank">
                <img src="{{Helpers::thumbSm($banner, true)}}" class="img-responsive">
            </a>
        @endif
    @endforeach
</div>
