<div class="row">
    <div class="col-xs-12">
        <h2>Специальное предложение</h2>
        <h3>Зимние шины по привлекательной цене, <span class="free-montage">шиномонтаж бесплатно</span></h3>
    </div>
    @foreach($tires as $tire)
        <div class="col-xs-3 text-center">
            <div class="wrapp-speci">
                <a href="{{route('front.products.show',['id' => $tire->id])}}">
                    <div class="wrapp-thumb-spec">
                        <img src="{{Helpers::thumbSm($tire)}}" title="{{$tire->name}}" alt="{{$tire->name}}">
                    </div>
                    <div class="name">{{ $tire->name }}</div>
                    <div class="price">{{ $tire->price }} р.</div>
                </a>
            </div>
        </div>
    @endforeach
</div>
