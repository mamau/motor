<ul class="nav nav-top  navbar-left">
        <li>
            <a href="/">
                <span>Главная</span>
            </a>
        </li>
    @foreach($pages as $page)
        <li>
            @if(!empty($page->alias))
                <a href="{{route('front.page.alias', ['alias' => $page->alias])}}">
                    <span>{{$page->name}}</span>
                </a>
            @endif
        </li>
    @endforeach
</ul>
