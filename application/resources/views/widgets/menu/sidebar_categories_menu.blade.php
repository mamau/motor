<ul class="sidebar__categories">
    @foreach ($categories as $category)
        <li class="sidebar__item @if(count($category->childrenCategory)) has-childs @endif">
            <a href="{{route('front.categories.show',['categories' => $category->id])}}">{{$category->name}}</a>
            @include('front.pages.categories._parts.category')
        </li>
    @endforeach
</ul>
