<ul>
    <li class="active hvr-sweep-to-right">
        <a href="/">Главная</a>
    </li>
    @foreach($pages as $page)
        <li class="{{(count($page->childrenPage) > 0)?'tree-nav':'hvr-sweep-to-right'}}">
            @if(is_null($page->url) || empty($page->url))
                <a href="{{(count($page->childrenPage) > 0)?'#':route('front.page.show',['id' => $page->id])}}">
                    {{$page->name}}
                </a>
                @include('front.pages.page._parts.child_left')

            @else
                <a href="{{$page->url}}" target="_blank">
                    {{$page->name}}
                </a>
                @include('front.pages.page._parts.child_left')
            @endif
        </li>
    @endforeach
    <li class="hvr-sweep-to-right">
        <a href="{{route('news.index')}}">Новости</a>
    </li>
</ul>
