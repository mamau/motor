<div class="row">
    <div class="col-xs-6 col-sm-12">
        <div class="cabinet-wrapper">
        <div class="wrapp-cabinet-title">
            <div class="title-cabine">
                <a href="{{route('cabinet.index')}}">
                    <i class="fa fa-user"></i>
                    <strong>Личный кабинет</strong>
                </a>
            </div>
        </div>
        @if(!Auth::check())
            <form action="{{route('front.login')}}" method="post" class="front-login-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="email" placeholder="E-mail" name="email" class="form-control" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>
                                <small>{{ $errors->first('email') }}</small>
                            </strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Пароль" class="form-control" required name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>
                                <small>{{ $errors->first('password') }}</small>
                            </strong>
                        </span>
                    @endif
                </div>
                <div class="form-group text-right">
                    <div class="wrapp-btn">
                        <button class="btn btn-danger btn-sm" type="submit">
                            Вход
                        </button>
                    </div>
                </div>
                <div class="row-fogot-pass pass-tbl">
                    <div class="pass-td text-left">
                        <a href="{{route('front.forgot.index')}}">Забыли пароль?</a>
                    </div>
                    <div class="pass-td  text-right">
                        <a href="{{route('front.register.index')}}">Регистрация</a>
                    </div>
                </div>
            </form>
        @else
            <div class="wrapper-auth-user">
                <p>
                    Вы вошли как {{Auth::user()->name}}
                </p>
                <div class="text-left">
                    <a href="{{ route('front.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выйти
                    </a>
                    <form id="logout-form" action="{{ route('front.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        @endif
    </div>
    </div>
</div>
@if(!Auth::check())
<div class="wrapper-social">
    <small>Войти через:</small>
    <hr>
    <div class="social-login">
        <a href="{{ url('/social_login/vkontakte') }}">
            <i class="fa fa-vk"></i>
        </a>
    </div>
    <hr>
</div>
@endif

{{--@widget('newsBlock', ['type' => 'col'])--}}
@widget('Categories', ['type' => 'sidebar'])
