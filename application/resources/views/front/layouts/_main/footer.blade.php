<div class="container footer-wrapper">
    <div class="footer">
        <div class="row">
            <div class="col-xs-3 footer-spec foot-block">
                <div>Мы работаем</div>
                <div class="wrapp-time">
                    @widget('welcomeBlock',['field' => 'work_time','explode' => true])
                </div>
            </div>
            <div class="col-xs-3 foot-block">
                <div class="wrapper-contacts">
                    <div>Наш телефон: @widget('welcomeBlock',['field' => 'phone','explode' => true])</div>
                    <div>Наша почта: @widget('welcomeBlock',['field' => 'email'])</div>
                </div>
            </div>
            <div class="col-xs-3 foot-block">
                <img src="{{asset('img/payments/visa.png')}}" alt="visa">
                <img src="{{asset('img/payments/mastercard.png')}}" alt="mastercard">
                <img src="{{asset('img/payments/yandexmoney.png')}}" alt="yandexmoney">
            </div>
            <div class="col-xs-3 foot-block">
                <div class="quest-tbl">
                    <div class="quest-td td-quest-i">
                        <i class="fa fa-comments-o"></i>
                    </div>
                    <div class="quest-td">
                        <div>Есть вопросы?</div>
                        <a href="javascript:void(0)" onclick="feedback()">Отправьте нам сообщение!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    @parent
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('ace/components/bootbox.js/bootbox.js')}}"></script>
    <script src="{{asset('ace/components/jquery.maskedinput/dist/jquery.maskedinput.js')}}"></script>
@endsection
