@section('styles')
    <link rel="stylesheet" href="{{asset('ace/components/jquery.gritter/css/jquery.gritter.css')}}">
    @parent
@endsection
<div class="container">
    <nav class="navbar navbar-default navbar-static-top">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars"></span>
        </a>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                @widget('Menu',['type' => 'top'])
            <div class="cart hvr-shutter-out-vertical" id="cart">
                <div class="cart-tbl">
                    <div class="cart-td">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="cart-description cart-td">
                        @if(Auth::check())
                            <a href="javascript:void(0)">
                                товаров: <span id="products_count">{{Cart::count()}}</span> шт., на сумму: <span id="products_amount">{{Cart::subtotal()}}</span> руб.
                            </a>
                        @else
                            <div class="text-center">
                                Войдите на сайт
                            </div>
                        @endif
                        <div class="cart_product_list">
                            <table class="table table-striped">

                                @forelse(Cart::content() as $product)
                                    <tr class="{{$product->rowId}}">
                                        <td>
                                            {{$product->name}}
                                        </td>
                                        <td class="quant_good">
                                            <span>
                                                {{$product->qty}}
                                            </span>
                                            <small>шт.</small>
                                        </td>
                                        <td class="price_good">
                                            <span>
                                                {{$product->qty * $product->price}}
                                            </span>
                                            <i class="fa fa-rub"></i>
                                        </td>
                                        <td>
                                            <i onclick="destroyProduct('{{$product->rowId}}')" class="fa fa-close"></i>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center">
                                            Корзина пуста
                                        </td>
                                    </tr>
                                @endforelse
                                @if(count(Cart::content()) > 0)
                                    <tr>
                                        <td colspan="1" class="text-left">
                                            <a href="{{route('front.cart.index')}}">Оформить заказ</a>
                                        </td>
                                        <td colspan="3" class="text-right">
                                            <strong>Итого: </strong> <span class="total_cart">{{Cart::subtotal()}}</span>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-sm-3 text-center header-row">
            <div class="phones phones-tbl">
                <div class="phones-tr">
                    <div class="phones-icon phones-td">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="phone-list phones-td">
                        @widget('welcomeBlock',['field' => 'phone','explode' => true])
                    </div>
                </div>
                <div class="phones-tr text-left order-call-link">
                    <a href="javascript:void(0)" onclick="orderCall()">Заказать звонок</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 text-center logo-wrapper header-row">
            <a href="/">
                <img src="{{asset('img/logo.svg')}}" class="img-logo" alt="мотор">
            </a>
        </div>
        <div class="col-sm-3 text-center header-row">
            <div class="clocks clocks-tbl">
                <div class="clocks-tr">
                    <div class="clocks-icon clocks-td">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="clocks-list clocks-td text-left">
                        @widget('welcomeBlock',['field' => 'work_time','explode' => true])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
