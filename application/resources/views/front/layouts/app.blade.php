<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    @section('keywords')
        <meta name="keywords" content="Автозапчасти, мотор 48 грязи, грязи, мотор, автомобильный магазин, запчасти грязи, мотор48, магазин, motor48, грязинский район, запчасти, мотор грязи, купить запчасти дешево, Грязи, Магазин автозапчастей и автотоваров, Автомобильные диски и шины, Шиномонтаж, заказать запчасти, заказать автозапчасти, заказ, купить, мотор48 грязи, motor 48 грязи, motor48 грязи">
    @show
    @section('description')
        <meta name="description" content="Магазин автозапчастей в городе Грязи. Большой ассортимент автомобильных запчастей как на отечествеенные авто, так и на иномарку.">
    @show
    <title>
        @section('title')
            {{ config('app.name', 'Motor') }}
        @show
    </title>
    @section('styles')
    <link rel="stylesheet" href="{{ asset('css/front/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/front/app_custom.min.css') }}">
    @show
</head>
<body class="no-skin">
    @yield('content')
    @section('scripts')
        <script type="text/javascript" src="{{ asset('js/front/app.min.js') }}"></script>
        <script src="{{asset('ace/assets/js/ace.js')}}"></script>
        <script src="{{asset('js/front/index.js')}}"></script>
        @if(app()->environment('production'))
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript" >
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter48336275 = new Ya.Metrika({
                                id:48336275,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/48336275" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
        @endif
    @show
</body>
</html>
