@extends('front.layouts.app')
@section('title')
    Новости
@stop
@section('keywords')
    <meta name="keywords" content="новости, автоновости, авто новости в грязях">
@stop
@section('description')
    <meta name="description" content="самые актуальные автомобильные новости">
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <div class="col-xs-12">
                    @widget('Categories')
                </div>
            </div>
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')

                    @forelse($news as $article)
                        <h2>
                            <a href="{{route('news.show', ['id' => $article->id])}}">{{$article->title}}</a>
                        </h2>
                        <div class="page-wrapper">
                            {!! $article->description !!}
                        </div>
                    @empty
                        <small>Нет новостей</small>
                    @endforelse
                    {{ $news->render() }}
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
