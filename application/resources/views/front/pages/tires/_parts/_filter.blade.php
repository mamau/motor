
<div class="widget-box">
    <div class="widget-header">
        <h4 class="widget-title">Фильтр</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <form action="{{route('front.tires.filter')}}" method="get">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('width') || $errors->has('height') ? ' has-error' : '' }}">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Ширина</label>
                                    <select class="form-control" name="width">
                                        <option value="0">Все</option>
                                        @foreach(config('settings.width') as $val)
                                            <option value="{{$val}}"
                                                @if(count($path) > 0)
                                                    @if($path['width'] == $val)
                                                        selected
                                                    @endif
                                                @endif
                                            >{{$val}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('width'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('width') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6">
                                    <label>Профиль</label>
                                    <select class="form-control" name="height">
                                        <option value="0">Все</option>
                                        @foreach(config('settings.height') as $val)
                                            <option value="{{$val}}"
                                                @if(count($path) > 0)
                                                    @if($path['height'] == $val)
                                                        selected
                                                    @endif
                                                @endif
                                            >{{$val}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('height'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('height') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('diameter') ? ' has-error' : '' }}">
                            <label>Диаметр</label>
                            <select class="form-control" name="diameter">
                                <option value="0">Все</option>
                                @foreach(config('settings.diameter') as $val)
                                    <option value="{{$val}}"
                                        @if(count($path) > 0)
                                            @if($path['diameter'] == $val)
                                                selected
                                            @endif
                                        @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('diameter'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('diameter') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('season') ? ' has-error' : '' }}">
                            <label>Сезон</label>
                            <select class="form-control" name="season">
                                <option value="0">Все</option>
                                @foreach(config('settings.season') as $val)
                                    <option value="{{$val}}"
                                    @if(count($path) > 0)
                                        @if($path['season'] == $val)
                                            selected
                                        @endif
                                    @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('season'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('season') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
                            <label>Производитель</label>
                            <select class="form-control" name="brand">
                                <option value="0">Все</option>
                                @foreach(config('settings.brand') as $val)
                                    <option value="{{$val}}"
                                        @if(count($path) > 0)
                                            @if($path['brand'] == $val)
                                                selected
                                            @endif
                                        @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('brand'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('brand') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12 text-right">
                        @if(count($path) > 0)
                            <a class="btn btn-danger btn-sm" href="{{route('front.tires.index')}}">Сбросить</a>
                        @else
                            <button class="btn btn-danger btn-sm" type="reset">Сбросить</button>
                        @endif

                        <button class="btn btn-primary btn-sm" type="submit">Применить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
