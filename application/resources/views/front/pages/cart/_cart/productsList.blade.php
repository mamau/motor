<table class="table table-striped">

    @forelse($cartContent as $product)
        <tr id="{{$product->rowId}}">
            <td>
                {{$product->name}}
            </td>
            <td>
                {{$product->qty}} <small>шт.</small>
            </td>
            <td>
                {{$product->qty * $product->price}} <i class="fa fa-rub"></i>
            </td>
            <td>
                <i onclick="destroyProduct('{{$product->rowId}}')" class="fa fa-close"></i>
            </td>
        </tr>
    @empty
        <tr>
            <td class="text-center">
                Корзина пуста
            </td>
        </tr>
    @endforelse
    @if(count($cartContent) > 0)
        <tr>
            <td colspan="1" class="text-left">
                <a href="{{route('front.cart.index')}}">Оформить заказ</a>
            </td>
            <td colspan="3" class="text-right">
                <strong>Итого: </strong> {{$cartSubtotal}}
            </td>
        </tr>
    @endif
</table>
