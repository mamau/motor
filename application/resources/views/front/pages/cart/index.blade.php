@extends('front.layouts.app')
@section('title')
    {{ trans('interface.page.cart.title') }}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')
                    <h1>{{trans('interface.page.cart.title')}}</h1>
                    <div class="cart-wrapper">
                       <table class="table table-striped table-bordered">
                           <thead>
                               <tr>
                                  <td>Название</td>
                                  <td>Количество</td>
                                  <td>Цена</td>
                                  <td>Итого</td>
                                  <td>Действие</td>
                               </tr>
                           </thead>
                            <tbody>
                           @forelse($cartContent as $product)
                               <tr class="{{$product->rowId}} product_row" data-rowid="{{$product->rowId}}" data-productid="{{$product->id}}">
                                   <td style="width: 60%">
                                        {{$product->name}}
                                   </td>
                                   <td>
                                       <input id="count-good" type="text" class="count" value="{{$product->qty}}"
                                      data-max="{{$product->options->maxcount}}"
                                      data-min="{{$product->options->mincount}}"
                                       >
                                   </td>
                                   <td>
                                       {{round($product->price,2,PHP_ROUND_HALF_UP)}} <i class="fa fa-rub"></i>
                                   </td>
                                   <td class="price_good">
                                       <span>{{round($product->qty * $product->price,2,PHP_ROUND_HALF_UP)}}</span>
                                       <i class="fa fa-rub"></i>
                                   </td>
                                   <td class="text-right" style="width: 10%">
                                        <button class="btn btn-danger btn-sm tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Удалить из корзины" onclick="destroyProduct('{{$product->rowId}}')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                   </td>
                               </tr>
                           @empty
                               <tr>
                                   <td class="text-center" colspan="4">
                                       Корзина пуста
                                   </td>
                               </tr>
                           @endforelse
                            </tbody>
                           <tfoot>
                           @if(count($cartContent) > 0)
                               <tr>
                                   <td colspan="5" class="text-right">
                                       <strong>Итого: </strong> <span id="cart_sum">{{$total}}</span> <i class="fa fa-rub"></i>
                                   </td>
                               </tr>
                           @endif
                           </tfoot>
                       </table>
                        <div class="checkbox">
                            <label>
                                <input name="agree" type="checkbox" class="ace" id="agree">
                                <span class="lbl" id="agreetext">
                                    Я соглашаюсь с условиями <a href="{{url('/page/10')}}" target="_blank">публичной оферты</a> и <a href="{{url('/page/11')}}" target="_blank">политикой конфиденциальности</a>
                                </span>
                            </label>
                        </div>
                        @include('yandex_kassa::form')
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script>
        var user_id = '{{Auth::user()->id}}';
        var params = '{!! json_encode(Auth::user()->userParams()->first()) !!}';
    </script>
    <script src="{{asset('ace/assets/js/ace-elements.js')}}"></script>
    <script src="{{asset('ace/components/fuelux/dist/js/fuelux.js')}}"></script>
    <script src="{{asset('js/front/cart/index.js')}}"></script>
@endsection
