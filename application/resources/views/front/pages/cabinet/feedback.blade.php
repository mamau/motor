@extends('front.layouts.app')
@section('title')
    Обратная связь
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Задать вопрос</h3>
                    {!! Breadcrumbs::render('feedback') !!}
                    @if (session('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                    @endif
                    <h4>Открытые вопросы:</h4>
                    @forelse($feedbacks as $feedback)
                        <p>
                            <a href="{{route('dialog.index',['id' => $feedback->id])}}">{{$feedback->title}}</a>
                        </p>
                    @empty
                        <p>У вас нет открытых вопросов</p>
                    @endforelse

                    <h4 class="new-question">Задать новый вопрос:</h4>
                    <form action="{{route('cabinet.feedback.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Тема вопроса</label>
                                    <input type="text" placeholder="Заказ запчасти" name="title" value="{{old('title')}}" class="form-control">
                                    <input type="hidden" name="author_id" value="{{$user->id}}">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('title') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Ваш вопрос</label>
                                    <textarea class="form-control" placeholder="Возможно ли заказать стойку стабилизатора для mazda 3?" name="text">{{old('text')}}</textarea>
                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('text') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="text-left">
                            <button class="btn btn-primary btn-sm" type="submit">Спросить</button>
                        </div>
                    </form>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>


@include("front.layouts._main.footer")
@endsection

@section('scripts')
    @parent
    <script src="{{asset('js/front/cabinet/feedback.js')}}"></script>
@endsection
