@extends('front.layouts.app')
@section('title')
    История заказов
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>История заказов</h3>
                    {!! Breadcrumbs::render('orders') !!}
                   <table class="table table-striped">
                       <thead>
                           <tr>
                               <td>#</td>
                               <td>Дата</td>
                               <td>Сумма</td>
                               <td>Статус</td>
                               <td class="text-center">
                                   <i class="fa fa-cogs"></i>
                               </td>
                           </tr>
                       </thead>
                       <tbody>
                        @forelse($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->created_at->format('m.d.Y H:i')}}</td>
                                <td>{{$order->sum}} <i class="fa fa-rub"></i></td>
                                <td>
                                    @if(!is_null($order->statuz))
                                        <span class="label label-success arrowed-in arrowed-in-right">
                                                    {{$order->statuz->name}}
                                                </span>
                                    @else
                                        @if($order->status)
                                            <span class="label label-success arrowed-in arrowed-in-right">Оплачено</span>
                                        @else
                                            <span class="label label-danger arrowed-in arrowed-in-right">Не оплачено</span>
                                        @endif
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{route('cabinet.orders.show',['id' => $order->id])}}" class="btn btn-primary btn-xs tooltip-info" data-rel="tooltip" data-placement="top" data-original-title="Посмотреть товары">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">У вас нет заказов</td>
                            </tr>
                        @endforelse
                       </tbody>
                   </table>

                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('js/front/cabinet/orders.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

