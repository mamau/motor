@extends('front.layouts.app')
@section('title')
    Изменить контактные данные
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Изменить пароль</h3>
                    {!! Breadcrumbs::render('profile') !!}
                    @if (session('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                    @endif
                    <form action="{{route('cabinet.password.update',['id' => $user->id])}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Пароль</label>
                                    <input type="password" placeholder="Пароль" name="password" value="{{old('password')}}" class="form-control">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Пароль ещё раз</label>
                                    <input type="password" placeholder="Пароль ещё раз" name="password_re" value="{{old('password_re')}}" class="form-control">
                                    @if ($errors->has('password_re'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password_re') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary btn-sm" type="submit">Обновить</button>
                        </div>
                    </form>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('js/front/cabinet/index.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

