@extends('front.layouts.app')
@section('title')
    Изменить контактные данные
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('ace/components/chosen/chosen.css') }}">
    @parent
    <link rel="stylesheet" href="{{asset('ace/components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('ace/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Изменить контактные данные</h3>
                    {!! Breadcrumbs::render('profile') !!}
                    @if (session('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                    @endif
                    <form action="{{route('cabinet.profile.update',['id' => $user->id])}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Имя</label>
                                    <input type="text" placeholder="Имя" name="name" value="{{old('name', $user->name)}}" class="form-control">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('name') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Фамилия</label>

                                    <input type="text" placeholder="Фамилия" name="params[surname]" value="{{ old('surname', $params->get('surname'))}}" class="form-control">

                                    @if ($errors->has('params.surname'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('params.surname') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="email" placeholder="yourmail" name="email" value="{{old('email', $user->email)}}" class="form-control">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('email') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Телефон</label>
                                    <input type="text" placeholder="88002148611" name="phone" value="{{old('phone', $user->phone)}}" class="form-control input-mask-phone">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('phone') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Город</label>
                                    <select class="form-control" id="filterCity" name="params[city]">
                                        @if(!is_null($city))
                                            <option value="{{$city->id}}">{{$city->city.' ('.$city->region.')'}}</option>
                                        @endif
                                    </select>
                                    @if ($errors->has('params.city'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('params.city') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Пол</label>
                                    <div class="radio">
                                        <label>
                                            <input name="params[gender]" type="radio" class="ace" value="male" {{($params->get('gender') == 'male' || $params->get('gender') == '') ? 'checked' : ''}}>
                                            <span class="lbl"> Мужской</span>
                                        </label>
                                        <label>
                                            <input name="params[gender]" type="radio" class="ace" value="female" {{($params->get('gender') == 'female') ? 'checked' : ''}}>
                                            <span class="lbl"> Женский</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('params.gender'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('params.gender') }}</small>
                                            </strong>
                                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label>День рождения</label>
                                    <div class="input-group">
                                        <input type="text" name="params[birthday]" placeholder="11.02.2017" class="form-control date-picker" value="{{\Carbon\Carbon::parse($params->get('birthday'))->format('d.m.Y')}}">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('params.birthday'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('params.birthday') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Адрес <small>(на него будет осуществляться доставка)</small></label>
                                    <textarea class="form-control" placeholder="ул. Гагарина, д.4 кв. 10, этаж 2, подъезд 1" name="params[address]">{{$params->get('address')}}</textarea>
                                    @if ($errors->has('params.address'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('params.address') }}</small>
                                            </strong>
                                        </span>
                                    @endif

                                </div>

                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary btn-sm" type="submit">Обновить</button>
                        </div>
                    </form>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

    @section('scripts')
        @parent
        <script src="{{asset('ace/components/chosen/chosen.jquery.js')}}"></script>
        <script src="{{asset('ace/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('ace/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js')}}"></script>
        <script src="{{asset('ace/components/select2/dist/js/select2.min.js')}}"></script>
        <script src="{{asset('ace/components/moment/moment.js')}}"></script>
        <script src="{{asset('js/front/cabinet/index.js')}}"></script>
    @endsection
    @include("front.layouts._main.footer")
@endsection

