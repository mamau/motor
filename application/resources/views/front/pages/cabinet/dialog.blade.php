@extends('front.layouts.app')
@section('title')
    {{$feedback->title}}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>{{$feedback->title}}</h3>
                    {!! Breadcrumbs::render('dialog',$feedback) !!}
                    @if (session('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                    @endif
                    <div class="wrapper-dialog">
                        @foreach($feedback->dialog as $dialog)
                            <div class="{{($feedback->author_id == $dialog->author_id)?'user-self':'user-manager'}}">
                                <div class="alert alert-{{($feedback->author_id == $dialog->author_id)?'info text-left ':'success text-right'}}">
                                    <div class="header-quest">
                                        <strong>{{Helpers::userById($dialog->author_id)}}:</strong> <small>{{$dialog->updated_at->format('d.m.Y H:i')}}</small>
                                    </div>
                                    {{$dialog->text}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if($feedback->closed)
                    <div class="alert alert-danger">Тема закрыта администратором</div>
                    @else
                        <h4 class="new-question">Добавить ответ:</h4>
                        <form action="{{route('cabinet.dialog.store',['id' => $feedback->id])}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Ответ</label>
                                        <input type="hidden" name="author_id" value="{{$feedback->author_id}}">
                                        <textarea class="form-control" placeholder="Возможно ли заказать стойку стабилизатора для mazda 3?" name="text">{{old('text')}}</textarea>
                                        @if ($errors->has('text'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('text') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="text-left">
                                <button class="btn btn-primary btn-sm" type="submit">Спросить</button>
                            </div>
                        </form>
                    @endif

                </div>
                <!--Контент-->
            </div>
        </div>
    </div>


    @include("front.layouts._main.footer")
@endsection

@section('scripts')
    @parent
    <script src="{{asset('js/front/cabinet/feedback.js')}}"></script>
@endsection
