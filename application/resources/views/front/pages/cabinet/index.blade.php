@extends('front.layouts.app')
@section('title')
    {{ trans('interface.page.cabinet.title') }}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Добро пожалвать в {{trans('interface.page.cabinet.title')}}, {{$user->name}}</h3>
                    {!! Breadcrumbs::render('cabinet') !!}
                    <table class="table table-striped">
                        <tr>
                            <th>
                                <i class="fa fa-address-card"></i> Учётная запись
                            </th>
                            <th>
                                Заказы
                            </th>
                            <th>
                                Связь
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <a href="{{route('cabinet.profile')}}"><i class="fa fa-id-card-o"></i> Изменить контактную информацию </a>
                                </div>
                                <div>
                                    <a href="{{route('cabinet.password')}}"><i class="fa fa-key"></i> Изменить пароль </a>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a href="{{route('cabinet.orders')}}"><i class="fa fa-history"></i> История заказов </a>
                                </div>
                                <div>
                                    <a href="{{route('cabinet.return')}}"><i class="fa fa-thumbs-o-down"></i> Возвраты </a>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a href="{{route('cabinet.feedback')}}"><i class="fa fa-send"></i> Обратная свзяь </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
