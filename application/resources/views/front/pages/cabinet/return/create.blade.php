@extends('front.layouts.app')
@section('title')
    Сделать возврат
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Сделать возврат</h3>
                    {!! Breadcrumbs::render('return_create') !!}

                    <form action="{{route('cabinet.return')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Заказ в котором надо вернуть товар</label>
                                        <select class="form-control" name="orders">
                                                <option value="0">Выбрать</option>
                                            @foreach($orders as $order)
                                                <option value="{{$order->id}}">
                                                    от {{$order->created_at->format('d.m.Y H:i')}} на {{$order->sum}} р.
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('orders'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('orders') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Причина (кратко)</label>
                                        <input type="text" class="form-control" name="cause" placeholder="Брак детали" value="{{old('cause')}}">
                                        @if ($errors->has('cause'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('cause') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Опишите проблему</label>
                                        <textarea class="form-control" name="description" placeholder="Найдена большая трещина на детали">{{old('description')}}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('description') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Изображение дефекта</label>
                                        <input type="file" name="thumb[]" id="thumb" class="form-control" multiple>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary btn-sm" type="submit">Вернуть</button>
                        </div>
                    </form>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('js/front/cabinet/return.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

