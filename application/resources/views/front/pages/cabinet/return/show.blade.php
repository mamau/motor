@extends('front.layouts.app')
@section('title')
    {{$return->cause}}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>{{$return->cause}}</h3>
                    {!! Breadcrumbs::render('return_show', $return->cause) !!}

                    <p>{{$return->description}}</p>
                    <p>Прикреплённые изображения:</p>
                    <div class="row">
                        @forelse($return->thumbs as $thumb)
                            <div class="col-sm-6 col-xs-12">
                                <img src="{{Helpers::buildImage($thumb)}}" class="img-responsive">
                            </div>
                        @empty
                            <div class="col-xs-12">
                                <p>Вы не прикрепили изображения</p>
                            </div>
                        @endforelse
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('js/front/cabinet/return.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

