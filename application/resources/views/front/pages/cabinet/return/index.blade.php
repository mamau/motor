@extends('front.layouts.app')
@section('title')
    Список возвратов
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Список возвратов</h3>
                    {!! Breadcrumbs::render('return') !!}
                    @if (session('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger">{{ session()->get('danger') }}</div>
                    @endif
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Заказ</th>
                            <th>Причина</th>
                            <th><i class="fa fa-cogs"></i></th>
                        </tr>
                        <tbody>
                            @foreach($returns as $return)
                                <tr>
                                    <td>{{$return['id']}}</td>
                                    <td>{{$return['orderTitle']}}</td>
                                    <td>{{$return['cause']}}</td>
                                    <td>
                                        <a href="{{route('return.show', ['id' => $return['id']])}}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-right">
                        <a href="{{route('return.create')}}" class="btn btn-danger btn-sm">
                            Сделать возврат
                        </a>
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('js/front/cabinet/return.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

