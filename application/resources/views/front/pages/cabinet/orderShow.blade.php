@extends('front.layouts.app')
@section('title')
    Заказ #{{$order->id}}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h3>Заказ #{{$order->id}}</h3>
                    {!! Breadcrumbs::render('order_show', $order) !!}

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Название</td>
                                <td>Цена</td>
                                <td>Количество</td>
                                <td>Диллер</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order->foreignGoods as $good)
                            <tr>
                                <td>{{$good->name}}</td>
                                <td>{{$good->price}} <i class="fa fa-rub"></i></td>
                                <td>{{$good->count}}</td>
                                <td>{{$good->client_type}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>

@section('scripts')
    @parent
    <script src="{{asset('js/front/cabinet/orders.js')}}"></script>
@endsection
@include("front.layouts._main.footer")
@endsection

