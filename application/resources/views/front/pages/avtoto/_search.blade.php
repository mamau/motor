
    @foreach($products as $product)
        <tr id="product_{{$product['RemoteID']}}" class="avtoto-tbl">
            <td>
                {{$product['Name']}}
            </td>
            <td>
                {{$product['Manuf']}}
            </td>


            <td>
                {{$product['Code']}}
            </td>
            <td>
                {{$product['Delivery']}} д.
            </td>
            <td>

                {{Helpers::markup($product['Price'], $markup)}} <i class="fa fa-rub"></i>
            </td>
            <td class="product-info">
                <input type="text" class="count"

                data-min="{{$product['BaseCount']}}"
                data-max="{{($product['MaxCount'] <= 0)?1:$product['MaxCount']}}"
                data-step="{{$product['BaseCount']}}"
                >
                <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCart('{{$product['RemoteID']}}','{{$client_id}}')"
                data-code="{{$product['Code']}}"
                data-manuf="{{$product['Manuf']}}"
                data-name="{{$product['Name']}}"
                data-price="{{Helpers::markup($product['Price'], $markup)}}"
                data-storage="{{$product['Storage']}}"
                data-delivery="{{$product['Delivery']}}"
                data-maxcount="{{($product['MaxCount'] <= 0)?1:$product['MaxCount']}}"
                data-basecount="{{$product['BaseCount']}}"
                data-storagedate="{{$product['StorageDate']}}"
                data-deliverypercent="{{$product['DeliveryPercent']}}"
                data-remoteid="{{$product['RemoteID']}}"
                data-searchid="{{$product['SearchID']}}"
                data-partid="{{$product['PartId']}}"
               data-markup="{{$markup}}"
               data-startprice="{{$product['Price']}}"
                >
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </td>
        </tr>
    @endforeach

