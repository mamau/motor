
    @foreach($products as $product)

        <tr class="armtek-tbl" id="product_{{$product->ARTID}}{{$product->KEYZAK}}">
            <td>{{$product->NAME}}</td>
            <td>{{$product->BRAND}}</td>
            <td>{{$product->PIN}}</td>
            <td class="text-center">{{\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($product->DLVDT))}} д.</td>
            <td>{{Helpers::markup($product->PRICE, $markup)}} <i class="fa fa-rub"></i></td>
            <td class="product-info">
                <input type="text" class="count"
                       data-min="{{(int)$product->MINBM}}"
                       data-max="100"
                       data-step="{{$product->RDPRF}}"
                >
                <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCartArmtek('{{$product->ARTID}}','{{$product->KEYZAK}}','{{$client_id}}')"
                   data-pin="{{$product->PIN}}"
                   data-brand="{{$product->BRAND}}"
                   data-name="{{$product->NAME}}"
                   data-artid="{{$product->ARTID}}"
                   data-parnr="{{$product->PARNR}}"
                   data-keyzak="{{$product->KEYZAK}}"
                   data-rvalue="{{$product->RVALUE}}"
                   data-rdprf="{{$product->RDPRF}}"
                   data-minbm="{{$product->MINBM}}"
                   data-vensl="{{$product->VENSL}}"
                   data-price="{{Helpers::markup($product->PRICE, $markup)}}"
                   data-waers="{{$product->WAERS}}"
                   data-dlvdt="{{$product->DLVDT}}"
                   data-analog="{{$product->ANALOG}}"
                   data-markup="{{$markup}}"
                   data-startprice="{{$product->PRICE}}"

                >
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </td>
        </tr>
    @endforeach

