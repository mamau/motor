@extends('front.layouts.app')
@section('title')
    Платёж прошёл успешно!
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')
                    <h1>Платёж прошёл успешно!</h1>
                    <div class="cart-wrapper">
                        <p>Вы успешно сделали свой заказ. За его статусом вы можете следить в своём личном кабинете.</p>
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

    <script src="{{asset('ace/assets/js/ace-elements.js')}}"></script>
    <script src="{{asset('ace/components/fuelux/dist/js/fuelux.js')}}"></script>
    <script src="{{asset('js/front/pay/success.js')}}"></script>
@endsection
