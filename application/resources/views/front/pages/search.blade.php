@extends('front.layouts.app')
@section('title')
    Результаты поиска
@stop
@section('styles')
    @parent
@stop
@section('content')
    @include('front.layouts._main.header')
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include('front.layouts.sidebar')
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm',['active' => $active,'needle' => $needle])

                    {!! Breadcrumbs::render('search') !!}
                    <h3>Результаты поиска по запросу <i>"{{$needle}}"</i></h3>
                    <div class="page-wrapper">

                        @forelse($products as $product)
                            <div class="col-sm-3 col-xs-12 text-center cat-wrapper">
                                <a href="{{route('front.products.show',['id' => $product->id])}}">
                                    <div class="cat-thumb">
                                        <img src="{{Helpers::thumbXs($product)}}">
                                    </div>
                                    <div class="name-cat text-center">{{$product->name}}</div>
                                    <div class="product-price text-center">
                                        @if($product->price !== 0)
                                            {{$product->price}} <i class=" fa fa-rub"></i>
                                        @else
                                            {{ $product::CLARIFY_PRICE }}
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @empty
                            <small>Нет товаров удовлетворяющие вашему поиску</small>
                        @endforelse

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include('front.layouts._main.footer')
@endsection
@section('scripts')
    @parent
@endsection
