
<div class="widget-box">
    <div class="widget-header">
        <h4 class="widget-title">Фильтр</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <form action="{{route('front.wheels.filter')}}" method="get">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('diameter') ? ' has-error' : '' }}">
                            <label>Диаметр обода в дюймах</label>
                            <select class="form-control" name="diameter">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_diameter') as $val)
                                    <option value="{{$val}}"
                                        @if(count($path) > 0)
                                            @if($path['diameter'] == $val)
                                                selected
                                            @endif
                                        @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('diameter'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('diameter') }}</small>
                                    </strong>
                                </span>
                            @endif


                            </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('width') ? ' has-error' : '' }}">
                            <label>Ширина обода</label>
                            <select class="form-control" name="width">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_width') as $val)
                                    <option value="{{$val}}"
                                        @if(count($path) > 0)
                                            @if($path['width'] == $val)
                                                selected
                                            @endif
                                        @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('width'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('width') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('diameter_hole') ? ' has-error' : '' }}">
                            <label>Крепежные отверстия PCD</label>
                            <select class="form-control" name="diameter_hole">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_diameter_hole') as $val)
                                    <option value="{{$val}}"
                                    @if(count($path) > 0)
                                        @if($path['diameter_hole'] == $val)
                                            selected
                                        @endif
                                    @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('diameter_hole'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('diameter_hole') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group{{ $errors->has('outreach') ? ' has-error' : '' }}">
                            <label>Вылет ET</label>
                            <select class="form-control" name="outreach">
                                <option value="null"
                                @if(count($path) > 0)
                                    @if($path['outreach'] == 'null')
                                        selected
                                    @endif
                                @endif
                                >Все</option>

                                @foreach(config('settings.w_outreach') as $val)
                                    <option value="{{$val}}"
                                        @if(count($path) > 0)
                                            @if($path['outreach'] == $val and $path['outreach'] != 'null')
                                                selected
                                            @endif
                                        @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('outreach'))
                                <span class="help-block">
                                    <strong>
                                        <small>{{ $errors->first('outreach') }}</small>
                                    </strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group{{ $errors->has('diameter_central') ? ' has-error' : '' }}">
                            <label>Центральное отверстие</label>
                            <select class="form-control" name="diameter_central">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_diameter_central') as $val)
                                    <option value="{{$val}}"
                                            @if(count($path) > 0)
                                            @if($path['diameter_central'] == $val)
                                            selected
                                            @endif
                                            @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('diameter_central'))
                                <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('diameter_central') }}</small>
                                        </strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label>Тип диска</label>
                            <select class="form-control" name="type">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_type') as $val)
                                    <option value="{{$val}}"
                                            @if(count($path) > 0)
                                            @if($path['type'] == $val)
                                            selected
                                            @endif
                                            @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('type') }}</small>
                                        </strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
                            <label>Производитель</label>
                            <select class="form-control" name="brand">
                                <option value="0">Все</option>
                                @foreach(config('settings.w_brand') as $val)
                                    <option value="{{$val}}"
                                            @if(count($path) > 0)
                                            @if($path['brand'] == $val)
                                            selected
                                            @endif
                                            @endif
                                    >{{$val}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('brand'))
                                <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('brand') }}</small>
                                        </strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        @if(count($path) > 0)
                            <a class="btn btn-danger btn-sm" href="{{route('front.wheels.index')}}">Сбросить</a>
                        @else
                            <button class="btn btn-danger btn-sm" type="reset">Сбросить</button>
                        @endif

                        <button class="btn btn-primary btn-sm" type="submit">Применить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
