@extends('front.layouts.app')
@section('title')
    {{trans('interface.page.wheels.title')}}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <div class="col-xs-12">
                    @widget('Categories')
                </div>
            </div>
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')

                    {!! Breadcrumbs::render('wheels') !!}
                    @include("front.pages.wheels._parts._filter")
                    <h1>{{trans('interface.page.wheels.title')}}</h1>
                    <div class="page-wrapper">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Диаметер</th>
                                <th>Бренд</th>
                                <th class="price-td">Цена</th>
                                <th class="text-center">
                                    <i class="fa fa-shopping-cart"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($wheels as $wheel)
                                    <tr id="product_{{$wheel->id}}">
                                        <td>
                                            <a href="javascript:void(0)" onclick="showDetails('{{$wheel->id}}')">
                                                {{$wheel->name}}
                                            </a>
                                        </td>
                                        <td>{{$wheel->diameter}}</td>
                                        <td>{{$wheel->brand}}</td>
                                        <td class="price-td"> {{$wheel->price}} <i class="fa fa-rub"></i></td>
                                        <td class="product-info">
                                            <input type="text" class="count"

                                                   data-min="4"
                                                   data-max="100"
                                                   data-step="4"
                                            >
                                            <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCart('{{$wheel->id}}','{{$client_id}}')">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$wheels->appends($path)->links()}}

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/ace-elements.js')}}"></script>
    <script src="{{asset('ace/components/fuelux/dist/js/fuelux.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('js/front/wheels/index.js')}}"></script>
@endsection
