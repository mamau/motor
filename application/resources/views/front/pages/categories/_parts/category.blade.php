@if(count($category->childrenCategory))
    <ul>
        @foreach ($category->childrenCategory as $child)
            <li><a href="{{route('front.categories.show',['categories' => $child->id])}}">{{$child->name}}</a>
                @include('front.pages.categories._parts.category', ['category' => $child])
            </li>
        @endforeach
    </ul>
@endif
