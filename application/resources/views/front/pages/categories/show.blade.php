@extends('front.layouts.app')
@section('title')
    @if($current_category->seo !== null)
        {{$current_category->seo->title_seo}}
    @else
        {{$current_category->name}}
    @endif
@stop
@section('keywords')
    @if($current_category->seo !== null)
        <meta
                name="keywords"
                content="{{$current_category->seo->keywords_seo}}">
    @else
        <meta
                name="keywords"
                content="{{$current_category->name}}">
    @endif

@stop
@section('description')
    @if($current_category->seo !== null)
        <meta
                name="description"
                content="{{$current_category->seo->description_seo}}">
    @else
        <meta
                name="description"
                content="{{$current_category->name}}">
    @endif

@stop
@section('styles')
    @parent

@stop
@section('content')
    @include('front.layouts._main.header')
    <div class="container">
        <div class="content-page">
            <div class="row">
                <div class="col-xs-12">
                    @widget('Categories')
                </div>
            </div>
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include('front.layouts.sidebar')
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')
                    <h1>{{$current_category->name}}</h1>
                    <div class="categories-wrapper">
                        {!! Breadcrumbs::render('category',$current_category) !!}
                        <ul>
                            @foreach ($current_category->childrenCategory as $category)
                                <li>
                                    <a href="{{route('front.categories.show',['categories' => $category->id])}}">{{$category->name}}</a>
                                    @include('front.pages.categories._parts.category')
                                </li>
                            @endforeach
                        </ul>


                        <h3>Товары</h3>
                        @forelse($products as $product)
                            <div class="col-sm-3 col-xs-12 text-center cat-wrapper">
                                <a href="{{route('front.products.show',['id' => $product->id])}}">
                                    <div class="cat-thumb">
                                        <img
                                                src="{{Helpers::thumbXs($product)}}"
                                                alt="{{$product->name}}"
                                                title="{{$product->name}}">
                                    </div>
                                    <div class="name-cat text-center">{{$product->name}}</div>
                                    <div class="product-price text-center">
                                        @if($product->price !== 0)
                                            {{$product->price}} <i class=" fa fa-rub"></i>
                                        @else
                                            {{ $product::CLARIFY_PRICE }}
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @empty
                            <small>Нет товаров в этой категории</small>
                        @endforelse
                        {{ $products->render() }}
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include('front.layouts._main.footer')
@endsection
@section('scripts')
    @parent

@endsection
