@extends('front.layouts.app')
@section('title')
    {{ trans('interface.page.categories.title') }}
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include('front.layouts._main.header')
    <div class="container">
        <div class="content-page">
            <div class="row">
                <div class="col-xs-12">
                    @widget('Categories')
                </div>
            </div>
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include('front.layouts.sidebar')
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')
                    <h1>{{trans('interface.page.categories.title')}}</h1>
                    <div class="categories-wrapper">

                        <ul class="categories-list">
                            @foreach ($categories as $category)
                                <li>
                                    <a href="{{route('front.categories.show',['categories' => $category->id])}}">{{$category->name}}</a>
                                    @include('front.pages.categories._parts.category')
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include('front.layouts._main.footer')
@endsection
@section('scripts')
    @parent

@endsection
