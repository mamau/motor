@foreach($products as $product)
    <tr id="productfa_{{$product->id}}" class="favorit-tbl">
        <td>
            {{$product->name}}
        </td>
        <td>
            {{$product->marka}}
        </td>

        <td>
            {{$product->code}}
        </td>
        <td>
            1-2 д.
        </td>
        <td>
            {{Helpers::markup($product->price, $markup)}} <i class="fa fa-rub"></i>
        </td>

        <td class="product-info">
            <input type="text" class="count"

                   data-min="{{$product->minimum}}"
                   data-max="{{$product->count}}"
                   data-step="{{$product->minimum}}"
            >
            <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCartForumAuto('{{$product->id}}','{{$client_id}}')" data-id="{{$product->id}}">
                <i class="fa fa-shopping-cart"></i>
            </a>
        </td>
    </tr>
@endforeach

