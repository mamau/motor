@extends('front.layouts.app')
@section('title')
    Результаты поиска
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm',['active' => $active,'needle' => $needle])

                    {!! Breadcrumbs::render('search') !!}
                    <h3>Результаты поиска по запросу <i>"{{$needle}}"</i></h3>
                    <div class="page-wrapper">
                        <table class="table table-hover armtek-tbl">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Бренд</th>
                                <th>Код</th>
                                <th>Доставка</th>
                                <th>Цена</th>
                                <th class="text-center">
                                    <i class="fa fa-shopping-cart"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                {!! $result !!}
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/assets/js/ace-elements.js')}}"></script>
    <script src="{{asset('ace/components/fuelux/dist/js/fuelux.js')}}"></script>
    <script src="{{asset('ace/components/jquery.gritter/js/jquery.gritter.js')}}"></script>
    <script src="{{asset('js/front/armtek/search.js')}}"></script>
    <script src="{{asset('js/front/favorit/search.js')}}"></script>
    <script src="{{asset('js/front/forumAuto/search.js')}}"></script>
    <script src="{{asset('js/front/trinity/search.js')}}"></script>
@endsection
