@extends('front.layouts.app')
@section('title')
    Регистрация
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h1>Регистрация</h1>
                    <div class="wrapp-register">
                        <div class="row">
                            <div class="col-xs-8">
                                <form action="{{route('front.register')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('email') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Имя</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('name') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Пароль</label>
                                    <input type="password" class="form-control" name="password" value="{{old('password')}}" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Пароль ёщё раз</label>
                                    <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password_confirmation') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <button type="submit" class="btn btn-primary btn-sm">Зарегистрироваться</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
