@extends('front.layouts.app')
@section('title')
    Забыли пароль?
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    <h1>Забыли пароль ?</h1>
                    <div class="wrapp-register">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-8">
                                <form action="{{route('front.forgot')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Введите свой Email</label>
                                        <input type="email" class="form-control" name="email" value="{{old('email')}}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('email') }}</small>
                                            </strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <button type="submit" class="btn btn-primary btn-sm">Восстановить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
