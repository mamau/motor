@extends('front.layouts.app')
@section('title')
    @if($product->seo !== null)
        {{$product->seo->title_seo}}
    @else
        {{$product->name}}
    @endif
@stop
@section('keywords')
    @if($product->seo !== null)
        <meta
                name="keywords"
                content="{{$product->seo->keywords_seo}}">
    @else
        <meta
                name="keywords"
                content="{{$product->name}}">
    @endif
@stop
@section('description')
    @if($product->seo !== null)
        <meta
                name="description"
                content="{{$product->seo->description_seo}}">
    @else
        <meta
                name="description"
                content="{{$product->name}}">
    @endif
@stop
@section('styles')
    <link
            rel="stylesheet"
            href="{{asset('ace/components/jquery-colorbox/example1/colorbox.css')}}">
    @parent
@stop
@section('content')
    @include('front.layouts._main.header')
    <div class="container">
        <div class="content-page">
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include('front.layouts.sidebar')
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')

                    {!! Breadcrumbs::render('product',$product) !!}
                    <div class="product-wrapper">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="wrapper-img text-center">
                                    <a
                                            href="{{Helpers::thumbSm($product,true)}}"
                                            title="{{$product->name}}"
                                            data-rel="colorbox">
                                        <img
                                                src="{{Helpers::thumbSm($product)}}"
                                                title="{{$product->name}}"
                                                alt="{{$product->name}}">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-8 col-xs-12 product-info">
                                <h1>{{$product->name}}</h1>
                                <table class="table table-striped">
                                    @if ($product->sku)
                                        <tr>
                                            <td>
                                                Код запчасти:
                                            </td>
                                            <td>
                                                {{$product->sku}}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($product->model_product)
                                        <tr>
                                            <td>
                                                Модель:
                                            </td>
                                            <td>
                                                {{$product->model_product}}
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>
                                            Наличие:
                                        </td>
                                        <td>
                                            {{$product->stock_status}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Дата поступления
                                        </td>
                                        <td>
                                            @if($product->date_available !== null)
                                                {{$product->date_available->format('d.m.Y')}}
                                            @else
                                                {{\Carbon\Carbon::now()->format('d.m.Y')}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Цена:
                                        </td>
                                        <td>
                                            @if($product->price !== 0)
                                                <span class="product-price">{{$product->price}}</span>
                                                <i class="fa fa-rub"></i>
                                            @else
                                                {{ $product::CLARIFY_PRICE }}
                                            @endif
                                        </td>
                                    </tr>
                                    @foreach($product->attributes as $attr)
                                        <tr>
                                            <td>{{$attr->name}}</td>
                                            <td>{{$attr->value}}</td>
                                        </tr>
                                    @endforeach
                                    @if($product->price !== 0 && $product->quantity !== 0)
                                        <tr>
                                            <td>
                                            </td>
                                            <td class="text-right">
                                                <input
                                                        type="text"
                                                        id="count"
                                                        data-max="{{$product->quantity}}"
                                                        value="{{$product->minimum}}">
                                                <button
                                                        class="btn btn-sm btn-primary"
                                                        onclick="addToCart('{{$product->id}}','{{$product->name}}','{{$product->price}}','{{$client_id}}','{{$product->minimum?$product->minimum:0}}','{{$product->quantity}}')">
                                                    Купить
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <h4>Описание товара</h4>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="descr-wrapp">
                                    @if(!is_null($product->seo))
                                        {!! $product->seo->full_description !!}
                                    @else
                                        Описание отсутствует
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent
    <script>
        var count_minimum = '{{$product->minimum?$product->minimum:0}}';
        var count_step = '{{$product->minimum?$product->minimum:1}}';
    </script>
    <script src="{{asset('ace/components/jquery-colorbox/jquery.colorbox.js')}}"></script>
    <script src="{{asset('ace/components/fuelux/js/spinbox.js')}}"></script>
    <script src="{{asset('ace/assets/js/src/elements.spinner.js')}}"></script>

    <script src="{{asset('js/front/product.js')}}"></script>
@endsection
