
    @foreach($products as $product)
        <tr id="productf_{{$product->id}}" class="favorit-tbl">
            <td>
                {{$product->naimenovanie}}
            </td>
            <td>
                {{$product->proizvoditel}}
            </td>

            <td>
                {{$product->nomer_po_katalogu}}
            </td>
            <td>
                1-2 д.
            </td>
            <td>
                {{Helpers::markup($product->tsena_po_dogovoru, $markup)}} <i class="fa fa-rub"></i>
            </td>
            <td class="product-info">
                <input type="text" class="count"

                       data-min="{{$product->kratnost}}"
                       data-max="{{$product->kolichestvo}}"
                       data-step="{{$product->kratnost}}"
                >
                <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCartFavorit('{{$product->id}}','{{$client_id}}')" data-id="{{$product->id}}">
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </td>
        </tr>
    @endforeach
