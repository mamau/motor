@if(count($page->childrenPage) > 0)
<ul class="submenu">
    @foreach ($page->childrenPage as $child)
        <li>
        @if(is_null($child->url) || empty($child->url))
            <a href="{{route('front.page.show',['id' => $child->id])}}">
                <span>{{$child->name}}</span>
            </a>
        @include('front.pages.page._parts.child', ['page' => $child])

        @else
            <a href="{{$child->url}}" target="_blank">
                <span>{{$child->name}}</span>
            </a>
        @endif
        @include('front.pages.page._parts.child', ['page' => $child])
        </li>
    @endforeach
</ul>
@endif
