@if(count($page->childrenPage) > 0)
    <ul class="inner-nav">
        @foreach ($page->childrenPage as $child)
            <li class="{{(count($child->childrenPage) > 0)?'tree-nav':'hvr-sweep-to-right'}}">
                @if(is_null($child->url) || empty($child->url))
                    <a href="{{(count($child->childrenPage) > 0)?'#':route('front.page.show',['id' => $child->id])}}">
                        {{$child->name}}
                    </a>
                    @include('front.pages.page._parts.child_left', ['page' => $child])

                @else
                    <a href="{{$child->url}}" target="_blank">
                        {{$child->name}}
                    </a>
                @endif
                @include('front.pages.page._parts.child_left', ['page' => $child])
            </li>
        @endforeach
    </ul>
@endif
