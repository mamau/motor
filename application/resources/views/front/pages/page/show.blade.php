@extends('front.layouts.app')
@section('title')
    {{$page->seo->title_seo}}
@stop
@section('keywords')
    <meta name="keywords" content="{{$page->seo->keywords_seo}}" >
@stop
@section('description')
    <meta name="description" content="{{$page->seo->description_seo}}" >
@stop
@section('styles')
    @parent

@stop
@section('content')
    @include("front.layouts._main.header")
    <div class="container">
        <div class="content-page">
            <div class="row">
                <div class="col-xs-12">
                    @widget('Categories')
                </div>
            </div>
            <div class="row">
                <!--Левая колонка-->
                <div class="col-sm-2 no-padding-right">
                    @include("front.layouts.sidebar")
                </div>
                <!--Левая колонка-->

                <!--Контент-->
                <div class="col-sm-10 content-wrapp">
                    @widget('searchForm')

                    {!! Breadcrumbs::render('page',$page) !!}
                    <h1>{{$page->name}}</h1>
                    <div class="page-wrapper">
                        {!! $page->seo->full_description !!}
                    </div>
                </div>
                <!--Контент-->
            </div>
        </div>
    </div>
    @include("front.layouts._main.footer")
@endsection
@section('scripts')
    @parent

@endsection
