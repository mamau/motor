@foreach($products as $key => $product)
    <tr class="stparts-tbl" id="product_{{$key+1}}">
        <td>
            {{$product->description}}
        </td>
        <td>
            {{$product->brand}}
        </td>

        <td>
            {{$product->number}}
        </td>
        <td>
            1-2 д.
        </td>
        <td>
            {{Helpers::markup($product->price, $markup)}} <i class="fa fa-rub"></i>
        </td>

        <td class="product-info">
            <input type="text" class="count"

                   data-min="{{$product->packing}}"
                   data-max="100"
                   data-step="{{$product->packing}}"
            >
            <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCartStparts('{{$key+1}}','{{$client_id}}')"
               data-id="{{$key+1}}"
               data-distributorId="{{$product->distributorId}}"
               data-grp="{{$product->grp}}"
               data-code="{{$product->code}}"
               data-brand="{{$product->brand}}"
               data-number="{{$product->number}}"
               data-numberFix="{{$product->numberFix}}"
               data-description="{{$product->description}}"
               data-availability="{{$product->availability}}"
               data-packing="{{$product->packing}}"
               data-deliveryPeriod="{{$product->deliveryPeriod}}"
               data-deliveryPeriodMax="{{$product->deliveryPeriodMax}}"
               data-deadlineReplace="{{$product->deadlineReplace}}"
               data-distributorCode="{{$product->distributorCode}}"
               data-supplierCode="{{$product->supplierCode}}"
               data-supplierColor="{{$product->supplierColor}}"
               data-supplierDescription="{{$product->supplierDescription}}"
               data-price="{{Helpers::markup($product->price, $markup)}}"
               data-weight="{{$product->weight}}"
               data-volume="{{$product->volume}}"
               data-deliveryProbability="{{$product->deliveryProbability}}"
               data-lastUpdateTime="{{$product->lastUpdateTime}}"
               data-additionalPrice="{{$product->additionalPrice}}"
               data-noReturn="{{$product->noReturn}}"
               data-markup="{{$markup}}"

            >
                <i class="fa fa-shopping-cart"></i>
            </a>
        </td>
    </tr>
@endforeach
