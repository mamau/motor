@if (count($brands))
    @foreach($brands as $brand)
        <tr onclick="searchIt('{{$brand->brand}}','{{$brand->number}}');">
            <td>{{$brand->brand}}</td>
            <td>{{$brand->number}}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td>Нет результатов</td>
    </tr>
@endif
