    @foreach($products as $product)
        <tr id="product_{{$product['code']}}" class="teal-tbl">
            <td>
                {{$product['caption']}}
            </td>
            <td>
                {{$product['producer']}}
            </td>

            <td>
                {{$product['code']}}
            </td>
            <td>
                1-2 д.
            </td>
            <td>
                {{Helpers::markup($product['price'], $markup)}} <i class="fa fa-rub"></i>
            </td>
            <td class="product-info">
                <input type="text" class="count"

                       data-min="{{$product['minOrderCount']}}"
                       data-max="{{$product['rest']}}"
                       data-step="{{$product['minOrderCount']}}"
                >
                <a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="addToCartTrinity('{{$product['code']}}','{{$client_id}}')"
                   data-id="{{$product['code']}}"

                   data-code="{{$product['code']}}"
                   data-caption="{{$product['caption']}}"
                   data-currency="{{$product['currency']}}"
                   data-price="{{Helpers::markup($product['price'], $markup)}}"
                   data-producer="{{$product['producer']}}"
                   data-stock="{{$product['stock']}}"
                   data-supplier_id="{{$product['supplier_id']}}"
                   data-deliverydays="{{$product['deliverydays']}}"
                   data-rest="{{$product['rest']}}"
                   data-minOrderCount="{{$product['minOrderCount']}}"
                   data-bid="{{$product['bid']}}"
                   data-bra_id="{{$product['bra_id']}}"
                   data-product_code="{{$product['product_code']}}"
                   data-outputTime="{{$product['outputTime']}}"
                   data-store="{{$product['store']}}"
                   data-minCount="{{$product['minOrderCount']}}"
                   data-maxCount="{{$product['rest']}}"
                   data-client_type="trinity"
                >
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </td>
        </tr>
    @endforeach
