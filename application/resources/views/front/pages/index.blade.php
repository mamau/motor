@extends('front.layouts.app')
@section('title')
    {{ trans('interface.page.main.title') }}
@stop
@section('styles')
    @parent
    <link rel="stylesheet" href="{{asset('ace/components/_mod/jquery-ui.custom/jquery-ui.custom.css')}}">
@stop
@section('content')
    @include('front.layouts._main.header')
    <div class="container">
       <div class="content-page">
           <div class="row">
               <div class="col-xs-12">
                   @widget('Categories')
               </div>
           </div>
           <div class="row">
               <!--Левая колонка-->
               <div class="col-sm-2 no-padding-right">
                   @include('front.layouts.sidebar')
               </div>
               <!--Левая колонка-->

               <!--Контент-->
               <div class="col-sm-10 content-wrapp">
                   @widget('searchForm')

                   @widget('slider')

                   @widget('slider', ['type' => 'banner'])

                   {{--@widget('special')--}}

                   @widget('welcomeBlock')

                   @widget('newsBlock', ['type' => 'row'])
               </div>
               <!--Контент-->
           </div>
       </div>
        <?=$sape->return_links(2);?>
    </div>
    @include('front.layouts._main.footer')
@endsection
@section('scripts')
    @parent
    <script src="{{asset('ace/components/_mod/jquery-ui.custom/jquery-ui.custom.js')}}"></script>

@endsection
