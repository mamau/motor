@extends('emails.layouts.simple')
@section('caption')
    Поступил новый заказ
@stop
@section('content')
    <p>С вашего сайта <a href="https://мотор48.рф" target="_blank">Мотор48</a> поступил новый заказ!</p>
    <div>Просмотреть детали заказа вы можете в <a href="https://мотор48.рф/admin">административной части сайта</a></div>
@stop
