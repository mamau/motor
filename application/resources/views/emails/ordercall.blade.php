@extends('emails.layouts.simple')
@section('caption')
    Заказан звонок
@stop
@section('content')
    <p>С вашего сайта пришёл заказ на звонок!</p>
    <p>Данные пользователя ниже:</p>
    <div>Имя: <strong>{{$name}}</strong></div>
    <div>Телефон: <strong>{{$phone}}</strong></div>
@stop
