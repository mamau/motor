@extends('emails.layouts.simple')
@section('caption')
    Задан вопрос
@stop
@section('content')
    <p>С вашего сайта пришёл вопрос!</p>
    <div>Пользователь <strong>{{$name}}</strong> спрашивает:</div>
    <div><i>{{$question}}</i></div>
    <div>Ответить ему вы можете на его электронный адресс: <strong>{{$email}}</strong></div>
@stop
