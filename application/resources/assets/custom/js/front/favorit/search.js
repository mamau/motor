/**
 * Created by mamau on 05.04.17.
 */
function addToCartFavorit(id, client_id) {

    if(client_id != 0){
        var obj = $("#productf_"+id+" .product-info a");
        var count = $("#productf_"+id+" .ace-spinner input").val();

        var data = new FormData();
        data.append('id', obj.data('id'));

        data.append('count', count);


        $.ajax({
            url: '/favorit/basket/add',
            method: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data: data,
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}
$(function () {

    $('.count').each(function(){
        var obj = $(this);
        var min = obj.data('min');
        var max = obj.data('max');
        var step = obj.data('step');

        obj.ace_spinner({
            value: (Number(min) <= 0)? 1: min,
            min: (Number(min) <= 0)? 1: min,
            max: (Number(max) <= 0)? 1: max,
            step: (Number(step) <= 0) ? 1: step,
            on_sides: true,
            icon_up:'ace-icon fa fa-plus',
            icon_down:'ace-icon fa fa-minus',
            btn_up_class:'btn-success' ,
            btn_down_class:'btn-danger'
        });
        $(".icon-only").removeClass('icon-only');
    });

});
