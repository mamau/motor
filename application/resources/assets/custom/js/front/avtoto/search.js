/**
 * Created by mamau on 16.03.17.
 */

function addToCart(id, client_id) {

    if(client_id != 0){
        var obj = $("#product_"+id+" .product-info a");
        var count = $("#product_"+id+" .ace-spinner input").val();

        var data = new FormData();
        data.append('Code', obj.data('code'));
        data.append('Manuf', obj.data('manuf'));
        data.append('Name', obj.data('name'));
        data.append('Price', obj.data('price'));
        data.append('Storage', obj.data('storage'));
        data.append('Delivery', obj.data('delivery'));
        data.append('MaxCount', obj.data('maxcount'));
        data.append('BaseCount', obj.data('basecount'));
        data.append('StorageDate', obj.data('storagedate'));
        data.append('DeliveryPercent', obj.data('deliverypercent'));
        data.append('RemoteID', obj.data('remoteid'));
        data.append('SearchID', obj.data('searchid'));
        data.append('PartId', obj.data('partid'));
        data.append('markup', obj.data('markup'));
        data.append('startprice', obj.data('startprice'));
        data.append('Count', count);


        $.ajax({
            url: '/avtoto/basket/add',
            method: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data: data,
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}

$(function () {

    $('.count').each(function(){
        var obj = $(this);
        var min = obj.data('min');
        var max = obj.data('max');
        var step = obj.data('step');

        obj.ace_spinner({
            value: (Number(min) <= 0)? 1: min,
            min: (Number(min) <= 0)? 1: min,
            max: (Number(max) <= 0)? 1: max,
            step: (Number(step) <= 0) ? 1: step,
            on_sides: true,
            icon_up:'ace-icon fa fa-plus',
            icon_down:'ace-icon fa fa-minus',
            btn_up_class:'btn-success' ,
            btn_down_class:'btn-danger'
        });
        $(".icon-only").removeClass('icon-only');
    });

});
