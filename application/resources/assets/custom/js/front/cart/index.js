/**
 * Created by mamau on 10.02.17.
 */
$(document).ready(function(){

    var sum = $("#cart_sum").text();
    $("#yandex_money_sum").val(Number(sum.replace(',','')));
    $("#yandex_money_customer_number").val(user_id);
});

$('.count').each(function () {

   var obj = $(this);
   var min = obj.data('min');
   var max = obj.data('max');
   var value = obj.val();
   //console.log(obj.closest('.ace-spinner').find('.btn'));

    obj.ace_spinner({
        value: value,
        min: min,
        max: max,
        step: min,
        on_sides: true,
        icon_up:'ace-icon fa fa-plus',
        icon_down:'ace-icon fa fa-minus',
        btn_up_class:'btn-success' ,
        btn_down_class:'btn-danger'
    });


    $(".icon-only").removeClass('icon-only');
});

$(".spinbox-up, .spinbox-down").click(function () {
    $(this).closest('.ace-spinner').find('.count').change();
});

$('.count').change(function(){
    var rowId = $(this).closest('tr').data('rowid');

    $.ajax({
        url: '/cart/update',
        method: 'post',
        data: {
            rowId: rowId,
            count: $(this).val()
        },
        success: function(data){

            if(data.status){

                $("."+data.rowid+" .price_good span").html(Number(data.product.price * data.product.qty).toFixed(2));
                $("."+data.rowid+" .quant_good span").html(data.product.qty);
                $("#products_count").html(data.productcount);
                $("#cart_sum").html(data.total);
                $(".total_cart").html(data.total);
                $("#products_amount").html(data.total);
                $("#yandex_money_sum").val(Number(data.total.replace(',','')));
            }
        }
    });
});
var user_params = jQuery.parseJSON(params);

$("#sendForm").click(function(e){
    e.preventDefault();
    if(user_params.address.length > 10) {

        if ($("#agree").is(':checked')) {
            var user_id = $("#yandex_money_customer_number").val();
            var sum = $("#yandex_money_sum").val();
            var goods = [];

            $(".product_row").each(function () {
                goods.push($(this).data('productid'));
            });

            $.ajax({
                url: '/order/make',
                method: 'post',
                data: {
                    user_id: user_id,
                    sum: sum,
                    goods: goods
                },
                success: function (data) {

                    if (data.status) {

                        $("#orderNumber").val(data.order_id);
                        $("#ya_form").submit();
                    }
                }
            });
        } else {
            $("#agreetext").parent().css({
                'borderBottom': '1px solid red',
                'paddingRight': '10px'
            });
            $.gritter.add({
                text: 'Вы должны согласиться с условиями опалаты и доставки',
                sticky: false,
                time: '5000',
                class_name: 'gritter-error'
            });
        }
    }else{
        $.gritter.add({
            text: 'Заполните свой профиль в личном кабинете',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }

});
