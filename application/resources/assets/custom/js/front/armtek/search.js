/**
 * Created by mamau on 26.03.17.
 */
function addToCartArmtek(id,key, client_id) {

    if(client_id != 0){
        var obj = $("#product_"+id+key+" .product-info a");
        var count = $("#product_"+id+key+" .ace-spinner input").val();

        var data = new FormData();
        data.append('PIN', obj.data('pin'));
        data.append('BRAND', obj.data('brand'));
        data.append('NAME', obj.data('name'));
        data.append('ARTID', obj.data('artid'));
        data.append('PARNR', obj.data('parnr'));
        data.append('KEYZAK', obj.data('keyzak'));
        data.append('RVALUE', obj.data('rvalue'));
        data.append('RDPRF', obj.data('rdprf'));
        data.append('MINBM', obj.data('minbm'));
        data.append('VENSL', obj.data('vensl'));
        data.append('PRICE', obj.data('price'));
        data.append('WAERS', obj.data('waers'));
        data.append('DLVDT', obj.data('dlvdt'));
        data.append('ANALOG', obj.data('analog'));
        data.append('markup', obj.data('markup'));
        data.append('startprice', obj.data('startprice'));
        data.append('COUNT', count);


        $.ajax({
            url: '/armtek/basket/add',
            method: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data: data,
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}
$(function () {

    $('.count').each(function(){
        var obj = $(this);
        var min = obj.data('min');
        var max = obj.data('max');
        var step = obj.data('step');

        obj.ace_spinner({
            value: (Number(min) <= 0)? 1: min,
            min: (Number(min) <= 0)? 1: min,
            max: (Number(max) <= 0)? 1: max,
            step: (Number(step) <= 0) ? 1: step,
            on_sides: true,
            icon_up:'ace-icon fa fa-plus',
            icon_down:'ace-icon fa fa-minus',
            btn_up_class:'btn-success' ,
            btn_down_class:'btn-danger'
        });
        $(".icon-only").removeClass('icon-only');
    });

});
