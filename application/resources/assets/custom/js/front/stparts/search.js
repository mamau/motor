$(function () {

    $('.count').each(function(){
        var obj = $(this);
        var min = obj.data('min');
        var max = obj.data('max');
        var step = obj.data('step');

        obj.ace_spinner({
            value: (Number(min) <= 0)? 1: min,
            min: (Number(min) <= 0)? 1: min,
            max: (Number(max) <= 0)? 1: max,
            step: (Number(step) <= 0) ? 1: step,
            on_sides: true,
            icon_up:'ace-icon fa fa-plus',
            icon_down:'ace-icon fa fa-minus',
            btn_up_class:'btn-success' ,
            btn_down_class:'btn-danger'
        });
        $(".icon-only").removeClass('icon-only');
    });

});

function addToCartStparts(id, client_id) {

    if(client_id != 0){
        var obj = $("#product_"+id+" .product-info a");
        var count = $("#product_"+id+" .ace-spinner input").val();

        var data = new FormData();
        data.append('id', !!obj.data('id')?obj.data('id'):'');
        data.append('distributorId', !!obj.data('distributorId')?obj.data('distributorId'):'');
        data.append('grp', !!obj.data('grp')?obj.data('grp'):'');
        data.append('code', !!obj.data('code')?obj.data('code'):'');
        data.append('brand', !!obj.data('brand')?obj.data('brand'):'');
        data.append('number', !!obj.data('number')?obj.data('number'):'');
        data.append('numberFix', !!obj.data('numberFix')?obj.data('numberFix'):'');
        data.append('description', !!obj.data('description')?obj.data('description'):'');
        data.append('availability', !!obj.data('availability')?obj.data('availability'):'');
        data.append('packing', !!obj.data('packing')?obj.data('packing'):'');
        data.append('deliveryPeriod', !!obj.data('deliveryPeriod')?obj.data('deliveryPeriod'):'');
        data.append('deliveryPeriodMax', !!obj.data('deliveryPeriodMax')?obj.data('deliveryPeriodMax'):'');
        data.append('deadlineReplace', !!obj.data('deadlineReplace')?obj.data('deadlineReplace'):'');
        data.append('distributorCode', !!obj.data('distributorCode')?obj.data('distributorCode'):'');
        data.append('markup', !!obj.data('markup')?obj.data('markup'):'');
        data.append('supplierCode', !!obj.data('supplierCode')?obj.data('supplierCode'):'');
        data.append('supplierColor', !!obj.data('supplierColor')?obj.data('supplierColor'):'');
        data.append('supplierDescription', !!obj.data('supplierDescription')?obj.data('supplierDescription'):'');
        data.append('price', !!obj.data('price')?obj.data('price'):'');
        data.append('weight', !!obj.data('weight')?obj.data('weight'):'');
        data.append('volume', !!obj.data('volume')?obj.data('volume'):'');
        data.append('deliveryProbability', !!obj.data('deliveryProbability')?obj.data('deliveryProbability'):'');
        data.append('lastUpdateTime', !!obj.data('lastUpdateTime')?obj.data('lastUpdateTime'):'');
        data.append('additionalPrice', !!obj.data('additionalPrice')?obj.data('additionalPrice'):'');
        data.append('noReturn', !!obj.data('noReturn')?obj.data('noReturn'):'');
        data.append('Count', count);
        data.append('MaxCount', 100);
        data.append('BaseCount', obj.data('packing'));




        $.ajax({
            url: '/stparts/basket/add',
            method: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data: data,
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}
