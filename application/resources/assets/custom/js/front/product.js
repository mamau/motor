/**
 * Created by mamau on 29.01.17.
 */
var colorbox_params = {
    rel: 'colorbox',
    reposition:true,
    scalePhotos:true,
    scrolling:false,
    previous:'<i class="ace-icon fa fa-arrow-left"></i>',
    next:'<i class="ace-icon fa fa-arrow-right"></i>',
    close:'&times;',
    current:'{current} of {total}',
    maxWidth:'100%',
    maxHeight:'100%',
    onOpen:function(){
        $overflow = document.body.style.overflow;
        document.body.style.overflow = 'hidden';
    },
    onClosed:function(){
        document.body.style.overflow = $overflow;
    },
    onComplete:function(){
        $.colorbox.resize();
    }
};
$('.wrapper-img [data-rel="colorbox"]').colorbox(colorbox_params);


$('#count').ace_spinner({step: Number(count_step), min:count_minimum,on_sides: true, icon_up:'ace-icon fa fa-plus ', icon_down:'ace-icon fa fa-minus ', btn_up_class:'btn-primary' , btn_down_class:'btn-danger'});

$(".spinbox-buttons button i").removeClass('icon-only');


function addToCart(id, name, price, client_id, min, max){

    if(client_id != 0){
        var quant = $('#count').val();

        $.ajax({
            url: '/cart/add',
            method:'post',
            data: {
                id: id,
                name: name,
                quantity: quant,
                price: price,
                min: min,
                max: max
            },
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}


