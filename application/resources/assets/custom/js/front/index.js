$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

$(document).ready(function(){
   $(".carousel-inner .item:first-child").addClass('active');

   $("#searchBrand").keyup(function(){
        var obj = $(this);

        if(obj.val().length >=3 ){
            $.ajax({
                method:'post',
                url: '/stparts/brand',
                data:{
                    code: obj.val()
                },
                success: function(data){
                    if(data.status){
                        var obj = $(".needle-search");
                        obj.find('tbody').html(data.html);
                        obj.addClass('active');
                    }
                }
            });
        }
   });
});

function searchIt(brand, code) {
    window.location.href = '/stparts/codebrand/'+brand+'/'+code;
}


$(function(){

    $(".tree-nav").click(function(e){

        var obj = $(this);

        obj.toggleClass('active');
        if(obj.hasClass('active')){
            var height = $('li',obj).length * 40;
            $('ul.inner-nav',obj).css('height',height+'px');
        }else{
            $('ul.inner-nav',obj).css('height',0+'px');
        }
        obj.find('a i').toggleClass(function(){
            if($(this).hasClass('fa-angle-right')){
                return 'fa-angle-down';
            }else{
                return 'fa-angle-right';
            }
        });

    });

});



function destroyProduct(id){

    $.ajax({
        url: '/cart/remove',
        method:'post',
        data: {
            id: id
        },
        success: function(data){

            if(data.status){

                $("#products_count").html(data.data.count);
                $("#products_amount").html(data.data.total);
                $("."+id).remove();
            }
        }
    });
}

$('[data-rel=tooltip]').tooltip();

$(".cart").click(function(){

    $(".cart_product_list").toggleClass('active');
});
function orderCall(){


    bootbox.dialog({
        title: 'Заказать звонок',
        message: '<div class="row">  ' +
        '<div class="col-xs-12"> ' +
        '<div class="row">'+
        '<div class="col-xs-12">' +
        '<div class="form-group"> ' +
        '<label>Ваше имя</label> ' +
        '<input type="text" class="form-control" name="name">'+
        '<span class="help-block"><small></small></span>'+
        '</div>' +
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<div class="col-xs-12">' +
        '<div class="form-group"> ' +
        '<label>Телефон</label> ' +
        '<input type="text" class="form-control input-mask-phone" name="phone">'+
        '<span class="help-block"><small></small></span>'+
        '</div>' +
        '</div>' +
        '</div>'+
        '</div>' +
        '</div>',
        buttons: {

            dismiss: {
                label: "Отмена",
                className: "btn-default btn-sm pull-left",
                callback: function () {

                }
            },
            success: {
                label: "Заказать звонок",
                className: "btn-primary btn-sm",
                callback: function () {

                    $.ajax({
                        url: '/index/orderCall',
                        method: 'post',
                        data:{
                            name: $(".modal-dialog input[name='name']").val(),
                            phone: $(".modal-dialog input[name='phone']").val()
                        },
                        success: function(data){

                            if(data.status){
                                $.gritter.add({
                                    text: 'Вы успешно заказали звонок. Наши менеджеры свяжутся с вами.',
                                    sticky: false,
                                    time: '5000',
                                    class_name: 'gritter-info'
                                });
                                bootbox.hideAll();
                            }else{
                                $.gritter.add({
                                    text: 'Произошла ошибка, попробуйте позже',
                                    sticky: false,
                                    time: '5000',
                                    class_name: 'gritter-error'
                                });
                                bootbox.hideAll();
                            }
                        },
                        error: function (response) {

                            var obj = response.responseJSON;

                            $.each(obj, function(index, value) {

                                gritterErrorPromo(index, value);
                            });
                        }
                    });
                    return false;
                }

            }
        }
    });

    $('.input-mask-phone').mask('+7(999) 999-9999');
}


function feedback(){


    bootbox.dialog({
        title: 'Задать вопрос',
        message: '<div class="row">  ' +
        '<div class="col-xs-12"> ' +
        '<div class="row">'+
        '<div class="col-xs-12">' +
        '<div class="form-group"> ' +
        '<label>Ваше имя</label> ' +
        '<input type="text" class="form-control" name="name">'+
        '<span class="help-block"><small></small></span>'+
        '</div>' +
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<div class="col-xs-12">' +
        '<div class="form-group"> ' +
        '<label>Ваш E-mail</label> ' +
        '<input type="email" class="form-control" name="email">'+
        '<span class="help-block"><small></small></span>'+
        '</div>' +
        '</div>' +
        '</div>'+
        '<div class="row">'+
        '<div class="col-xs-12">' +
        '<div class="form-group"> ' +
        '<label>Ваш вопрос</label> ' +
        '<textarea class="form-control" name="question"></textarea>'+
        '<span class="help-block"><small></small></span>'+
        '</div>' +
        '</div>' +
        '</div>'+
        '</div>' +
        '</div>',
        buttons: {

            dismiss: {
                label: "Отмена",
                className: "btn-default btn-sm pull-left",
                callback: function () {

                }
            },
            success: {
                label: "Задать вопрос",
                className: "btn-primary btn-sm",
                callback: function () {

                    $.ajax({
                        url: '/index/feedback',
                        method: 'post',
                        data:{
                            name: $(".modal-dialog input[name='name']").val(),
                            email: $(".modal-dialog input[name='email']").val(),
                            question: $(".modal-dialog textarea[name='question']").val()
                        },
                        success: function(data){

                            if(data.status){
                                $.gritter.add({
                                    text: 'Вы успешно задали вопрос. В ближайшее время наши менеджеры ответят вам.',
                                    sticky: false,
                                    time: '5000',
                                    class_name: 'gritter-info'
                                });
                                bootbox.hideAll();
                            }else{
                                $.gritter.add({
                                    text: 'Произошла ошибка, попробуйте позже',
                                    sticky: false,
                                    time: '5000',
                                    class_name: 'gritter-error'
                                });
                                bootbox.hideAll();
                            }
                        },
                        error: function (response) {

                            var obj = response.responseJSON;

                            $.each(obj, function(index, value) {

                                gritterErrorPromo(index, value);
                            });
                        }
                    });
                    return false;
                }
            }
        }
    });
}

// function gritterErrorPromo(type){
//
//     $.gritter.add({
//         title: 'Неудача',
//         text: type.join(';'),
//         sticky: false,
//         time: '5000',
//         class_name: 'gritter-error'
//     });
// }

function gritterErrorPromo(index, type){

    var parent = $(".modal-dialog [name='"+index+"']").closest('.form-group');
    parent.addClass('has-error');
    parent.find('.help-block small').html(type.join(';'));
    parent.find('[name="'+index+'"]').addClass('has-error');

    // $.gritter.add({
    //     title: 'Неудача',
    //     text: type.join(';'),
    //     sticky: true,
    //     time: '5000',
    //     class_name: 'gritter-error'
    // });
}

$(function () {
    $('.sidebar__item ul a').click(function (event) {
        event.stopPropagation();
    })
    $('.sidebar__item').click(function(event) {
        var obj = $(this);
        if (obj.children('ul').length) {
            event.preventDefault();
            if (obj.hasClass('active')) {
                obj.removeClass('active');
            } else {
                $('.sidebar__item').removeClass('active');
                obj.addClass('active');
            }
        }
    })
});
