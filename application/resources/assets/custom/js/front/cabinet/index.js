/**
 * Created by mamau on 16.02.17.
 */

$('.input-mask-phone').mask('+7(999) 999-9999');

$('.chosen-select').chosen({allow_single_deselect:true});

$('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'dd.mm.yyyy',
    language: 'ru',
    orientation: 'bottom auto',
    todayBtn: 'linked'
}).next().on(ace.click_event, function(){
    $(this).prev().focus();
});


$("#filterCity").select2({
    delay: 250,
    minimumInputLength: 1,
    placeholder: "Грязи",
    ajax: {
        url: '/city/search',
        method: 'post',
        dataType: 'json',
        data: function (term, page) {

            return { query: term };
        },
        processResults: function (data, page) {
            return {
                results: data[0]
            };
        }
    },
    language: {
        inputTooShort: function(args) {
            return "Введите город";
        },
        inputTooLong: function(args) {
            return "Слишком много символов";
        },
        errorLoading: function() {
            return "Ошибка загрузки результата";
        },
        loadingMore: function() {
            return "Loading more results";
        },
        noResults: function() {
            return "Не найдено";
        },
        searching: function() {
            return "Поиск...";
        },
        maximumSelected: function(args) {
            return "Ошибка загрузки";
        }
    }
});
