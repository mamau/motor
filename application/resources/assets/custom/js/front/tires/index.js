/**
 * Created by mamau on 15.04.17.
 */

function addToCart(id, client_id) {

    if(client_id != 0){

        var count = $("#product_"+id+" .ace-spinner input").val();

        var data = new FormData();
        data.append('id', id);
        data.append('count', count);


        $.ajax({
            url: '/tires/basket/add',
            method: 'post',
            cache: false,
            processData: false,
            contentType: false,
            data: data,
            success: function(data){

                if(data.status){

                    $("#products_count").html(data.data.count);
                    $("#products_amount").html(data.data.total);
                    $(".cart_product_list .table").html(data.data.html);
                    $.gritter.add({
                        text: 'Товар '+data.data.name+' успшено добавлен в корзину',
                        sticky: false,
                        time: '5000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Войдите на сайт или зарегистрируйтесь, чтобы заказывать товар',
            sticky: false,
            time: '5000',
            class_name: 'gritter-error'
        });
    }
}

function showDetails(id) {
    $.ajax({
        url: '/tires/tire',
        method: 'post',
        data:{
            id : id
        },
        success: function (data) {
            if(data.status){

                bootbox.dialog({
                    title: data.data.name,
                    message: '<div class="row">' +
                    '<div class="col-xs-12">' +
                    '<table class="table table-striped">' +
                    '<tr>'+
                    '<td>Ширина</td>'+
                    '<td>'+data.data.width+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Высота профиля</td>'+
                    '<td>'+data.data.height+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Диаметр обода в дюймах</td>'+
                    '<td>'+data.data.diameter+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Индекс скорости</td>'+
                    '<td>'+data.data.index_speed+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Индекс нагрузки </td>'+
                    '<td>'+data.data.index_load+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Тип шины </td>'+
                    '<td>'+data.data.type+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Сезон </td>'+
                    '<td>'+data.data.season+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Бренд</td>'+
                    '<td>'+data.data.brand+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Модель</td>'+
                    '<td>'+data.data.model+'</td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td>Цена</td>'+
                    '<td>'+data.data.price+' <i class="fa fa-rub"></i></td>'+
                    '</tr>'+
                    '</table>'+
                    '</div>' +
                    '</div>',
                    buttons: {

                        dismiss: {
                            label: "Закрыть",
                            className: "btn-default btn-sm",
                            callback: function () {

                            }
                        }
                    }
                });
            }
        }
    });
}

$(function () {

    $('.count').each(function(){
        var obj = $(this);
        var min = obj.data('min');
        var max = obj.data('max');
        var step = obj.data('step');

        obj.ace_spinner({
            value: (Number(min) <= 0)? 1: min,
            min: (Number(min) <= 0)? 1: min,
            max: (Number(max) <= 0)? 1: max,
            step: (Number(step) <= 0) ? 1: step,
            on_sides: true,
            icon_up:'ace-icon fa fa-plus',
            icon_down:'ace-icon fa fa-minus',
            btn_up_class:'btn-success' ,
            btn_down_class:'btn-danger'
        });
        $(".icon-only").removeClass('icon-only');
    });

});
