
function parseMotor() {
    $.ajax({
        type: 'POST',
        url: '/admin/price/motor',
        beforeSend: function(){
            $(".preloader").toggleClass('active');
        },
        success: function (data) {
            $(".preloader").toggleClass('active');
            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Задача на разбор файла поставлена, изменения вступят в силу через 20-30 минут',
                    sticky: false,
                    time: '10000',
                    class_name: 'gritter-success'
                });
            }
        },
        error: function (data) {
            $(".preloader").toggleClass('active');
            $.gritter.add({
                title: 'Неудача',
                text: 'Ошибка разбора файла 1С',
                sticky: false,
                time: '3000',
                class_name: 'gritter-error'
            });
        }
    });
}

function uploadWheels() {
    $.ajax({
        type: 'POST',
        url: '/admin/price/wheels',
        beforeSend: function(){
            $(".preloader").toggleClass('active');
        },
        success: function (data) {
            $(".preloader").toggleClass('active');
            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Шины успешно внесёны в БД',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        },
        error: function (data) {
            $(".preloader").toggleClass('active');
            $.gritter.add({
                title: 'Неудача',
                text: 'Ошибка разбора файла с шинами',
                sticky: false,
                time: '3000',
                class_name: 'gritter-error'
            });
        }
    });
}
function uploadTires() {
    $.ajax({
        type: 'POST',
        url: '/admin/price/tires',
        beforeSend: function(){
            $(".preloader").toggleClass('active');
        },
        success: function (data) {
            $(".preloader").toggleClass('active');
            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Диски успешно внесёны в БД',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        },
        error: function (data) {
            $(".preloader").toggleClass('active');
            $.gritter.add({
                title: 'Неудача',
                text: 'Ошибка разбора файла с дисками',
                sticky: false,
                time: '3000',
                class_name: 'gritter-error'
            });
        }
    });
}
function parseFavorit(type) {
    $.ajax({
        type: 'POST',
        url: '/admin/price/parse',
        data: {
            type: type
        },
        beforeSend: function(){
            $(".preloader").toggleClass('active');
        },
        success: function (data) {
            $(".preloader").toggleClass('active');
            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Прайс успешно внесён в БД',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        },
        error: function (data) {
            $(".preloader").toggleClass('active');
            $.gritter.add({
                title: 'Неудача',
                text: 'Ошибка разбора файла',
                sticky: false,
                time: '3000',
                class_name: 'gritter-error'
            });
        }
    });
}
function uploadFavorit() {

    $.ajax({
        type: 'POST',
        url: '/admin/price/import',
        beforeSend: function () {
            $(".preloader").toggleClass('active');
        },
        success: function (data) {
            $(".preloader").toggleClass('active');
            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Прайс успешно скачан',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        },
        error: function (data) {
            $(".preloader").toggleClass('active');
            $.gritter.add({
                title: 'Неудача',
                text: 'В ходе скачивания прайса проузошла ошибка',
                sticky: false,
                time: '3000',
                class_name: 'gritter-error'
            });
        }
    });
}
