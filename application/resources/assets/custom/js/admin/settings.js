/**
 * Created by mamau on 03.02.17.
 */
$(function(){

    var tag_input = $('#phone');
    try{
        tag_input.tag({
            placeholder: tag_input.attr('placeholder')
            //source: ['tag 1', 'tag 2'],//static autocomplet array

        });
    }
    catch(e) {
        //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
        tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
    }
});


function showErrorAlert (reason, detail) {
    var msg='';
    if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
    else {
        //console.log("error uploading file", reason, detail);
    }
    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
}
$('#description').ace_wysiwyg({
    toolbar:
        [
            'font',
            null,
            'fontSize',
            null,
            {name:'bold', className:'btn-info'},
            {name:'italic', className:'btn-info'},
            {name:'strikethrough', className:'btn-info'},
            {name:'underline', className:'btn-info'},
            null,
            {name:'insertunorderedlist', className:'btn-success'},
            {name:'insertorderedlist', className:'btn-success'},
            {name:'outdent', className:'btn-purple'},
            {name:'indent', className:'btn-purple'},
            null,
            {name:'justifyleft', className:'btn-primary'},
            {name:'justifycenter', className:'btn-primary'},
            {name:'justifyright', className:'btn-primary'},
            {name:'justifyfull', className:'btn-inverse'},
            null,
            {name:'createLink', className:'btn-pink'},
            {name:'unlink', className:'btn-pink'},
            null,
            {name:'insertImage', className:'btn-success'},
            null,
            'foreColor',
            null,
            {name:'undo', className:'btn-grey'},
            {name:'redo', className:'btn-grey'}
        ],
    'wysiwyg': {
        fileUploadError: showErrorAlert
    }
}).prev().addClass('wysiwyg-style2');

$('body').on('blur keyup mouseout','.wysiwyg-editor', function(){

    $('textarea[name="welcome"]').html($(this).html());
});

$(".icon-on-right").removeClass('icon-on-right');
