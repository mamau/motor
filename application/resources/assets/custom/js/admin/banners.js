/**
 * Created by mamau on 03.02.17.
 */
$(function(){

    if(!!$("#banner").length){
        $('#banner').ace_file_input({
            style: 'well',
            btn_choose: 'Ваще изображение',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'large'
        });

        if(typeof currBanner !== 'undefined'){
            $('#banner').ace_file_input('show_file_list', [
                {type: 'image', name: $('input[name="banner"]').val(), path: currBanner}
            ]);
        }
    }


    if(!!$("#bannersTable").length){

        var myTable = $("#bannersTable").dataTable({
            ajax: window.location.href,
            language: {
                url: '/ace/lang/dataTables.russian.lang'
            },
            bAutoWidth: false,
            "aoColumns": [
                { "bSortable": false },
                { visible: false },
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": false }
            ],
            select: {
                style: 'multi'
            },
            "order": [ [2, 'desc'] ]
        }).DataTable();

        myTable.on( 'select', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
            }
        });
        myTable.on( 'deselect', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
            }
        });
        /////////////////////////////////
        //table checkboxes
        $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

        //select/deselect all rows according to table header checkbox
        $('#bannersTable > thead > tr > th input[type=checkbox], #bannersTable_wrapper input[type=checkbox]').eq(0).on('click', function(){
            var th_checked = this.checked;//checkbox inside "TH" table header

            $('#bannersTable').find('tbody > tr').each(function(){
                var row = this;
                if(th_checked) myTable.row(row).select();
                else  myTable.row(row).deselect();
            });
        });

        //select/deselect a row when the checkbox is checked/unchecked
        $('#bannersTable').on('click', 'td input[type=checkbox]' , function(){
            var row = $(this).closest('tr').get(0);

            if(this.checked) {

                myTable.row(row).deselect();
            }else{

                myTable.row(row).select();
            }
        });
    }


    var tag_input = $('#keywords_seo');
    try{
        tag_input.tag({
            placeholder: tag_input.attr('placeholder')
            //source: ['tag 1', 'tag 2'],//static autocomplet array

        });
    }
    catch(e) {
        //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
        tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
    }
});
$("#toggleFile").click(function(){

    $(this).closest('.img-upload-wrapper').find(':file').prop('disabled', false).click();
});

$(".img-upload-wrapper input[type='file']").change(function(){
    readURL(this,'toggleFile');
});

/*
$('form').submit(function(e){

    var files = $("#banner").get(0).files.length;
    if(files == 0){
        $("#banner").prop('disabled', true);
    }
});*/


function readURL(input, image) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+image).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


function deleteBanners(){

    var offerIds = [];
    $('#bannersTable').find('tbody > tr').each(function(){
        var row = this;
        if($('input[type=checkbox]', row).prop('checked')){
            offerIds.push($('input[type=checkbox]:checked', row).val());
        }
    });
    if(offerIds.length > 0) {
        $.ajax({
            type: "DELETE",
            url: '/admin/banners/'+1,
            data: {
                ids: JSON.stringify(offerIds)
            },
            success: function (data) {

                if (data.status) {

                    offerIds.forEach(function (item, i, offerIds) {
                        if(i % 2 == 0){
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutRight animated');
                        }else{
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutLeft animated');
                        }
                        function removeElem() {

                            $('td span[data-id-row="' + item + '"]').parent().parent().remove();
                        }

                        setTimeout(removeElem, 500);
                    });


                    $.gritter.add({
                        title: 'Успех',
                        text: 'Баннеры успешно удалены',
                        sticky: false,
                        time: '3000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Не выбрано не одной записи',
            sticky: false,
            time: '3000',
            class_name: 'gritter-error'
        });
    }
}




