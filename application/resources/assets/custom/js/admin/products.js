/**
 * Created by mamau on 22.01.17.
 */
$(function() {
    var tag_input = $('#keywords_seo');
    try{
        tag_input.tag({
            placeholder: tag_input.attr('placeholder')
            //source: ['tag 1', 'tag 2'],//static autocomplet array

        });
    }
    catch(e) {
        //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
        tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
    }
});
$("#toggleFile").click(function(){

    $(this).closest('.img-upload-wrapper').find(':file').prop('disabled', false).click();
});

$(".img-upload-wrapper input[type='file']").change(function(){
    readURL(this,'toggleFile');
});

$('form').submit(function(e){

    var files = $("#thumb").get(0).files.length;
    if(files == 0){
        $("#thumb").prop('disabled', true);
    }
});

function readURL(input, image) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+image).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showErrorAlert (reason, detail) {
    var msg='';
    if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
    else {
        //console.log("error uploading file", reason, detail);
    }
    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
}

if($('#full_description').length){
    $('#full_description').ace_wysiwyg({
        toolbar:
            [
                'font',
                null,
                'fontSize',
                null,
                {name:'bold', className:'btn-info'},
                {name:'italic', className:'btn-info'},
                {name:'strikethrough', className:'btn-info'},
                {name:'underline', className:'btn-info'},
                null,
                {name:'insertunorderedlist', className:'btn-success'},
                {name:'insertorderedlist', className:'btn-success'},
                {name:'outdent', className:'btn-purple'},
                {name:'indent', className:'btn-purple'},
                null,
                {name:'justifyleft', className:'btn-primary'},
                {name:'justifycenter', className:'btn-primary'},
                {name:'justifyright', className:'btn-primary'},
                {name:'justifyfull', className:'btn-inverse'},
                null,
                {name:'createLink', className:'btn-pink'},
                {name:'unlink', className:'btn-pink'},
                null,
                {name:'insertImage', className:'btn-success'},
                null,
                'foreColor',
                null,
                {name:'undo', className:'btn-grey'},
                {name:'redo', className:'btn-grey'}
            ],
        'wysiwyg': {
            fileUploadError: showErrorAlert
        }
    }).prev().addClass('wysiwyg-style2');

    $('body').on('blur keyup mouseout','.wysiwyg-editor', function(){

        $('textarea[name="full_description"]').html($(this).html());
    });

    $(".icon-on-right").removeClass('icon-on-right');
}

if($('.chosen-select').length){
    $('.chosen-select').chosen({allow_single_deselect:true});
}

if($('.date-picker').length){
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd.mm.yyyy',
        language: 'ru',
        orientation: 'bottom auto',
        todayBtn: 'linked'
    }).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
}

function restoreProducts(){

    var offerIds = [];
    $('#productsTrashTable').find('tbody > tr').each(function(){
        var row = this;
        if($('input[type=checkbox]', row).prop('checked')){
            offerIds.push($('input[type=checkbox]:checked', row).val());
        }
    });
    if(offerIds.length > 0) {
        $.ajax({
            type: "POST",
            url: '/admin/products/restore',
            data: {
                ids: JSON.stringify(offerIds)
            },
            success: function (data) {

                if (data.status) {

                    offerIds.forEach(function (item, i, offerIds) {
                        if(i % 2 == 0){
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutRight animated');
                        }else{
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutLeft animated');
                        }
                        function removeElem() {

                            $('td span[data-id-row="' + item + '"]').parent().parent().remove();
                        }

                        setTimeout(removeElem, 500);
                    });


                    $.gritter.add({
                        title: 'Успех',
                        text: 'Товары успешно восстановлены',
                        sticky: false,
                        time: '3000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Не выбрано не одной записи',
            sticky: false,
            time: '3000',
            class_name: 'gritter-error'
        });
    }
}

function deleteProducts(force){


    var offerIds = [];
    $('#productTable,#productsTrashTable').find('tbody > tr').each(function(){
        var row = this;
        if($('input[type=checkbox]', row).prop('checked')){
            offerIds.push($('input[type=checkbox]:checked', row).val());
        }
    });
    if(offerIds.length > 0) {
        $.ajax({
            type: "POST",
            url: '/admin/products/delete',
            data: {
                ids: JSON.stringify(offerIds),
                force: force
            },
            success: function (data) {

                if (data.status) {

                    offerIds.forEach(function (item, i, offerIds) {
                        if(i % 2 == 0){
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutRight animated');
                        }else{
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutLeft animated');
                        }
                        function removeElem() {

                            $('td span[data-id-row="' + item + '"]').parent().parent().remove();
                        }

                        setTimeout(removeElem, 500);
                    });


                    $.gritter.add({
                        title: 'Успех',
                        text: 'Товары успешно удалены',
                        sticky: false,
                        time: '3000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Не выбрано не одной записи',
            sticky: false,
            time: '3000',
            class_name: 'gritter-error'
        });
    }
}

function addAttr(){

    var htmlAttr = '<div class="currElem"><div class="col-xs-12 col-sm-6">'+
                        '<div class="form-group">'+
                            '<label>Название атрибута</label>'+
                            '<input type="text" name="attr_name[]" class="form-control">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-11 col-sm-5">'+
                        '<div class="form-group">'+
                            '<label>Значение атрибута</label>'+
                            '<input type="text" name="attr_value[]" class="form-control">'+
                        '</div>'+
                    '</div>' +
                    '<div class="col-xs-1 col-sm-1 no-padding text-center del-wrapper">' +
                        '<label>Удалить</label>'+
                        '<a href="javascript:void(0)" onclick="deleteAttr(this)" class="removeElem">'+
                            '<i class="fa fa-trash btn btn-danger btn-xs"></i>'+
                        '</a>'+
                    '</div>'+
                    '</div>';
    $(".wrapp_attrs").prepend(htmlAttr);
}

function deleteAttr(elem){

    elem.closest('.currElem').remove();
}

function deleteExistAttr(elem, id){

    elem.closest('.currElem').remove();

    $.ajax({
        method: 'post',
        url: '/admin/delete/attr',
        data: {
            id: id
        },
        success: function(data){

            if(data.status){
                $.gritter.add({
                    title: 'Успех',
                    text: 'Атрибут успешно удален',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        }
    });
}
