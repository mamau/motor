/**
 * Created by mamau on 13.05.17.
 */

function showDetailProduct(id){

    $.ajax({
        method:'get',
        url:'/admin/orders/detail/'+id,
        success: function(data){

            if(data.status){

                bootbox.dialog({
                    title: 'Детали товара',
                    message: data.data.html,
                    buttons: {

                        dismiss: {
                            label: "Закрыть",
                            className: "btn-default btn-sm",
                            callback: function () {

                            }
                        }
                    }
                });
            }
        }
    });
}

function changeStatusOrder(id) {

    var obj = $("#statuses");
    var option = obj.find('option:selected').val();

    $.ajax({
        method: 'post',
        url: '/admin/order/changeStatus',
        data:{
            id: id,
            status: option
        },
        success: function (data) {

            if(data.status){

                $.gritter.add({
                    title: 'Успех',
                    text: 'Статус успешно обновлен',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        }
    })
}
