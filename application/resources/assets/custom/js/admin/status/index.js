/**
 * Created by mamau on 26.06.17.
 */

function deleteStatus(id) {
    $.ajax({
        method: 'DELETE',
        url: '/admin/status/'+id,
        data: {
            _method: 'DELETE'
        },
        success: function(data){

            if(data.status){

                $("#status_"+data.id).remove();
                $.gritter.add({
                    title: 'Успех',
                    text: 'Статус успешно удален',
                    sticky: false,
                    time: '3000',
                    class_name: 'gritter-success'
                });
            }
        }
    });
}

