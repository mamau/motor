$(function(){

    if(!!$("#usersTable").length){

        var myTable = $("#usersTable").dataTable({
            ajax: window.location.href,
            language: {
                url: '/ace/lang/dataTables.russian.lang'
            },
            bAutoWidth: false,
            "aoColumns": [
                { "bSortable": false },
                { visible: false },
                null,
                null,
                { "bSortable": false },
                { "bSortable": true },
                null
            ],
            select: {
                style: 'multi'
            },
            "order": [ [2, 'desc'] ]
        }).DataTable();

        myTable.on( 'select', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
            }
        });
        myTable.on( 'deselect', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
            }
        });
        /////////////////////////////////
        //table checkboxes
        $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

        //select/deselect all rows according to table header checkbox
        $('#usersTable > thead > tr > th input[type=checkbox], #usersTable_wrapper input[type=checkbox]').eq(0).on('click', function(){
            var th_checked = this.checked;//checkbox inside "TH" table header

            $('#usersTable').find('tbody > tr').each(function(){
                var row = this;
                if(th_checked) myTable.row(row).select();
                else  myTable.row(row).deselect();
            });
        });

        //select/deselect a row when the checkbox is checked/unchecked
        $('#usersTable').on('click', 'td input[type=checkbox]' , function(){
            var row = $(this).closest('tr').get(0);

            if(this.checked) {

                myTable.row(row).deselect();
            }else{

                myTable.row(row).select();
            }
        });
    }

    if(!!$("#usersTrashTable").length){

        var myTable = $("#usersTrashTable").dataTable({
            ajax: window.location.href,
            language: {
                url: '/ace/lang/dataTables.russian.lang'
            },
            bAutoWidth: false,
            "aoColumns": [
                { "bSortable": false },
                { visible: false },
                null,
                null,
                { "bSortable": false },
                { visible: false }
            ],
            select: {
                style: 'multi'
            },
            "order": [ [2, 'desc'] ]
        }).DataTable();

        myTable.on( 'select', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
            }
        });
        myTable.on( 'deselect', function ( e, dt, type, index ) {
            if ( type === 'row' ) {
                $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
            }
        });
        /////////////////////////////////
        //table checkboxes
        $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

        //select/deselect all rows according to table header checkbox
        $('#usersTrashTable > thead > tr > th input[type=checkbox], #usersTrashTable_wrapper input[type=checkbox]').eq(0).on('click', function(){
            var th_checked = this.checked;//checkbox inside "TH" table header

            $('#usersTrashTable').find('tbody > tr').each(function(){
                var row = this;
                if(th_checked) myTable.row(row).select();
                else  myTable.row(row).deselect();
            });
        });

        //select/deselect a row when the checkbox is checked/unchecked
        $('#usersTrashTable').on('click', 'td input[type=checkbox]' , function(){
            var row = $(this).closest('tr').get(0);

            if(this.checked) {

                myTable.row(row).deselect();
            }else{

                myTable.row(row).select();
            }
        });
    }



    if(!!$('.input-mask-phone').length){
        $('.input-mask-phone').mask('+7(999) 999-9999');
    }


});

function restoreUsers(){

    var offerIds = [];
    $('#usersTrashTable').find('tbody > tr').each(function(){
        var row = this;
        if($('input[type=checkbox]', row).prop('checked')){
            offerIds.push($('input[type=checkbox]:checked', row).val());
        }
    });
    if(offerIds.length > 0) {
        $.ajax({
            type: "POST",
            url: '/admin/users/restore',
            data: {
                ids: JSON.stringify(offerIds)
            },
            success: function (data) {

                if (data.status) {

                    offerIds.forEach(function (item, i, offerIds) {
                        if(i % 2 == 0){
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutRight animated');
                        }else{
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutLeft animated');
                        }
                        function removeElem() {

                            $('td span[data-id-row="' + item + '"]').parent().parent().remove();
                        }

                        setTimeout(removeElem, 500);
                    });


                    $.gritter.add({
                        title: 'Успех',
                        text: 'Пользователи успешно восстановлены',
                        sticky: false,
                        time: '3000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Не выбрано не одной записи',
            sticky: false,
            time: '3000',
            class_name: 'gritter-error'
        });
    }
}

function deleteUsers(force){


    var offerIds = [];
    $('#usersTable,#usersTrashTable').find('tbody > tr').each(function(){
        var row = this;
        if($('input[type=checkbox]', row).prop('checked')){
            offerIds.push($('input[type=checkbox]:checked', row).val());
        }
    });
    if(offerIds.length > 0) {
        $.ajax({
            type: "POST",
            url: '/admin/users/delete',
            data: {
                ids: JSON.stringify(offerIds),
                force: force
            },
            success: function (data) {

                if (data.status) {

                    offerIds.forEach(function (item, i, offerIds) {
                        if(i % 2 == 0){
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutRight animated');
                        }else{
                            $('td span[data-id-row="' + item + '"]').parent().parent().addClass('slideOutLeft animated');
                        }
                        function removeElem() {

                            $('td span[data-id-row="' + item + '"]').parent().parent().remove();
                        }

                        setTimeout(removeElem, 500);
                    });


                    $.gritter.add({
                        title: 'Успех',
                        text: 'Пользователи успешно удалены',
                        sticky: false,
                        time: '3000',
                        class_name: 'gritter-success'
                    });
                }
            }
        });
    }else{
        $.gritter.add({
            title: 'Неудача',
            text: 'Не выбрано не одной записи',
            sticky: false,
            time: '3000',
            class_name: 'gritter-error'
        });
    }
}
